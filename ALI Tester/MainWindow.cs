
/*****************************************************************************
* FILE: MainWindow.cs
*  
* DESCRIPTION: ALI Main Window
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 30 Aug 2007 Jesse Elliott
******************************************************************************/

using Tester;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ALITester
{
    public partial class MainWindow : TesterMainWindow
    {
        private enum ALIMessageType
        {
            Auto            = 0, // Automatically determine the type
            One_Path_Up     = 1, // Data retrieved, only one path available
            Two_Paths_Up    = 2, // Data retrieved, both paths operational
            Broadcast       = 3, // Broadcast message from ALI database (text may or may not be included)
            Broadcast_Stop  = 5, // Broadcast message from ALI database indicating host going out of service
            No_Record_Found = 9  // No address information found message, text = �NPANXX-TN No Record Found�
        }


        /****************************************************************************************************
        * NAME: Constructor: MainWindow()
        *
        * DESCRIPTION: Constructs an instance of this class and initializes the GUI elements
        *  
        * PARAMETERS:
        *   param - desc TODO
        * 
        * VARIABLES:
        *   CheckBox                 Local   - desc
        * 
        * FUNCTIONS:
        *   Func()                   Library - <System.>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private enum GuiMode {TesterMode, BasicMode, AdvancedMode};
        private GuiMode guiMode = GuiMode.BasicMode;

        public MainWindow(string[] args)
        {
            try
            {
                // parse args
                for (int i = 0; i < args.Length; ++i)
                {
                    if (args[i][0] != '/' && args[i][0] != '-') continue;

                    char[] trimChars = {'/','-'};
                    string arg = args[i].TrimStart(trimChars);

                    // guiTesterMode
                   if      (arg.ToLower() == "guitestermode") guiMode = GuiMode.TesterMode;
                   else if (arg.ToLower() == "advanced")      guiMode = GuiMode.AdvancedMode;
                }
            }
            catch {}

            // build GUI
            InitializeComponent();

            // default values:
            // - alternate links
            // - delay ack/nak .5
            // - delay after request .5
 
            // populate combo boxes
            sendModeComboBox.DataSource    = Enum.GetNames(typeof(SendMode));
            sendModeComboBox.SelectedIndex = SendMode.Alternate_Links.GetHashCode();

            nakHeartbeats1CheckBox. Checked = false;
            nakRequests1CheckBox.   Checked = false;
            nakHeartbeats2CheckBox. Checked = false;
            nakRequests2CheckBox.   Checked = false;

            delayRequest1SpinBox. Value = 0.5M;
            delayAckNak1SpinBox.  Value = 0.5M;
            delayRequest2SpinBox. Value = 0.5M;
            delayAckNak2SpinBox.  Value = 0.5M;

            stxCheckBox.Checked = true;
            etxCheckBox.Checked = true;
            transmitPosAndTypeCheckBox.Checked = false;

            //transmitTypeComboBox.DataSource    = Enum.GetNames(typeof(ALIMessageType));
            //transmitTypeComboBox.SelectedIndex = ALIMessageType.Auto.GetHashCode();

            if (guiMode == GuiMode.TesterMode)
            {
                // initialize denial of service (must be before init() so settings are restored)
                Button[] dosButtonList = {dosLink1Button, dosLink2Button, dosBothButton};

                initDOS(ref dosFileLabel, ref dosFileButton, ref dosButtonList, ref dosSpinBox);

                dosLink1Button. Click += dosLink1Clicked;
                dosLink2Button. Click += dosLink2Clicked;
                dosBothButton.  Click += dosBothClicked;
            }
            else // hide Gui elemets if not in Tester Mode
            {
                mainLayout.Controls.Remove(splitter);
                mainLayout.RowCount--;
                mainLayout.RowStyles.RemoveAt(0);

                splitter.Visible = false;
            }

            // use link headers in messages
            CommWidget.useMessageHeaders();

            // initialize main window
            if (guiMode == GuiMode.TesterMode) init("ALI Tester",    Version.getVersionStr(), "ALI Simulator.ico", mainLayout, splitter, 2);
            else                               init("ALI Simulator", Version.getVersionStr(), "ALI Simulator.ico", mainLayout, null, 2);

            // set duplicate message timeout
            setupRecentSocketMsg(50, 200);

            if (guiMode == GuiMode.TesterMode)
            {
                // connect buttons
                databaseFileButton.      Click += openDatabaseFile;
                saveDatabaseButton.      Click += saveDatabaseFile;

                addRecordButton.         Click += addCurrentRecord;
                deleteRecordButton.      Click += deleteCurrentRecord;
                transmitRecordButton.    Click += transmitCurrentRecord;
                clearRecordButton.       Click += clearRecordView;

                link1TransmitButton.     Click += link1TransmitData;
                link2TransmitButton.     Click += link2TransmitData;
                allTransmitButton.       Click += allTransmitData;
                clearBroadcastButton.    Click += clearBroadcastView;

                transmitPosAndTypeCheckBox.CheckedChanged += transmitPosAndTypeChanged;

                // connect combo boxes
                recordComboBox.   SelectedValueChanged += refreshCurrentRecord;
                sendModeComboBox. SelectedValueChanged += setSendMode;

                // connect text box short cuts
                recordTextBox.    KeyDown += textBoxKeyPress;
                broadcastTextBox. KeyDown += textBoxKeyPress;

                // add tool tips
                toolTip.SetToolTip(databaseFileButton,      "Select ALI Database File");
                //toolTip.SetToolTip(transmitPositionSpinBox, "Position Number");
                //toolTip.SetToolTip(transmitTypeComboBox,    "ALI Message Type");
            }
            else // not in Tester Mode
            {
                commGroupBox.Visible = false;

                npdLabel0.Text = "NPD0";
                npdLabel1.Text = "NPD1";
                npdLabel2.Text = "NPD2";
                npdLabel3.Text = "NPD3";

                SizeGripStyle = SizeGripStyle.Hide;
                MaximizeBox = false;
            }

            // load database file (settings are already loaded by this point)
            loadDatabaseFile(alidbFilename);
        }


        /****************************************************************************************************
        * NAME: Method: void addToBottomLayout()
        *
        * DESCRIPTION: Adds the going-out-of-service button at the bottom of the GUI
        * 
        * PARAMETERS:
        *   layout - the bottom layout to add GUI items to
        * 
        * VARIABLES:
        *   goingOutOfServiceButton  Local   - manually sends the broadcast stop message
        * 
        * FUNCTIONS:
        *   Add()                    Library - <System.Windows.Forms.TableLayoutPanel.TableLayoutControlCollection>
        *   sendBroadcastStop()      Local   - sends the broadcast stop message on the appropriate link(s)
        * 
        * COPYRIGHT: 2008 Experient Corporation
        *    AUTHOR: 28 Feb 2008 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void addToBottomLayout(ref TableLayoutPanel layout)
        {
            if (guiMode == GuiMode.TesterMode)
            {
                layout.Controls.Add(goingOutOfServiceButton, 8, 0);

                goingOutOfServiceButton.Text         = "Send Broadcast Stop";
                goingOutOfServiceButton.AutoSize     = true;
                goingOutOfServiceButton.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                goingOutOfServiceButton.Anchor       = AnchorStyles.None;
                goingOutOfServiceButton.Enabled      = false;
                goingOutOfServiceButton.Click       += sendBroadcastStop;
            }
            else
            {
                logCheckBox.Visible = false;
                logCheckBox.Checked = true;

                verboseCheckBox.Visible = false;
                verboseCheckBox.Checked = true;

                clearWindowButton.Text = "&Clear";

                // add Browse button select ALI database
                Button browseButton       = new Button();
                browseButton.AutoSize     = true;
                browseButton.Anchor       = AnchorStyles.None;
                browseButton.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                browseButton.Text         = "&Browse...";
                browseButton.Click       += openDatabaseFile;

                toolTip.SetToolTip(browseButton, "Browse for the ALI Database to use");
                layout.Controls.Add(browseButton, 4, 0);

                // add advanced checkbox
                advancedCheckBox = new CheckBox();
                advancedCheckBox.AutoSize = true;
                advancedCheckBox.Anchor   = AnchorStyles.None;
                advancedCheckBox.Text = "Advanced";
                advancedCheckBox.CheckedChanged += advancedCheckBox_CheckedChanged;

                layout.Controls.Add(advancedCheckBox, 5, 0);

                // TODO now: add About?
            }
        }

        private void advancedCheckBox_CheckedChanged(object unusedObject, EventArgs unusedArgs)
        {
            bool advanced = ! commGroupBox.Visible;

            mainLayout.Visible = false;

            if (advanced)
            {
                mainLayout.Controls.Add(npdGroupBox, 0, 1);
                mainLayout.RowStyles.Insert(1, new RowStyle()); // auto-size
                mainLayout.RowCount++;
            }
            else
            {
                mainLayout.Controls.Remove(npdGroupBox);
                mainLayout.RowStyles.RemoveAt(1);
                mainLayout.RowCount--;
            }

            commGroupBox. Visible = advanced;
            npdGroupBox.  Visible = advanced;

            mainLayout.Visible = true;
        }


        /****************************************************************************************************
        * NAME: Method: void createStatusBar()
        * 
        * DESCRIPTION: Create status bar
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2011 Experient Corporation
        *    AUTHOR: 19 May 2011 Jesse Elliott
        ****************************************************************************************************/ 

        protected override StatusStrip createStatusBar(TableLayoutPanel mainLayout)
        {
            if (guiMode == GuiMode.TesterMode)
            {
                statusBar = base.createStatusBar(mainLayout);

                countStatusLabel = new ToolStripStatusLabel();
                countStatusLabel.Text = "0 records";
                countStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;

                statusBar.Items.Add(countStatusLabel);

                return statusBar;
            }

            statusBar = new StatusStrip();

            statusBarLabel = new ToolStripStatusLabel();
            statusBarLabel.Text = "No ALI Database is currently loaded";
            statusBarLabel.TextAlign = ContentAlignment.MiddleLeft;
            statusBarLabel.ForeColor = Color.Red;
            statusBarLabel.Spring = true;

            countStatusLabel = new ToolStripStatusLabel();
            countStatusLabel.Text = "0 records";
            countStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;

            statusBar.SuspendLayout();

            // add status bar to main layout
            mainLayout.Controls.Add(statusBar, 0, mainLayout.RowCount);
            mainLayout.RowCount++;
            mainLayout.RowStyles.Add(new RowStyle()); // auto-size

            statusBar.Dock = DockStyle.Fill;
            statusBar.Items.AddRange(new ToolStripItem[] {statusBarLabel, countStatusLabel});
            statusBar.ShowItemToolTips = true;

            statusBar.GripStyle = ToolStripGripStyle.Hidden;

            statusBar.ResumeLayout();

            return statusBar;
        }


        /****************************************************************************************************
        * NAME: Method: void restoreSettings()
        *
        * DESCRIPTION: Restores settings from the registry
        *  
        * PARAMETERS:
        *   pSettings - settings object giving access to the registry
        * 
        * VARIABLES:
        *   recordSplitter           Local   - splitter in GUI to split the record widgets and the broadcast widgets 
        *   npdSpinBox0              Local   - NPD spin box
        *   npdSpinBox1              Local   - NPD spin box
        *   npdSpinBox2              Local   - NPD spin box
        *   npdSpinBox3              Local   - NPD spin box
        *   alidbFilename            Local   - the full path of the ALI database file
        *   sendModeComboBox         Local   - drop down choices for send mode
        *   nakHeartbeats1CheckBox   Local   - checkbox for whether to NAK link 1 heartbeats
        *   nakRequests1CheckBox     Local   - checkbox for whether to NAK link 1 requests
        *   delayRequest1SpinBox     Local   - spin box specifying delay of link 1 requests
        *   delayAckNak1SpinBox      Local   - spin box specifying delay of link 1 ack/nak
        *   nakHeartbeats2CheckBox   Local   - checkbox for whether to NAK link 2 heartbeats
        *   nakRequests2CheckBox     Local   - checkbox for whether to NAK link 2 requests
        *   delayRequest2SpinBox     Local   - spin box specifying delay of link 2 requests
        *   delayAckNak2SpinBox      Local   - spin box specifying delay of link 2 ack/nak
        * 
        * FUNCTIONS:
        *   restoreSettings()        Library - <TesterMainWindow>
        *   GetValue()               Library - <Microsoft.Win32.RegistryKey>
        *   setSendMode()            Local   - resolves send mode GUI state
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void restoreWindowState(Microsoft.Win32.RegistryKey pSettings)
        {
            if (guiMode == GuiMode.TesterMode)
            {
                base.restoreWindowState(pSettings);
            }
            else
            {
                try
                {
                    Top  = (int)pSettings.GetValue("Top",  Top);
                    Left = (int)pSettings.GetValue("Left", Left);
                }
                catch (Exception e)
                {
                    warning("Failed to restore window location (" + e.Message + ")");
                }

                MinimumSize = new Size(780, 460);
                MaximumSize = new Size(780, 460);

                WindowState = FormWindowState.Normal;
            }
        }

        protected override void restoreSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            base.restoreSettings(pSettings);

            try
            {
                npdSpinBox0.Value = Convert.ToDecimal(pSettings.GetValue("npd0", npdSpinBox0.Value));
                npdSpinBox1.Value = Convert.ToDecimal(pSettings.GetValue("npd1", npdSpinBox1.Value));
                npdSpinBox2.Value = Convert.ToDecimal(pSettings.GetValue("npd2", npdSpinBox2.Value));
                npdSpinBox3.Value = Convert.ToDecimal(pSettings.GetValue("npd3", npdSpinBox3.Value));

                // initialize default ALI Database
                if (guiMode != GuiMode.TesterMode)
                {
                    string appDir = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                    appDir = appDir.Replace("file:\\", "");

                    alidbFilename = appDir + "\\demo.ali";
                }

                alidbFilename = (string)pSettings.GetValue("alidbFile", alidbFilename);

                if (guiMode == GuiMode.TesterMode)
                {
                    recordSplitter.SplitterDistance =
                        (int)pSettings.GetValue("RecordSplitDist", recordSplitter.SplitterDistance);

                    sendModeComboBox.SelectedIndex =
                        (int)pSettings.GetValue("sendMode", sendModeComboBox.SelectedIndex);

                    setSendMode(this, null);

                    // link 1
                    nakHeartbeats1CheckBox.Checked =
                        Convert.ToBoolean(pSettings.GetValue("nakHeartbeats1", nakHeartbeats1CheckBox.Checked));

                    nakRequests1CheckBox.Checked =
                        Convert.ToBoolean(pSettings.GetValue("nakRequests1", nakRequests1CheckBox.Checked));

                    delayRequest1SpinBox.Value =
                        Convert.ToDecimal(pSettings.GetValue("delayAfterRequest1", delayRequest1SpinBox.Value));

                    delayAckNak1SpinBox.Value =
                        Convert.ToDecimal(pSettings.GetValue("delayAckNak1", delayAckNak1SpinBox.Value));

                    // link 2
                    nakHeartbeats2CheckBox.Checked =
                        Convert.ToBoolean(pSettings.GetValue("nakHeartbeats2", nakHeartbeats2CheckBox.Checked));

                    nakRequests2CheckBox.Checked =
                        Convert.ToBoolean(pSettings.GetValue("nakRequests2", nakRequests2CheckBox.Checked));

                    delayRequest2SpinBox.Value =
                        Convert.ToDecimal(pSettings.GetValue("delayAfterRequest2", delayRequest2SpinBox.Value));

                    delayAckNak2SpinBox.Value =
                        Convert.ToDecimal(pSettings.GetValue("delayAckNak2", delayAckNak2SpinBox.Value));
                }
            }
            catch (Exception e)
            {
                warning("Failed to restore ALI settings (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void rememberSettings()
        *
        * DESCRIPTION: Remembers settings in the registry
        *  
        * PARAMETERS:
        *   pSettings - settings object giving access to the registry
        * 
        * VARIABLES:
        *   recordSplitter           Local   - splitter in GUI to split the record widgets and the broadcast widgets 
        *   npdSpinBox0              Local   - NPD spin box
        *   npdSpinBox1              Local   - NPD spin box
        *   npdSpinBox2              Local   - NPD spin box
        *   npdSpinBox3              Local   - NPD spin box
        *   alidbFilename            Local   - the full path of the ALI database file
        *   sendModeComboBox         Local   - drop down choices for send mode
        *   nakHeartbeats1CheckBox   Local   - checkbox for whether to NAK link 1 heartbeats
        *   nakRequests1CheckBox     Local   - checkbox for whether to NAK link 1 requests
        *   delayRequest1SpinBox     Local   - spin box specifying delay of link 1 requests
        *   delayAckNak1SpinBox      Local   - spin box specifying delay of link 1 ack/nak
        *   nakHeartbeats2CheckBox   Local   - checkbox for whether to NAK link 2 heartbeats
        *   nakRequests2CheckBox     Local   - checkbox for whether to NAK link 2 requests
        *   delayRequest2SpinBox     Local   - spin box specifying delay of link 2 requests
        *   delayAckNak2SpinBox      Local   - spin box specifying delay of link 2 ack/nak
        * 
        * FUNCTIONS:
        *   rememberSettings()       Library - <TesterMainWindow>
        *   SetValue()               Library - <Microsoft.Win32.RegistryKey>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void rememberSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            base.rememberSettings(pSettings);

            try
            {
                pSettings.SetValue("npd0", npdSpinBox0.Value);
                pSettings.SetValue("npd1", npdSpinBox1.Value);
                pSettings.SetValue("npd2", npdSpinBox2.Value);
                pSettings.SetValue("npd3", npdSpinBox3.Value);

                pSettings.SetValue("alidbFile", alidbFilename);

                if (guiMode == GuiMode.TesterMode)
                {
                    pSettings.SetValue("RecordSplitDist", recordSplitter.SplitterDistance);

                    pSettings.SetValue("sendMode",      sendModeComboBox.      SelectedIndex);

                    // link 1
                    pSettings.SetValue("nakHeartbeats1",     nakHeartbeats1CheckBox. Checked);
                    pSettings.SetValue("nakRequests1",       nakRequests1CheckBox.   Checked);
                    pSettings.SetValue("delayAfterRequest1", delayRequest1SpinBox.   Value);
                    pSettings.SetValue("delayAckNak1",       delayAckNak1SpinBox.    Value);

                    // link 2
                    pSettings.SetValue("nakHeartbeats2",     nakHeartbeats2CheckBox. Checked);
                    pSettings.SetValue("nakRequests2",       nakRequests2CheckBox.   Checked);
                    pSettings.SetValue("delayAfterRequest2", delayRequest2SpinBox.   Value);
                    pSettings.SetValue("delayAckNak2",       delayAckNak2SpinBox.    Value);
                }
            }
            catch (Exception e)
            {
                warning("Failed to remember ALI settings: " + e.Message);
            }
        }

        /****************************************************************************************************
        * NAME: Method: void loadEvent()
        *
        * DESCRIPTION: Event that occurs when the form loads for the first time
        *  
        * FUNCTIONS:
        *   TODO
        *
        * COPYRIGHT: 2011 Experient Corporation
        *    AUTHOR: 19 May 2011 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void loadEvent(object obj, EventArgs args)
        {
            base.loadEvent(obj, args);

            if (guiMode != GuiMode.TesterMode)
            {
                if (guiMode == GuiMode.AdvancedMode && advancedCheckBox != null)
                {
                    advancedCheckBox.Checked = true;
                }

                start(this, null);
            }
        }

        protected override void closeEvent(object sender, FormClosingEventArgs args)
        {
            DialogResult result = MessageBox.Show(
                this, "Are you sure you want to quit?", Text, MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes) base.closeEvent(sender, args);
            else                            args.Cancel = true;
        }


        /****************************************************************************************************
        * NAME: Method: void runningChanged()
        *
        * DESCRIPTION: Running state changed (start/stop)
        *  
        * PARAMETERS:
        *   running - whether start or stop was pushed
        * 
        * FUNCTIONS:
        *   sendBroadcastStop()      Local   - sends the broadcast stop message on the appropriate link(s)
        *   setSendMode()            Local   - resolves send mode GUI state
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void runningChanged(bool running)
        {
            // if stopping, send stop message
            if ( ! running) sendBroadcastStop(this, null);

            // setting the send mode enables the Broadcast buttons correctly
            setSendMode(this, null);
        }


        /****************************************************************************************************
        * NAME: Method: void sendBroadcastStop()
        *
        * DESCRIPTION: Sends the broadcast stop message on the appropriate link(s)
        *  
        * VARIABLES:
        *   rememberedSendMode       Local   - current send mode to set back to after sending on each link
        * 
        * FUNCTIONS:
        *   getSendMode()            Library - <TesterMainWindow>
        *   sendALIMessage()         Local   - send a correctly formatted ALI message
        *   setSendMode()            Library - <TesterMainWindow>
        *   setCurCommWidget()       Library - <TesterMainWindow>
        *   setSendMode()            Local   - resolves send mode GUI state
        *
        * COPYRIGHT: 2008 Experient Corporation
        *    AUTHOR: 28 Feb 2008 Jesse Elliott
        ****************************************************************************************************/ 

        private void sendBroadcastStop(object unusedObject, EventArgs unusedArgs)
        {
            if (getSendMode() == SendMode.Both_Down) return;

            if (getSendMode() == SendMode.Link1_Down || getSendMode() == SendMode.Link2_Down)
            {
                sendALIMessage("", ALIMessageType.Broadcast_Stop, 0, "Broadcast Stop", 0);
            }
            else
            {
                // remember send mode
                SendMode rememberedSendMode = getSendMode();
                setSendMode(SendMode.Independant_Links);

                setCurCommWidget(1);
                sendALIMessage("", ALIMessageType.Broadcast_Stop, 0, "Broadcast Stop", 0);

                setCurCommWidget(2);
                sendALIMessage("", ALIMessageType.Broadcast_Stop, 0, "Broadcast Stop", 0);

                // restore send mode
                setSendMode(rememberedSendMode);
            }
        }

        /****************************************************************************************************
        * NAME: Method: void transmitPosAndTypeChanged()
        *
        * DESCRIPTION: Sends the broadcast stop message on the appropriate link(s)
        *  
        * VARIABLES:
        *   transmitPosAndTypeTextBox Local  - text box holding ALI message type and position
        *   transmitPosAndTypeCheckBox Local - check box for enabling the text box
        * 
        * COPYRIGHT: 2008 Experient Corporation
        *    AUTHOR: 28 Feb 2008 Jesse Elliott
        ****************************************************************************************************/ 

        private void transmitPosAndTypeChanged(object unusedObject, EventArgs unusedArgs)
        {
            transmitPosAndTypeTextBox.Enabled = transmitPosAndTypeCheckBox.Checked;
        }


        /****************************************************************************************************
        * NAME: Method: void dosLink1Clicked(), dosLink2Clicked(), dosBothClicked()
        *
        * DESCRIPTION: Handled DOS buttons clicked
        *    
        * VARIABLES:
        *   dosSendMode              Local   - defines what links to send DOS on
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 22 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void dosLink1Clicked (object unusedObject, EventArgs unusedArgs) {dosSendMode = SendMode.Link2_Down;}
        private void dosLink2Clicked (object unusedObject, EventArgs unusedArgs) {dosSendMode = SendMode.Link1_Down;}
        private void dosBothClicked  (object unusedObject, EventArgs unusedArgs) {dosSendMode = SendMode.Alternate_Links;}


        /****************************************************************************************************
        * NAME: Method: void sendSingleDOS()
        *
        * DESCRIPTION: Sends DOS on the correct link
        *    
        * VARIABLES:
        *   rememberedSendMode       Local   - keeps track of current send mode
        *   dosSendMode              Local   - specifies which link(s) DOS goes out on
        * 
        * FUNCTIONS:
        *   getSendMode()            Library - <TesterMainWindow>
        *   setSendMode()            Library - <TesterMainWindow>
        *   setCurCommWidget()       Library - <TesterMainWindow>
        *   sendSingleDOS()          Library - <TesterMainWindow>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 22 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void sendSingleDOS(object unusedObject, EventArgs unusedArgs)
        {
            // remember send mode
            SendMode rememberedSendMode = getSendMode();

            if (dosSendMode == SendMode.Link1_Down)
            {
                setSendMode(SendMode.Independant_Links);
                setCurCommWidget(2);
            }
            else if (dosSendMode == SendMode.Link2_Down)
            {
                setSendMode(SendMode.Independant_Links);
                setCurCommWidget(1);
            }
            else
            {
                setSendMode(dosSendMode);
            }

            base.sendSingleDOS(unusedObject, unusedArgs);

            // restore send mode
            setSendMode(rememberedSendMode);
        }


        /****************************************************************************************************
        * NAME: Method: void openDatabaseFile()
        *
        * DESCRIPTION: Opens an ALI database file
        *  
        * VARIABLES:
        *   buffer                   Local   - holds the contents of the file
        *   length                   Local   - length of the file contents
        *   success                  Local   - whether the file was found and loaded
        *   alidbFilename            Local   - database file name
        *   databaseFileLabel        Local   - GUI label to display new filename and size
        * 
        * FUNCTIONS:
        *   chooseFile()             Library - <TesterMainWindow>
        *   loadDatabaseFile         Local   - Loads an ALI database file
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void openDatabaseFile(object unusedObj, EventArgs unusedArgs)
        {
            // choose file
            byte[] buffer = null;
            ulong  length = 0;

            bool success = chooseFile(ref alidbFilename, ref buffer, ref length, databaseFileLabel);

            if (guiMode != GuiMode.TesterMode)
            {
                statusBarLabel.Text        = databaseFileLabel.Text;
                statusBarLabel.ForeColor   = databaseFileLabel.ForeColor;
                statusBarLabel.ToolTipText = toolTip.GetToolTip(databaseFileLabel);
            }

            if (success)
            {
                loadDatabaseFile(buffer, length);

                if (guiMode != GuiMode.TesterMode) statusBarLabel.Text = alidbFilename;
            }
        }


        /****************************************************************************************************
        * NAME: Method: void loadDatabaseFile()
        *
        * DESCRIPTION: Loads an ALI database file
        *  
        * PARAMETERS:
        *   param - desc TODO
        * 
        * VARIABLES:
        *   CheckBox                 Local   - desc
        * 
        * FUNCTIONS:
        *   Func()                   Library - <System.>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 10 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        void loadDatabaseFile(string filename)
        {
            // load database file
            byte[] buffer;
            ulong  length;

            alidbFilename = filename;

            bool success = readFile(alidbFilename, out buffer, out length, databaseFileLabel);

            if (guiMode != GuiMode.TesterMode)
            {
                statusBarLabel.Text        = databaseFileLabel.Text;
                statusBarLabel.ForeColor   = databaseFileLabel.ForeColor;
                statusBarLabel.ToolTipText = toolTip.GetToolTip(databaseFileLabel);
            }

            if (success)
            {
                loadDatabaseFile(buffer, length);

                if (guiMode != GuiMode.TesterMode) statusBarLabel.Text = alidbFilename;
            }
        }

        private void addRecord(string curNumber, string curRecord)
        {
            if (curNumber != null && curNumber.Length != 0)
            {
                if ( ! aliDatabase.ContainsKey(curNumber))
                {
                    aliDatabase.Add(curNumber, new Queue<string>());
                    recordComboBox.Items.Add(curNumber);
                }

                aliDatabase[curNumber].Enqueue(curRecord);
                ++recordCount;

                if (countStatusLabel != null)
                {
                    countStatusLabel.Text = recordCount.ToString() + " records";
                }
            }
        }

        private void loadDatabaseFile(byte[] buffer, ulong length)
        {
            // clear the current database
            aliDatabase.Clear();
            recordComboBox.Items.Clear();
            clearRecordView(this, null);
            recordCount = 0;

            if (countStatusLabel != null) countStatusLabel.Text = "0 records";

            notice("Loading ALI Database from \"" + alidbFilename + "\"");

            // parse file
            ulong  i = 0;
            string curLine;
            int    curLineNum = 0;

            string curNumber = null;
            string curRecord = "";

            while (getLine(ref buffer, length, ref i, out curLine))
            {
                curLineNum++;

                // ignore comments
                if (curLine.Length > 0 && curLine[0] == '#') continue;

                // if this is the start of a new record
                if (curLine.Length > 0 && curLine[0] == '~')
                {
                    // add the current record first
                    addRecord(curNumber, curRecord);

                    curNumber = "";
                    curRecord = "";

                    try
                    {
                        // check for a second ~
                        int secondNumberIndex = curLine.IndexOf('~', 1);

                        // if there is no second number, read to the end
                        if (secondNumberIndex < 0)
                        {
                            curNumber = curLine.Substring(1);
                        }
                        // else stop at the second number
                        else
                        {
                            curNumber = curLine.Substring(1, secondNumberIndex - 1);
                        }

                        Convert.ToUInt64(curNumber);
                    }
                    catch (Exception)
                    {
                        warning("Ignoring Record \"" + curLine + "\" (expected a number)");
                    }
                }
                else if (curNumber != null)
                {
                    curRecord += curLine + "\r\n";
                }
                else
                {
                    warning("Expected a new record starting with '~' (found \"" + curLine + "\")");
                }
            }

            // add the last record
            addRecord(curNumber, curRecord);

            // select the first record in the newly loaded database
            if (recordComboBox.Items.Count > 0) recordComboBox.SelectedIndex = 0;

            notice("  " + recordCount + " records loaded\n");
        }


        /****************************************************************************************************
        * NAME: Method: void saveDatabaseFile()
        *
        * DESCRIPTION: Saves the current ALI database to a file
        *  
        * VARIABLES:
        *   aliDatabase              Local   - Dictionary structure that maps phone numbers to ALI records
        *   pFile                    Local   - pointer to the opened ALI database file
        *   alidbFilename            Local   - ALI database filename
        *   alidbStr                 Local   - ALI database file contents to be saved
        *   record                   Local   - loop variable for each ALI record
        *   buffer                   Local   - alidbStr converted to "bytes"
        * 
        * FUNCTIONS:
        *   Open()                   Library - <System.IO.File>
        *   GetBytes()               Library - <System.Text.ASCIIEncoding.ASCII>
        *   Write()                  Library - <System.IO.FileStream>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void saveDatabaseFile(object unusedObj, EventArgs unusedArgs)
        {
            if (recordCount != 0)
            {
                try
                {
                    // try to open file
                    System.IO.FileStream pFile = System.IO.File.Open(alidbFilename,
                                                                     System.IO.FileMode.Truncate,
                                                                     System.IO.FileAccess.Write,
                                                                     System.IO.FileShare.ReadWrite);
                    string alidbStr = "";

                    foreach (KeyValuePair<string, Queue<string>> recordQueue in aliDatabase)
                    {
                        foreach (string record in recordQueue.Value)
                        {
                            alidbStr += '~' + recordQueue.Key + "\r\n" + record;
                        }
                    }

                    byte[] buffer = new byte[alidbStr.Length];
                    ASCIIEncoding.ASCII.GetBytes(alidbStr, 0, alidbStr.Length, buffer, 0);

                    pFile.Write(buffer, 0, alidbStr.Length);

                    notice("Saved \"" + alidbFilename + "\"");
                }
                catch (Exception e)
                {
                    warning("Failed to save file (" + e.Message + ")");
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: void addCurrentRecord()
        *
        * DESCRIPTION: Adds the current record to the ALI database (not to the file yet)
        *  
        * VARIABLES:
        *   curText                  Local   - the current number in the record drop down
        *   recordComboBox           Local   - drop down for selecting, adding, and deleting records
        *   aliDatabase              Local   - Dictionary structure that maps phone numbers to ALI records
        * 
        * FUNCTIONS:
        *   Trim()                   Library - <System.string>
        *   FindStringExact()        Library - <System.Windows.Forms.ComboBox>
        *   ToUInt64()               Library - <System.Convert>
        *   ContainsKey()            Library - <System.Collections.Generic.Dictionary>
        *   Add()                    Library - <System.Collections.Generic.Dictionary>
        *   Add()                    Library - <System.Windows.Forms.ComboBox.ObjectCollection>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void addCurrentRecord(object unusedObj, EventArgs unusedArgs)
        {
            string curText = recordComboBox.Text.Trim();

            // if there is something to add, try to add it
            if (curText.Length > 0)
            {
                try
                {
                    Convert.ToUInt64(curText);
                    addRecord(curText, recordTextBox.Text);
                }
                catch (Exception)
                {
                    warning("Failed to add record " + curText + " (not a number)");
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: void deleteCurrentRecord()
        *
        * DESCRIPTION: Deletes the current record from the ALI database (not from the file yet)
        *  
        * VARIABLES:
        *   curText                  Local   - the current number in the record drop down
        *   recordComboBox           Local   - drop down for selecting, adding, and deleting records
        *   aliDatabase              Local   - Dictionary structure that maps phone numbers to ALI records
        * 
        * FUNCTIONS:
        *   Trim()                   Library - <System.string>
        *   ToUInt64()               Library - <System.Convert>
        *   ContainsKey()            Library - <System.Collections.Generic.Dictionary>
        *   Remove()                 Library - <System.Collections.Generic.Dictionary>
        *   Remove()                 Library - <System.Windows.Forms.ComboBox.ObjectCollection>
        *   clearRecordView          Local   - Clears the record view so the user can add a new record
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void deleteCurrentRecord(object unusedObj, EventArgs unusedArgs)
        {
            string curText = recordComboBox.Text.Trim();

            try
            {
                if (curText.Length > 0)
                {
                    Convert.ToUInt64(curText);

                    // TODO now: only remove one record or ask?
                    if (aliDatabase.ContainsKey(curText))
                    {
                        recordCount -= aliDatabase[curText].Count;
                        aliDatabase.Remove(curText);

                        if (countStatusLabel != null)
                        {
                            countStatusLabel.Text = recordCount.ToString() + " records";
                        }
                    }

                    recordComboBox.Items.Remove(curText);
                }
            }
            catch (Exception)
            {
                // ignore conversion errors on remove, and just clear the record
            }

            clearRecordView(this, null);
        }


        /****************************************************************************************************
        * NAME: Method: void refreshCurrentRecord()
        *
        * DESCRIPTION: Refreshes the current record from the ALI database
        *  
        * VARIABLES:
        *   curRecord                Local   - the content for the current record
        *   recordComboBox           Local   - drop down for selecting, adding, and deleting records
        *   aliDatabase              Local   - Dictionary structure that maps phone numbers to ALI records
        *   recordTextBox            Local   - displays the current ALI record content
        * 
        * FUNCTIONS:
        *   TryGetValue              Library - <System.Collections.Generic.Dictionary>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void refreshCurrentRecord(object unusedObj, EventArgs unusedArgs)
        {
            // TODO now: add a button next to the recordComboBox "1/3"
            //           that cycles to the next record in the queue
            Queue<string> curRecordQueue;

            if (aliDatabase.TryGetValue((string)recordComboBox.SelectedItem, out curRecordQueue))
            {
                // cycle record queue
                recordTextBox.Text = curRecordQueue.Dequeue();
                curRecordQueue.Enqueue(recordTextBox.Text);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void transmitCurrentRecord()
        *
        * DESCRIPTION: Transmits the current record
        *  
        * VARIABLES:
        *   curText                  Local   - the current number in the record drop down
        *   recordComboBox           Local   - drop down for selecting, adding, and deleting records
        *   aliMsgType               Local   - ALI msg type
        *   recordTextBox            Local   - displays the current ALI record content
        * 
        * FUNCTIONS:
        *   Trim()                   Library - <System.string>
        *   ToUInt64()               Library - <System.Convert>
        *   getNumSocketsConnected() Library - <TesterMainWindo>
        *   getSendMode()            Library - <TesterMainWindo>
        *   sendALIMessage()         Local   - send a correctly formatted ALI message
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void transmitCurrentRecord(object unusedObj, EventArgs unusedArgs)
        {
            string curText = recordComboBox.Text.Trim();

            // if there is something to send
            if (curText.Length > 0)
            {
                try
                {
                    // make sure the text in the combo box is a number
                    Convert.ToUInt64(curText);

                    ALIMessageType aliMsgType = ALIMessageType.Broadcast;
                    int            aliMsgPos  = 0;
                    //int            aliMsgPos  = (int)transmitPositionSpinBox.Value;


                    //if (transmitTypeComboBox.SelectedIndex == (int)ALIMessageType.Auto)
                    if ( ! transmitPosAndTypeCheckBox.Checked)
                    {
                        if (getNumSocketsConnected() == 2 &&
                            getSendMode() != SendMode.Link1_Down && getSendMode() != SendMode.Link2_Down)
                        {
                            aliMsgType = ALIMessageType.Two_Paths_Up;
                        }
                        else if (getNumSocketsConnected() > 0)
                        {
                            aliMsgType = ALIMessageType.One_Path_Up;
                        }

                        if (getSendMode() == SendMode.Independant_Links)
                        {
                            setCurCommWidget(1);

                            sendALIMessage(recordTextBox.Text, stxCheckBox.Checked, etxCheckBox.Checked,
                                aliMsgType, aliMsgPos, "Record " + recordComboBox.Text, 0);

                            setCurCommWidget(2);
                        }

                        sendALIMessage(recordTextBox.Text, stxCheckBox.Checked, etxCheckBox.Checked,
                            aliMsgType, aliMsgPos, "Record " + recordComboBox.Text, 0);
                    }
                    else
                    {
                        //Array enumArray = Enum.GetValues(typeof(ALIMessageType));
                        //aliMsgType = (ALIMessageType)enumArray.GetValue(transmitTypeComboBox.SelectedIndex);

                        string aliMsgTypeAndPosStr = transmitPosAndTypeTextBox.Text;

                        if (getSendMode() == SendMode.Independant_Links)
                        {
                            setCurCommWidget(1);

                            sendALIMessage(recordTextBox.Text, stxCheckBox.Checked, etxCheckBox.Checked,
                                aliMsgTypeAndPosStr, "Record " + recordComboBox.Text, 0);

                            setCurCommWidget(2);
                        }

                        sendALIMessage(recordTextBox.Text, stxCheckBox.Checked, etxCheckBox.Checked,
                            aliMsgTypeAndPosStr, "Record " + recordComboBox.Text, 0);
                    }
                }
                catch (Exception)
                {
                    warning("Failed to transmit record " + curText + " (not a number)");
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: void clearRecordView()
        *
        * DESCRIPTION: Clears the record view so the user can add a new record
        *  
        * VARIABLES:
        *   recordTextBox            Local   - displays the current ALI record content
        *   recordComboBox           Local   - drop down for selecting, adding, and deleting records
        * 
        * FUNCTIONS:
        *   Clear()                  Library - <System.Windows.Forms.TextBox>
        *   ResetText()              Library - <System.Windows.Forms.ComboBox>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void clearRecordView(object unusedObj, EventArgs unusedArgs)
        {
            recordTextBox.Clear();
            recordComboBox.ResetText();
        }


        /****************************************************************************************************
        * NAME: Method: void allTransmitData() void link1TransmitData() void link2TransmitData()
        *
        * DESCRIPTION: Transmits the current data in the Broadcast view
        *  
        * VARIABLES:
        *   rememberedSendMode       Local   - current send mode
        *   broadcastTextBox         Local   - content to broadcast
        * 
        * FUNCTIONS:
        *   getSendMode()            Library - <TesterMainWindow>
        *   setSendMode()            Library - <TesterMainWindow>
        *   setCurCommWidget()       Library - <TesterMainWindow>
        *   sendALIMessage()         Local   - send a correctly formatted ALI message
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void link1TransmitData(object unusedObj, EventArgs unusedArgs)
        {
            // remember send mode
            SendMode rememberedSendMode = getSendMode();
            setSendMode(SendMode.Independant_Links);
            setCurCommWidget(1);

            sendALIMessage(broadcastTextBox.Text, ALIMessageType.Broadcast, 0, "Broadcast Data", 0);

            // restore send mode
            setSendMode(rememberedSendMode);
        }

        private void link2TransmitData(object unusedObj, EventArgs unusedArgs)
        {
            // remember send mode
            SendMode rememberedSendMode = getSendMode();
            setSendMode(SendMode.Independant_Links);
            setCurCommWidget(2);

            sendALIMessage(broadcastTextBox.Text, ALIMessageType.Broadcast, 0, "Broadcast Data", 0);

            // restore send mode
            setSendMode(rememberedSendMode);
        }

        private void allTransmitData(object unusedObj, EventArgs unusedArgs)
        {
            // remember send mode
            SendMode rememberedSendMode = getSendMode();
            setSendMode(SendMode.Independant_Links);

            setCurCommWidget(1);
            sendALIMessage(broadcastTextBox.Text, ALIMessageType.Broadcast, 0, "Broadcast Data", 0);

            setCurCommWidget(2);
            sendALIMessage(broadcastTextBox.Text, ALIMessageType.Broadcast, 0, "Broadcast Data", 0);

            // restore send mode
            setSendMode(rememberedSendMode);
        }


        /****************************************************************************************************
        * NAME: Method: void clearBroadcastView()
        *
        * DESCRIPTION: Clears the broadcast view
        *  
        * VARIABLES:
        *   broadcastTextBox         Local   - content to broadcast
        * 
        * FUNCTIONS:
        *   Clear()                  Library - <System.Windows.Forms.TextBox>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 10 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void clearBroadcastView(object unusedObj, EventArgs unusedArgs)
        {
            broadcastTextBox.Clear();
        }


        /****************************************************************************************************
        * NAME: Method: uint handleSocketMsg()
        *
        * DESCRIPTION: Handle socket messages (return number of bytes handled)
        *  
        * PARAMETERS:
        *   param - desc TODO
        * 
        * VARIABLES:
        *   CheckBox                 Local   - desc
        * 
        * FUNCTIONS:
        *   Func()                   Library - <System.>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 10 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override uint handleSocketMsg(byte[] newSocketMsg, ulong length, CommWidget c)
        {
            // if the user has specified this link as down, ignore the message
            if ( (SendMode)sendModeComboBox.SelectedIndex == SendMode.Both_Down ||
                ((SendMode)sendModeComboBox.SelectedIndex == SendMode.Link1_Down && c.getId() == 1) ||
                ((SendMode)sendModeComboBox.SelectedIndex == SendMode.Link2_Down && c.getId() == 2))
            {
                return 0;
            }

            // if this link is down, don't respond
            if ( getSendMode() == SendMode.Both_Down                     ||
                (getSendMode() == SendMode.Link1_Down && c.getId() == 1) ||
                (getSendMode() == SendMode.Link2_Down && c.getId() == 2)) return 0;

            // set per link settings
            bool nakHeartbeats = false, nakRequests  = false;
            int  ackNakDelay   = 0,     requestDelay = 0;

            if (c.getId() == 1)
            {
                nakHeartbeats = nakHeartbeats1CheckBox.    Checked;
                nakRequests   = nakRequests1CheckBox.      Checked;
                ackNakDelay   = (int)(delayAckNak1SpinBox.  Value * 1000);
                requestDelay  = (int)(delayRequest1SpinBox. Value * 1000);
            }
            else if (c.getId() == 2)
            {
                nakHeartbeats = nakHeartbeats2CheckBox.   Checked;
                nakRequests   = nakRequests2CheckBox.     Checked;
                ackNakDelay   = (int)(delayAckNak2SpinBox.  Value * 1000);
                requestDelay  = (int)(delayRequest2SpinBox. Value * 1000);
            }

            // if heartbeat
            if (newSocketMsg[0] == (byte)'H' && newSocketMsg[1] == (byte)'\r')
            {
                if (nakHeartbeats) c.sendByte(SocketObj.NAK, "Nak", ackNakDelay);
                else               c.sendByte(SocketObj.ACK, "Ack", ackNakDelay);

                return 2;
            }

            // request
            bool validRequest = ! nakRequests;

            // verify carriage return
            if (validRequest)
            {
                if (length >= 14 && newSocketMsg[13] == '\r')
                {
                    length = 14;
                }
                else if (length >= 16 && newSocketMsg[15] == '\r')
                {
                    length = 16;
                }
                else
                {
                    warning("Request did not end with a carriage return");
                    validRequest = false;
                    length       = 0;
                }
            }
            else
            {
                length = 0;
            }

            if (validRequest)
            {
                // verify request check sum
                int checkSum = 0;

                for (int i = 0; i < (int)length - 1; i++) checkSum += newSocketMsg[i];

                if (checkSum % 8 != 0)
                {
                    warning("Request had an invalid check sum: " + checkSum.ToString() + " is not divisible by 8");
                    validRequest = false;
                }
            }

            // NAK
            if ( ! validRequest)
            {
                c.sendByte(SocketObj.NAK, "Nak", ackNakDelay);
                return (uint)length;
            }

            c.sendByte(SocketObj.ACK, "Ack", ackNakDelay);

            // if Independant_Links mode, don't wait for duplicates
            if (getSendMode() == SendMode.Independant_Links)
            {
                respondToRequest(newSocketMsg, length, requestDelay, getSendMode());
            }
            else
            {
                // if this is a new message (not a duplicate), wait 200ms to see if the duplicate arrives
                if ( ! isDuplicateMsgFromOtherLinks(newSocketMsg, (uint)length, c))
                {
                    originalMsgList.Add(new MessageInfo(this, newSocketMsg, length, requestDelay, c));
                }
                // else this is a duplicate message
                else
                {
                    // find and remove the message from the original message list
                    bool found    = false;
                    int  msgIndex = -1;

                    foreach (MessageInfo originalMsg in originalMsgList)
                    {
                        msgIndex++;

                        if (originalMsg.length != length) continue;

                        // compare messages byte by byte
                        ulong i = 0;

                        for (; i < length; i++)
                        {
                            if (originalMsg.msg[i] != newSocketMsg[i]) break;
                        }

                        if (i == length)
                        {
                            originalMsg.duplicateTimer.Stop();
                            originalMsgList.RemoveAt(msgIndex);
                            found = true;
                            break;
                        }
                    }

                    if ( ! found) warning("a duplicate message failed to match to a message in the Original Message List");

                    respondToRequest(newSocketMsg, length, requestDelay, getSendMode());
                }
            }

            return (uint)length;
        }


        /****************************************************************************************************
        * NAME: Method: void respondToRequest()
        *
        * DESCRIPTION: Respond to ALI DB request
        *  
        * PARAMETERS:
        *   param - desc TODO
        * 
        * VARIABLES:
        *   CheckBox                 Local   - desc
        * 
        * FUNCTIONS:
        *   Func()                   Library - <System.>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 30 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void respondToRequest(byte[] requestMsg, ulong length, int requestDelay, SendMode actualSendMode)
        {
            // parse request
            string requestStr = ASCIIEncoding.ASCII.GetString(requestMsg);
            string npaStr     = "";
            string nxxStr     = "";
            string tnStr      = "";
            int    positionId = 0;

            // NPD request
            if (length == 14)
            {
                // convert NPD to area code
                if      (requestStr[0] == '0') npaStr = npdSpinBox0.Value.ToString();
                else if (requestStr[0] == '1') npaStr = npdSpinBox1.Value.ToString();
                else if (requestStr[0] == '2') npaStr = npdSpinBox2.Value.ToString();
                else if (requestStr[0] == '3') npaStr = npdSpinBox3.Value.ToString();
                else
                {
                    warning("Found NPD number '" + requestStr[0] + "' (expected 0-3)");
                    npaStr = "[BAD_NPD_" + requestStr[0] + "]";
                }

                nxxStr = requestStr.Substring(1, 3);
                tnStr  = requestStr.Substring(4, 4);

                string positionStr = requestStr.Substring(8, 2);
                positionId = Convert.ToInt32(positionStr);
            }
            // else NPA request
            else
            {
                npaStr = requestStr.Substring( 0, 3);
                nxxStr = requestStr.Substring( 3, 3);
                tnStr  = requestStr.Substring( 6, 4);

                string positionStr = requestStr.Substring(10, 2);
                positionId = Convert.ToInt32(positionStr);
            }

            string requestNumber    = npaStr + "-" + nxxStr + "-" + tnStr;
            string requestNumberKey = npaStr + nxxStr + tnStr;

            // find the request in the database
            Queue<string> requestedRecordQueue;

            if (aliDatabase.TryGetValue(requestNumberKey, out requestedRecordQueue))
            {
                ALIMessageType aliMsgType = ALIMessageType.Broadcast;

                if (getNumSocketsConnected() == 2 &&
                    actualSendMode != SendMode.Link1_Down && actualSendMode != SendMode.Link2_Down)
                {
                    aliMsgType = ALIMessageType.Two_Paths_Up;
                }
                else if (getNumSocketsConnected() > 0)
                {
                    aliMsgType = ALIMessageType.One_Path_Up;
                }

                // cycle record queue
                string requestedRecord = requestedRecordQueue.Dequeue();
                requestedRecordQueue.Enqueue(requestedRecord);

                sendALIMessage(requestedRecord, aliMsgType, positionId, "Record " + requestNumber, requestDelay);
            }
            else
            {
                sendALIMessage(requestNumber + " No Record Found",
                    ALIMessageType.No_Record_Found, positionId, "No-Record-Found " + requestNumber, requestDelay);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void duplicateTimeout()
        *
        * DESCRIPTION: Timer for duplicate messages expired
        *  
        * VARIABLES:
        *   requestDelay             Local   - ms to delay response
        *   originalMsgDelay         Local   - original msg delay
        *   rememberedSendMode       Local   - current send mode
        *   originalMsgCommWidget    Local   - the comm widget the original msg came in on
        *   originalMsg              Local   - original msg
        *   originalMsgLength        Local   - original msg length
        * 
        * FUNCTIONS:
        *   Stop()                   Library - <System.Windows.Forms.Timer>
        *   getSendMode()            Library - <TesterMainWindow>
        *   setSendMode()            Library - <TesterMainWindow>
        *   setCurCommWidget()       Library - <TesterMainWindow>
        *   respondToRequest()       Local   - Respond to ALI DB request
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 30 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void duplicateTimeout(MessageInfo originalMsg)
        {
            int requestDelay = originalMsg.delay - originalMsg.duplicateTimer.Interval;
            if (requestDelay < 0) requestDelay = 0;

            // remember send mode
            SendMode rememberedSendMode = getSendMode();

            // respond to original msg on original comm widget
            setSendMode(SendMode.Independant_Links);
            setCurCommWidget(originalMsg.commWidget.getId());
            respondToRequest(originalMsg.msg, originalMsg.length, requestDelay, rememberedSendMode);

            // restore send mode
            setSendMode(rememberedSendMode);

            // find and remove the message from the original message list
            originalMsgList.Remove(originalMsg);
        }


        /****************************************************************************************************
        * NAME: Method: void sendALIMessage()
        *
        * DESCRIPTION: Send an ALI message
        *  
        * PARAMETERS:
        *   content    - body of the message to send
        *   addSTX     - specifies whether to add an STX to the front
        *   addETX     - specifies whether to add an ETX to the end
        *   type       - ALI state or type (ONE_PATH_UP, Two_Paths_Up, Broadcast, Broadcast_Stop, No_Record_Found)
        *   positionId - position ID passed in with the request
        *   desc       - message description
        *   delayMSec  - optional delay for send
        * 
        * VARIABLES:
        *   msg                      Local   - complete message to be sent
        * 
        * FUNCTIONS:
        *   ToString()               Library - <System.Int32>
        *   sendString()             Library - <TesterMainWindow>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void sendALIMessage(string content, ALIMessageType type, int positionId, string desc, int delayMSec)
        {
            sendALIMessage(content, true, true, type, positionId, desc, delayMSec);
        }

        private void sendALIMessage(
            string         content,
            bool           addSTX,
            bool           addETX,
            ALIMessageType type,
            int            positionId,
            string         desc,
            int            delayMSec
        )
        {
            string aliMsgTypeAndPosStr = ((int)type).ToString();

            if (positionId <= 9) aliMsgTypeAndPosStr += 0;
            aliMsgTypeAndPosStr += positionId.ToString();

            sendALIMessage(content, addSTX, addETX, aliMsgTypeAndPosStr, desc, delayMSec);
        }

        private void sendALIMessage(
            string         content,
            bool           addSTX,
            bool           addETX,
            string         aliMsgTypeAndPosStr,
            string         desc,
            int            delayMSec
        )
        {
            string msg = "";

            // STX
            if (addSTX) msg += (char)SocketObj.STX;

            // type and position
            msg += aliMsgTypeAndPosStr;

            // content
            msg += content;

            // ETX
            if (addETX) msg += (char)SocketObj.ETX;

            sendString(msg, desc, delayMSec);
        }


        /****************************************************************************************************
        * NAME: Method: void setSendMode()
        *
        * DESCRIPTION: Sets the send mode based on the sendModeComboBox
        *  
        * VARIABLES:
        *   sendModeComboBox         Local   - TODO:
        *   link1Enabled             Local   - 
        *   link2Enabled             Local   - 
        *   link1GroupBox            Local   - 
        *   link2GroupBox            Local   - 
        *   transmitRecordButton     Local   - 
        *   link1TransmitButton      Local   - 
        *   link2TransmitButton      Local   - 
        *   allTransmitButton        Local   - 
        *   dosLink1Button           Local   - 
        *   dosLink2Button           Local   - 
        *   dosBothButton            Local   - 
        * 
        * FUNCTIONS:
        *   setSendMode()            Library - <TesterMainWindow>
        *   getSendMode()            Library - <TesterMainWindow>
        *   isRunning()              Library - <TesterMainWindow>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 11 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void setSendMode(object unusedObj, EventArgs unusedArgs)
        {
            setSendMode((SendMode)sendModeComboBox.SelectedIndex);

            bool link1Enabled = (getSendMode() != SendMode.Link1_Down && getSendMode() != SendMode.Both_Down);
            bool link2Enabled = (getSendMode() != SendMode.Link2_Down && getSendMode() != SendMode.Both_Down);

            link1GroupBox.Enabled = link1Enabled;
            link2GroupBox.Enabled = link2Enabled;

            goingOutOfServiceButton.Enabled = ((link1Enabled || link2Enabled) && isRunning());
            transmitRecordButton.   Enabled = ((link1Enabled || link2Enabled) && isRunning());

            link1TransmitButton.Enabled = (link1Enabled && isRunning());
            link2TransmitButton.Enabled = (link2Enabled && isRunning());

            allTransmitButton.Enabled = (link1Enabled && link2Enabled && isRunning());

            // dos
            dosLink1Button. Enabled = (link1Enabled && isRunning());
            dosLink2Button. Enabled = (link2Enabled && isRunning());
            dosBothButton.  Enabled = (link1Enabled && link2Enabled && isRunning());
        }


        // private data members
        private string   alidbFilename                       = "data\\alidb.dat";
        private Dictionary<string,Queue<string>> aliDatabase = new Dictionary<string,Queue<string>>();
        private int      recordCount;

        private Button   goingOutOfServiceButton        = new Button();
        private SendMode dosSendMode                    = SendMode.Alternate_Links;

        private ToolStripStatusLabel statusBarLabel     = null;
        private ToolStripStatusLabel countStatusLabel   = null;
        private CheckBox             advancedCheckBox   = null;


        private class MessageInfo
        {
            public MessageInfo(
                MainWindow _mainWindow,
                byte[]     _msg,
                ulong      _length,
                int        _delay,
                CommWidget _commWidget
            )
            {
                msg        = _msg;
                length     = _length;
                delay      = _delay;
                commWidget = _commWidget;
                mainWindow = _mainWindow;

                duplicateTimer.Tick    += timeout;
                duplicateTimer.Interval = 200;
                duplicateTimer.Start();
            }

            private void timeout(object unusedObject, EventArgs unusedArgs)
            {
                duplicateTimer.Stop();
                mainWindow.duplicateTimeout(this);
            }

            public  Timer      duplicateTimer = new Timer();
            public  byte[]     msg            = null;
            public  ulong      length         = 0;
            public  int        delay          = 0;
            public  CommWidget commWidget     = null;
            private MainWindow mainWindow     = null;
        }

        List<MessageInfo> originalMsgList = new List<MessageInfo>();
    }
}
