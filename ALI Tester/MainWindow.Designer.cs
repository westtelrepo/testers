
/*****************************************************************************
* FILE: MainWindow.Designer.cs
*  
* DESCRIPTION: ALI Main Window (auto-generated)
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 30 Aug 2007 Jesse Elliott
******************************************************************************/

namespace ALITester
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.splitter = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.recordSplitter = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.aliDatabaseGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.databaseFileLabel = new System.Windows.Forms.Label();
            this.databaseFileButton = new System.Windows.Forms.Button();
            this.recordComboBox = new System.Windows.Forms.ComboBox();
            this.recordTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.clearRecordButton = new System.Windows.Forms.Button();
            this.addRecordButton = new System.Windows.Forms.Button();
            this.deleteRecordButton = new System.Windows.Forms.Button();
            this.saveDatabaseButton = new System.Windows.Forms.Button();
            this.stxCheckBox = new System.Windows.Forms.CheckBox();
            this.etxCheckBox = new System.Windows.Forms.CheckBox();
            this.transmitRecordButton = new System.Windows.Forms.Button();
            this.transmitPosAndTypeTextBox = new System.Windows.Forms.TextBox();
            this.transmitPosAndTypeCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.broadcastGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.broadcastTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.allTransmitButton = new System.Windows.Forms.Button();
            this.link1TransmitButton = new System.Windows.Forms.Button();
            this.link2TransmitButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.clearBroadcastButton = new System.Windows.Forms.Button();
            this.aliOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.sendModeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.link2GroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.nakRequests2CheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.nakHeartbeats2CheckBox = new System.Windows.Forms.CheckBox();
            this.delayAckNak2SpinBox = new System.Windows.Forms.NumericUpDown();
            this.delayRequest2SpinBox = new System.Windows.Forms.NumericUpDown();
            this.link1GroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nakHeartbeats1CheckBox = new System.Windows.Forms.CheckBox();
            this.nakRequests1CheckBox = new System.Windows.Forms.CheckBox();
            this.delayRequest1SpinBox = new System.Windows.Forms.NumericUpDown();
            this.delayAckNak1SpinBox = new System.Windows.Forms.NumericUpDown();
            this.npdGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.npdLabel3 = new System.Windows.Forms.Label();
            this.npdLabel2 = new System.Windows.Forms.Label();
            this.npdLabel1 = new System.Windows.Forms.Label();
            this.npdLabel0 = new System.Windows.Forms.Label();
            this.npdSpinBox3 = new System.Windows.Forms.NumericUpDown();
            this.npdSpinBox2 = new System.Windows.Forms.NumericUpDown();
            this.npdSpinBox1 = new System.Windows.Forms.NumericUpDown();
            this.npdSpinBox0 = new System.Windows.Forms.NumericUpDown();
            this.dosGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.dosFileButton = new System.Windows.Forms.Button();
            this.dosFileLabel = new System.Windows.Forms.Label();
            this.dosLabel = new System.Windows.Forms.Label();
            this.dosSpinBox = new System.Windows.Forms.NumericUpDown();
            this.dosLink1Button = new System.Windows.Forms.Button();
            this.dosLink2Button = new System.Windows.Forms.Button();
            this.dosBothButton = new System.Windows.Forms.Button();
            this.mainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).BeginInit();
            this.splitter.Panel1.SuspendLayout();
            this.splitter.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recordSplitter)).BeginInit();
            this.recordSplitter.Panel1.SuspendLayout();
            this.recordSplitter.Panel2.SuspendLayout();
            this.recordSplitter.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.aliDatabaseGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.broadcastGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.aliOptionsGroupBox.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.link2GroupBox.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayAckNak2SpinBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayRequest2SpinBox)).BeginInit();
            this.link1GroupBox.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayRequest1SpinBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayAckNak1SpinBox)).BeginInit();
            this.npdGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox0)).BeginInit();
            this.dosGroupBox.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dosSpinBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.ColumnCount = 1;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Controls.Add(this.splitter, 0, 0);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 0);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 1;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Size = new System.Drawing.Size(784, 422);
            this.mainLayout.TabIndex = 0;
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitter.Location = new System.Drawing.Point(3, 3);
            this.splitter.Name = "splitter";
            this.splitter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitter.Panel1
            // 
            this.splitter.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitter.Size = new System.Drawing.Size(778, 416);
            this.splitter.SplitterDistance = 365;
            this.splitter.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.recordSplitter, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.npdGroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dosGroupBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(778, 365);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // recordSplitter
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.recordSplitter, 2);
            this.recordSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recordSplitter.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.recordSplitter.Location = new System.Drawing.Point(3, 57);
            this.recordSplitter.Name = "recordSplitter";
            // 
            // recordSplitter.Panel1
            // 
            this.recordSplitter.Panel1.Controls.Add(this.tableLayoutPanel7);
            // 
            // recordSplitter.Panel2
            // 
            this.recordSplitter.Panel2.Controls.Add(this.tableLayoutPanel6);
            this.recordSplitter.Size = new System.Drawing.Size(772, 305);
            this.recordSplitter.SplitterDistance = 294;
            this.recordSplitter.TabIndex = 1;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.AutoSize = true;
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.aliDatabaseGroupBox, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(294, 305);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // aliDatabaseGroupBox
            // 
            this.aliDatabaseGroupBox.AutoSize = true;
            this.aliDatabaseGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel7.SetColumnSpan(this.aliDatabaseGroupBox, 2);
            this.aliDatabaseGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.aliDatabaseGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aliDatabaseGroupBox.Location = new System.Drawing.Point(3, 3);
            this.aliDatabaseGroupBox.Name = "aliDatabaseGroupBox";
            this.aliDatabaseGroupBox.Size = new System.Drawing.Size(288, 299);
            this.aliDatabaseGroupBox.TabIndex = 0;
            this.aliDatabaseGroupBox.TabStop = false;
            this.aliDatabaseGroupBox.Text = "ALI Database";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.databaseFileLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.databaseFileButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.recordComboBox, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.recordTextBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(282, 280);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // databaseFileLabel
            // 
            this.databaseFileLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.databaseFileLabel.AutoSize = true;
            this.databaseFileLabel.Location = new System.Drawing.Point(3, 8);
            this.databaseFileLabel.Name = "databaseFileLabel";
            this.databaseFileLabel.Size = new System.Drawing.Size(47, 13);
            this.databaseFileLabel.TabIndex = 10;
            this.databaseFileLabel.Text = "alidb.dat";
            // 
            // databaseFileButton
            // 
            this.databaseFileButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.databaseFileButton.AutoSize = true;
            this.databaseFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.databaseFileButton.Location = new System.Drawing.Point(56, 3);
            this.databaseFileButton.Name = "databaseFileButton";
            this.databaseFileButton.Size = new System.Drawing.Size(26, 23);
            this.databaseFileButton.TabIndex = 11;
            this.databaseFileButton.Text = "...";
            this.databaseFileButton.UseVisualStyleBackColor = true;
            // 
            // recordComboBox
            // 
            this.recordComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.recordComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.recordComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.recordComboBox.Location = new System.Drawing.Point(158, 4);
            this.recordComboBox.MaxDropDownItems = 20;
            this.recordComboBox.Name = "recordComboBox";
            this.recordComboBox.Size = new System.Drawing.Size(121, 21);
            this.recordComboBox.TabIndex = 12;
            // 
            // recordTextBox
            // 
            this.recordTextBox.AcceptsReturn = true;
            this.recordTextBox.AllowDrop = true;
            this.tableLayoutPanel2.SetColumnSpan(this.recordTextBox, 3);
            this.recordTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recordTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recordTextBox.Location = new System.Drawing.Point(3, 32);
            this.recordTextBox.MaxLength = 2147483647;
            this.recordTextBox.Multiline = true;
            this.recordTextBox.Name = "recordTextBox";
            this.recordTextBox.Size = new System.Drawing.Size(276, 184);
            this.recordTextBox.TabIndex = 0;
            this.recordTextBox.WordWrap = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel5, 3);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.clearRecordButton, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.addRecordButton, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.deleteRecordButton, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.saveDatabaseButton, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.stxCheckBox, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.etxCheckBox, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.transmitRecordButton, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.transmitPosAndTypeTextBox, 5, 1);
            this.tableLayoutPanel5.Controls.Add(this.transmitPosAndTypeCheckBox, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 222);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(276, 55);
            this.tableLayoutPanel5.TabIndex = 13;
            // 
            // clearRecordButton
            // 
            this.clearRecordButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clearRecordButton.AutoSize = true;
            this.clearRecordButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clearRecordButton.Location = new System.Drawing.Point(110, 3);
            this.clearRecordButton.Name = "clearRecordButton";
            this.clearRecordButton.Size = new System.Drawing.Size(41, 23);
            this.clearRecordButton.TabIndex = 5;
            this.clearRecordButton.Text = "Clear";
            this.clearRecordButton.UseVisualStyleBackColor = true;
            // 
            // addRecordButton
            // 
            this.addRecordButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.addRecordButton.AutoSize = true;
            this.addRecordButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addRecordButton.Location = new System.Drawing.Point(8, 3);
            this.addRecordButton.Name = "addRecordButton";
            this.addRecordButton.Size = new System.Drawing.Size(36, 23);
            this.addRecordButton.TabIndex = 1;
            this.addRecordButton.Text = "Add";
            this.addRecordButton.UseVisualStyleBackColor = true;
            // 
            // deleteRecordButton
            // 
            this.deleteRecordButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deleteRecordButton.AutoSize = true;
            this.deleteRecordButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteRecordButton.Location = new System.Drawing.Point(56, 3);
            this.deleteRecordButton.Name = "deleteRecordButton";
            this.deleteRecordButton.Size = new System.Drawing.Size(48, 23);
            this.deleteRecordButton.TabIndex = 3;
            this.deleteRecordButton.Text = "Delete";
            this.deleteRecordButton.UseVisualStyleBackColor = true;
            // 
            // saveDatabaseButton
            // 
            this.saveDatabaseButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.saveDatabaseButton.AutoSize = true;
            this.saveDatabaseButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.saveDatabaseButton.Location = new System.Drawing.Point(157, 3);
            this.saveDatabaseButton.Name = "saveDatabaseButton";
            this.saveDatabaseButton.Size = new System.Drawing.Size(42, 23);
            this.saveDatabaseButton.TabIndex = 4;
            this.saveDatabaseButton.Text = "Save";
            this.saveDatabaseButton.UseVisualStyleBackColor = true;
            // 
            // stxCheckBox
            // 
            this.stxCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.stxCheckBox.AutoSize = true;
            this.stxCheckBox.Checked = true;
            this.stxCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.stxCheckBox.Location = new System.Drawing.Point(3, 33);
            this.stxCheckBox.Name = "stxCheckBox";
            this.stxCheckBox.Size = new System.Drawing.Size(47, 17);
            this.stxCheckBox.TabIndex = 6;
            this.stxCheckBox.Text = "STX";
            this.stxCheckBox.UseVisualStyleBackColor = true;
            // 
            // etxCheckBox
            // 
            this.etxCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.etxCheckBox.AutoSize = true;
            this.etxCheckBox.Checked = true;
            this.etxCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.etxCheckBox.Location = new System.Drawing.Point(56, 33);
            this.etxCheckBox.Name = "etxCheckBox";
            this.etxCheckBox.Size = new System.Drawing.Size(47, 17);
            this.etxCheckBox.TabIndex = 7;
            this.etxCheckBox.Text = "ETX";
            this.etxCheckBox.UseVisualStyleBackColor = true;
            // 
            // transmitRecordButton
            // 
            this.transmitRecordButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.transmitRecordButton.AutoSize = true;
            this.transmitRecordButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.SetColumnSpan(this.transmitRecordButton, 2);
            this.transmitRecordButton.Location = new System.Drawing.Point(216, 3);
            this.transmitRecordButton.Name = "transmitRecordButton";
            this.transmitRecordButton.Size = new System.Drawing.Size(57, 23);
            this.transmitRecordButton.TabIndex = 2;
            this.transmitRecordButton.Text = "Transmit";
            this.transmitRecordButton.UseVisualStyleBackColor = true;
            // 
            // transmitPosAndTypeTextBox
            // 
            this.transmitPosAndTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.transmitPosAndTypeTextBox.Enabled = false;
            this.transmitPosAndTypeTextBox.Location = new System.Drawing.Point(238, 32);
            this.transmitPosAndTypeTextBox.MaxLength = 3;
            this.transmitPosAndTypeTextBox.Name = "transmitPosAndTypeTextBox";
            this.transmitPosAndTypeTextBox.Size = new System.Drawing.Size(35, 20);
            this.transmitPosAndTypeTextBox.TabIndex = 10;
            this.transmitPosAndTypeTextBox.Text = "xxx";
            this.transmitPosAndTypeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.transmitPosAndTypeTextBox.WordWrap = false;
            // 
            // transmitPosAndTypeCheckBox
            // 
            this.transmitPosAndTypeCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.transmitPosAndTypeCheckBox.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.transmitPosAndTypeCheckBox, 3);
            this.transmitPosAndTypeCheckBox.Location = new System.Drawing.Point(140, 33);
            this.transmitPosAndTypeCheckBox.Name = "transmitPosAndTypeCheckBox";
            this.transmitPosAndTypeCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.transmitPosAndTypeCheckBox.Size = new System.Drawing.Size(92, 17);
            this.transmitPosAndTypeCheckBox.TabIndex = 11;
            this.transmitPosAndTypeCheckBox.Text = "Type and Pos";
            this.transmitPosAndTypeCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.transmitPosAndTypeCheckBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.broadcastGroupBox, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.aliOptionsGroupBox, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(474, 305);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // broadcastGroupBox
            // 
            this.broadcastGroupBox.AutoSize = true;
            this.broadcastGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.broadcastGroupBox.Controls.Add(this.tableLayoutPanel3);
            this.broadcastGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.broadcastGroupBox.Location = new System.Drawing.Point(3, 178);
            this.broadcastGroupBox.Name = "broadcastGroupBox";
            this.broadcastGroupBox.Size = new System.Drawing.Size(468, 124);
            this.broadcastGroupBox.TabIndex = 0;
            this.broadcastGroupBox.TabStop = false;
            this.broadcastGroupBox.Text = "Broadcast";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.broadcastTextBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(462, 105);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // broadcastTextBox
            // 
            this.broadcastTextBox.AcceptsReturn = true;
            this.broadcastTextBox.AllowDrop = true;
            this.broadcastTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.broadcastTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.broadcastTextBox.Location = new System.Drawing.Point(3, 3);
            this.broadcastTextBox.MaxLength = 2147483647;
            this.broadcastTextBox.Multiline = true;
            this.broadcastTextBox.Name = "broadcastTextBox";
            this.broadcastTextBox.Size = new System.Drawing.Size(456, 64);
            this.broadcastTextBox.TabIndex = 0;
            this.broadcastTextBox.WordWrap = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.AutoSize = true;
            this.tableLayoutPanel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel10.ColumnCount = 6;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.Controls.Add(this.allTransmitButton, 5, 0);
            this.tableLayoutPanel10.Controls.Add(this.link1TransmitButton, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.link2TransmitButton, 4, 0);
            this.tableLayoutPanel10.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.clearBroadcastButton, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 73);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.Size = new System.Drawing.Size(456, 29);
            this.tableLayoutPanel10.TabIndex = 7;
            // 
            // allTransmitButton
            // 
            this.allTransmitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.allTransmitButton.AutoSize = true;
            this.allTransmitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.allTransmitButton.Location = new System.Drawing.Point(414, 3);
            this.allTransmitButton.Name = "allTransmitButton";
            this.allTransmitButton.Size = new System.Drawing.Size(39, 23);
            this.allTransmitButton.TabIndex = 1;
            this.allTransmitButton.Text = "Both";
            this.allTransmitButton.UseVisualStyleBackColor = true;
            // 
            // link1TransmitButton
            // 
            this.link1TransmitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.link1TransmitButton.AutoSize = true;
            this.link1TransmitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.link1TransmitButton.Location = new System.Drawing.Point(316, 3);
            this.link1TransmitButton.Name = "link1TransmitButton";
            this.link1TransmitButton.Size = new System.Drawing.Size(43, 23);
            this.link1TransmitButton.TabIndex = 7;
            this.link1TransmitButton.Text = "Link1";
            this.link1TransmitButton.UseVisualStyleBackColor = true;
            // 
            // link2TransmitButton
            // 
            this.link2TransmitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.link2TransmitButton.AutoSize = true;
            this.link2TransmitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.link2TransmitButton.Location = new System.Drawing.Point(365, 3);
            this.link2TransmitButton.Name = "link2TransmitButton";
            this.link2TransmitButton.Size = new System.Drawing.Size(43, 23);
            this.link2TransmitButton.TabIndex = 8;
            this.link2TransmitButton.Text = "Link2";
            this.link2TransmitButton.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Transmit On";
            // 
            // clearBroadcastButton
            // 
            this.clearBroadcastButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.clearBroadcastButton.AutoSize = true;
            this.clearBroadcastButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clearBroadcastButton.Location = new System.Drawing.Point(3, 3);
            this.clearBroadcastButton.Name = "clearBroadcastButton";
            this.clearBroadcastButton.Size = new System.Drawing.Size(41, 23);
            this.clearBroadcastButton.TabIndex = 6;
            this.clearBroadcastButton.Text = "Clear";
            this.clearBroadcastButton.UseVisualStyleBackColor = true;
            // 
            // aliOptionsGroupBox
            // 
            this.aliOptionsGroupBox.AutoSize = true;
            this.aliOptionsGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.aliOptionsGroupBox.Controls.Add(this.tableLayoutPanel8);
            this.aliOptionsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aliOptionsGroupBox.Location = new System.Drawing.Point(3, 3);
            this.aliOptionsGroupBox.Name = "aliOptionsGroupBox";
            this.aliOptionsGroupBox.Size = new System.Drawing.Size(468, 169);
            this.aliOptionsGroupBox.TabIndex = 1;
            this.aliOptionsGroupBox.TabStop = false;
            this.aliOptionsGroupBox.Text = "ALI Options";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.AutoSize = true;
            this.tableLayoutPanel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.Controls.Add(this.sendModeComboBox, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.link2GroupBox, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.link1GroupBox, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.Size = new System.Drawing.Size(462, 150);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // sendModeComboBox
            // 
            this.sendModeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.sendModeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.sendModeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tableLayoutPanel8.SetColumnSpan(this.sendModeComboBox, 3);
            this.sendModeComboBox.Location = new System.Drawing.Point(71, 3);
            this.sendModeComboBox.MaxDropDownItems = 20;
            this.sendModeComboBox.Name = "sendModeComboBox";
            this.sendModeComboBox.Size = new System.Drawing.Size(133, 21);
            this.sendModeComboBox.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Send Mode";
            // 
            // link2GroupBox
            // 
            this.link2GroupBox.AutoSize = true;
            this.link2GroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.SetColumnSpan(this.link2GroupBox, 2);
            this.link2GroupBox.Controls.Add(this.tableLayoutPanel12);
            this.link2GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.link2GroupBox.Location = new System.Drawing.Point(216, 30);
            this.link2GroupBox.Name = "link2GroupBox";
            this.link2GroupBox.Size = new System.Drawing.Size(243, 117);
            this.link2GroupBox.TabIndex = 20;
            this.link2GroupBox.TabStop = false;
            this.link2GroupBox.Text = "Link2";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.AutoSize = true;
            this.tableLayoutPanel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.Controls.Add(this.nakRequests2CheckBox, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.nakHeartbeats2CheckBox, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.delayAckNak2SpinBox, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.delayRequest2SpinBox, 1, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 4;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.Size = new System.Drawing.Size(237, 98);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // nakRequests2CheckBox
            // 
            this.nakRequests2CheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nakRequests2CheckBox.AutoSize = true;
            this.nakRequests2CheckBox.Location = new System.Drawing.Point(3, 78);
            this.nakRequests2CheckBox.Name = "nakRequests2CheckBox";
            this.nakRequests2CheckBox.Size = new System.Drawing.Size(109, 17);
            this.nakRequests2CheckBox.TabIndex = 19;
            this.nakRequests2CheckBox.Text = "NAK all Requests";
            this.nakRequests2CheckBox.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Delay ACK/NAK (sec)";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Delay after Request (sec)";
            // 
            // nakHeartbeats2CheckBox
            // 
            this.nakHeartbeats2CheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nakHeartbeats2CheckBox.AutoSize = true;
            this.nakHeartbeats2CheckBox.Location = new System.Drawing.Point(3, 55);
            this.nakHeartbeats2CheckBox.Name = "nakHeartbeats2CheckBox";
            this.nakHeartbeats2CheckBox.Size = new System.Drawing.Size(116, 17);
            this.nakHeartbeats2CheckBox.TabIndex = 18;
            this.nakHeartbeats2CheckBox.Text = "NAK all Heartbeats";
            this.nakHeartbeats2CheckBox.UseVisualStyleBackColor = true;
            // 
            // delayAckNak2SpinBox
            // 
            this.delayAckNak2SpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delayAckNak2SpinBox.AutoSize = true;
            this.delayAckNak2SpinBox.DecimalPlaces = 1;
            this.delayAckNak2SpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.delayAckNak2SpinBox.Location = new System.Drawing.Point(136, 3);
            this.delayAckNak2SpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.delayAckNak2SpinBox.Name = "delayAckNak2SpinBox";
            this.delayAckNak2SpinBox.Size = new System.Drawing.Size(62, 20);
            this.delayAckNak2SpinBox.TabIndex = 21;
            this.delayAckNak2SpinBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // delayRequest2SpinBox
            // 
            this.delayRequest2SpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delayRequest2SpinBox.AutoSize = true;
            this.delayRequest2SpinBox.DecimalPlaces = 1;
            this.delayRequest2SpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.delayRequest2SpinBox.Location = new System.Drawing.Point(136, 29);
            this.delayRequest2SpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.delayRequest2SpinBox.Name = "delayRequest2SpinBox";
            this.delayRequest2SpinBox.Size = new System.Drawing.Size(62, 20);
            this.delayRequest2SpinBox.TabIndex = 20;
            this.delayRequest2SpinBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // link1GroupBox
            // 
            this.link1GroupBox.AutoSize = true;
            this.link1GroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.SetColumnSpan(this.link1GroupBox, 2);
            this.link1GroupBox.Controls.Add(this.tableLayoutPanel11);
            this.link1GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.link1GroupBox.Location = new System.Drawing.Point(3, 30);
            this.link1GroupBox.Name = "link1GroupBox";
            this.link1GroupBox.Size = new System.Drawing.Size(207, 117);
            this.link1GroupBox.TabIndex = 21;
            this.link1GroupBox.TabStop = false;
            this.link1GroupBox.Text = "Link1";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.AutoSize = true;
            this.tableLayoutPanel11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.nakHeartbeats1CheckBox, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.nakRequests1CheckBox, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.delayRequest1SpinBox, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.delayAckNak1SpinBox, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 4;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.Size = new System.Drawing.Size(201, 98);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Delay ACK/NAK (sec)";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Delay after Request (sec)";
            // 
            // nakHeartbeats1CheckBox
            // 
            this.nakHeartbeats1CheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nakHeartbeats1CheckBox.AutoSize = true;
            this.nakHeartbeats1CheckBox.Location = new System.Drawing.Point(3, 55);
            this.nakHeartbeats1CheckBox.Name = "nakHeartbeats1CheckBox";
            this.nakHeartbeats1CheckBox.Size = new System.Drawing.Size(116, 17);
            this.nakHeartbeats1CheckBox.TabIndex = 16;
            this.nakHeartbeats1CheckBox.Text = "NAK all Heartbeats";
            this.nakHeartbeats1CheckBox.UseVisualStyleBackColor = true;
            // 
            // nakRequests1CheckBox
            // 
            this.nakRequests1CheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nakRequests1CheckBox.AutoSize = true;
            this.nakRequests1CheckBox.Location = new System.Drawing.Point(3, 78);
            this.nakRequests1CheckBox.Name = "nakRequests1CheckBox";
            this.nakRequests1CheckBox.Size = new System.Drawing.Size(109, 17);
            this.nakRequests1CheckBox.TabIndex = 17;
            this.nakRequests1CheckBox.Text = "NAK all Requests";
            this.nakRequests1CheckBox.UseVisualStyleBackColor = true;
            // 
            // delayRequest1SpinBox
            // 
            this.delayRequest1SpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delayRequest1SpinBox.AutoSize = true;
            this.delayRequest1SpinBox.DecimalPlaces = 1;
            this.delayRequest1SpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.delayRequest1SpinBox.Location = new System.Drawing.Point(136, 29);
            this.delayRequest1SpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.delayRequest1SpinBox.Name = "delayRequest1SpinBox";
            this.delayRequest1SpinBox.Size = new System.Drawing.Size(62, 20);
            this.delayRequest1SpinBox.TabIndex = 18;
            this.delayRequest1SpinBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // delayAckNak1SpinBox
            // 
            this.delayAckNak1SpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delayAckNak1SpinBox.AutoSize = true;
            this.delayAckNak1SpinBox.DecimalPlaces = 1;
            this.delayAckNak1SpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.delayAckNak1SpinBox.Location = new System.Drawing.Point(136, 3);
            this.delayAckNak1SpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.delayAckNak1SpinBox.Name = "delayAckNak1SpinBox";
            this.delayAckNak1SpinBox.Size = new System.Drawing.Size(62, 20);
            this.delayAckNak1SpinBox.TabIndex = 19;
            this.delayAckNak1SpinBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // npdGroupBox
            // 
            this.npdGroupBox.AutoSize = true;
            this.npdGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.npdGroupBox.Controls.Add(this.tableLayoutPanel4);
            this.npdGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.npdGroupBox.Location = new System.Drawing.Point(3, 3);
            this.npdGroupBox.Name = "npdGroupBox";
            this.npdGroupBox.Size = new System.Drawing.Size(234, 48);
            this.npdGroupBox.TabIndex = 3;
            this.npdGroupBox.TabStop = false;
            this.npdGroupBox.Text = "NPDs";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 9;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.npdLabel3, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdLabel2, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdLabel1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdLabel0, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdSpinBox3, 7, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdSpinBox2, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdSpinBox1, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.npdSpinBox0, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(228, 29);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // npdLabel3
            // 
            this.npdLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdLabel3.AutoSize = true;
            this.npdLabel3.Location = new System.Drawing.Point(174, 8);
            this.npdLabel3.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.npdLabel3.Name = "npdLabel3";
            this.npdLabel3.Size = new System.Drawing.Size(13, 13);
            this.npdLabel3.TabIndex = 12;
            this.npdLabel3.Text = "3";
            // 
            // npdLabel2
            // 
            this.npdLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdLabel2.AutoSize = true;
            this.npdLabel2.Location = new System.Drawing.Point(117, 8);
            this.npdLabel2.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.npdLabel2.Name = "npdLabel2";
            this.npdLabel2.Size = new System.Drawing.Size(13, 13);
            this.npdLabel2.TabIndex = 11;
            this.npdLabel2.Text = "2";
            // 
            // npdLabel1
            // 
            this.npdLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdLabel1.AutoSize = true;
            this.npdLabel1.Location = new System.Drawing.Point(60, 8);
            this.npdLabel1.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.npdLabel1.Name = "npdLabel1";
            this.npdLabel1.Size = new System.Drawing.Size(13, 13);
            this.npdLabel1.TabIndex = 10;
            this.npdLabel1.Text = "1";
            // 
            // npdLabel0
            // 
            this.npdLabel0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdLabel0.AutoSize = true;
            this.npdLabel0.Location = new System.Drawing.Point(3, 8);
            this.npdLabel0.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.npdLabel0.Name = "npdLabel0";
            this.npdLabel0.Size = new System.Drawing.Size(13, 13);
            this.npdLabel0.TabIndex = 9;
            this.npdLabel0.Text = "0";
            // 
            // npdSpinBox3
            // 
            this.npdSpinBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdSpinBox3.AutoSize = true;
            this.npdSpinBox3.Location = new System.Drawing.Point(187, 4);
            this.npdSpinBox3.Margin = new System.Windows.Forms.Padding(0);
            this.npdSpinBox3.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.npdSpinBox3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.npdSpinBox3.Name = "npdSpinBox3";
            this.npdSpinBox3.Size = new System.Drawing.Size(41, 20);
            this.npdSpinBox3.TabIndex = 8;
            this.npdSpinBox3.Value = new decimal(new int[] {
            555,
            0,
            0,
            0});
            // 
            // npdSpinBox2
            // 
            this.npdSpinBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdSpinBox2.AutoSize = true;
            this.npdSpinBox2.Location = new System.Drawing.Point(130, 4);
            this.npdSpinBox2.Margin = new System.Windows.Forms.Padding(0);
            this.npdSpinBox2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.npdSpinBox2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.npdSpinBox2.Name = "npdSpinBox2";
            this.npdSpinBox2.Size = new System.Drawing.Size(41, 20);
            this.npdSpinBox2.TabIndex = 7;
            this.npdSpinBox2.Value = new decimal(new int[] {
            555,
            0,
            0,
            0});
            // 
            // npdSpinBox1
            // 
            this.npdSpinBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdSpinBox1.AutoSize = true;
            this.npdSpinBox1.Location = new System.Drawing.Point(73, 4);
            this.npdSpinBox1.Margin = new System.Windows.Forms.Padding(0);
            this.npdSpinBox1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.npdSpinBox1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.npdSpinBox1.Name = "npdSpinBox1";
            this.npdSpinBox1.Size = new System.Drawing.Size(41, 20);
            this.npdSpinBox1.TabIndex = 6;
            this.npdSpinBox1.Value = new decimal(new int[] {
            555,
            0,
            0,
            0});
            // 
            // npdSpinBox0
            // 
            this.npdSpinBox0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.npdSpinBox0.AutoSize = true;
            this.npdSpinBox0.Location = new System.Drawing.Point(16, 4);
            this.npdSpinBox0.Margin = new System.Windows.Forms.Padding(0);
            this.npdSpinBox0.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.npdSpinBox0.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.npdSpinBox0.Name = "npdSpinBox0";
            this.npdSpinBox0.Size = new System.Drawing.Size(41, 20);
            this.npdSpinBox0.TabIndex = 5;
            this.npdSpinBox0.Value = new decimal(new int[] {
            555,
            0,
            0,
            0});
            // 
            // dosGroupBox
            // 
            this.dosGroupBox.AutoSize = true;
            this.dosGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosGroupBox.Controls.Add(this.tableLayoutPanel9);
            this.dosGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dosGroupBox.Location = new System.Drawing.Point(243, 3);
            this.dosGroupBox.Name = "dosGroupBox";
            this.dosGroupBox.Size = new System.Drawing.Size(532, 48);
            this.dosGroupBox.TabIndex = 4;
            this.dosGroupBox.TabStop = false;
            this.dosGroupBox.Text = "Denial of Service (DOS)";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel9.ColumnCount = 8;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.Controls.Add(this.dosFileButton, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosFileLabel, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosLabel, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosSpinBox, 4, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosLink1Button, 5, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosLink2Button, 6, 0);
            this.tableLayoutPanel9.Controls.Add(this.dosBothButton, 7, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(526, 29);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // dosFileButton
            // 
            this.dosFileButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosFileButton.AutoSize = true;
            this.dosFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosFileButton.Location = new System.Drawing.Point(47, 3);
            this.dosFileButton.Name = "dosFileButton";
            this.dosFileButton.Size = new System.Drawing.Size(26, 23);
            this.dosFileButton.TabIndex = 4;
            this.dosFileButton.Text = "...";
            // 
            // dosFileLabel
            // 
            this.dosFileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosFileLabel.AutoSize = true;
            this.dosFileLabel.Location = new System.Drawing.Point(3, 8);
            this.dosFileLabel.Name = "dosFileLabel";
            this.dosFileLabel.Size = new System.Drawing.Size(38, 13);
            this.dosFileLabel.TabIndex = 3;
            this.dosFileLabel.Text = "dos.txt";
            // 
            // dosLabel
            // 
            this.dosLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosLabel.AutoSize = true;
            this.dosLabel.Location = new System.Drawing.Point(239, 8);
            this.dosLabel.Name = "dosLabel";
            this.dosLabel.Size = new System.Drawing.Size(73, 13);
            this.dosLabel.TabIndex = 5;
            this.dosLabel.Text = "Duration (sec)";
            // 
            // dosSpinBox
            // 
            this.dosSpinBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosSpinBox.AutoSize = true;
            this.dosSpinBox.DecimalPlaces = 1;
            this.dosSpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.dosSpinBox.Location = new System.Drawing.Point(318, 4);
            this.dosSpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.dosSpinBox.Name = "dosSpinBox";
            this.dosSpinBox.Size = new System.Drawing.Size(62, 20);
            this.dosSpinBox.TabIndex = 6;
            this.dosSpinBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // dosLink1Button
            // 
            this.dosLink1Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosLink1Button.AutoSize = true;
            this.dosLink1Button.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosLink1Button.Enabled = false;
            this.dosLink1Button.Location = new System.Drawing.Point(386, 3);
            this.dosLink1Button.Name = "dosLink1Button";
            this.dosLink1Button.Size = new System.Drawing.Size(43, 23);
            this.dosLink1Button.TabIndex = 7;
            this.dosLink1Button.Text = "Link1";
            // 
            // dosLink2Button
            // 
            this.dosLink2Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosLink2Button.AutoSize = true;
            this.dosLink2Button.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosLink2Button.Location = new System.Drawing.Point(435, 3);
            this.dosLink2Button.Name = "dosLink2Button";
            this.dosLink2Button.Size = new System.Drawing.Size(43, 23);
            this.dosLink2Button.TabIndex = 9;
            this.dosLink2Button.Text = "Link2";
            this.dosLink2Button.UseVisualStyleBackColor = true;
            // 
            // dosBothButton
            // 
            this.dosBothButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosBothButton.AutoSize = true;
            this.dosBothButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosBothButton.Location = new System.Drawing.Point(484, 3);
            this.dosBothButton.Name = "dosBothButton";
            this.dosBothButton.Size = new System.Drawing.Size(39, 23);
            this.dosBothButton.TabIndex = 10;
            this.dosBothButton.Text = "Both";
            this.dosBothButton.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 422);
            this.Controls.Add(this.mainLayout);
            this.Name = "MainWindow";
            this.Text = "WestTel ALI Simulator";
            this.mainLayout.ResumeLayout(false);
            this.splitter.Panel1.ResumeLayout(false);
            this.splitter.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).EndInit();
            this.splitter.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.recordSplitter.Panel1.ResumeLayout(false);
            this.recordSplitter.Panel1.PerformLayout();
            this.recordSplitter.Panel2.ResumeLayout(false);
            this.recordSplitter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recordSplitter)).EndInit();
            this.recordSplitter.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.aliDatabaseGroupBox.ResumeLayout(false);
            this.aliDatabaseGroupBox.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.broadcastGroupBox.ResumeLayout(false);
            this.broadcastGroupBox.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.aliOptionsGroupBox.ResumeLayout(false);
            this.aliOptionsGroupBox.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.link2GroupBox.ResumeLayout(false);
            this.link2GroupBox.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayAckNak2SpinBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayRequest2SpinBox)).EndInit();
            this.link1GroupBox.ResumeLayout(false);
            this.link1GroupBox.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delayRequest1SpinBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayAckNak1SpinBox)).EndInit();
            this.npdGroupBox.ResumeLayout(false);
            this.npdGroupBox.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.npdSpinBox0)).EndInit();
            this.dosGroupBox.ResumeLayout(false);
            this.dosGroupBox.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dosSpinBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.SplitContainer splitter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer recordSplitter;
        private System.Windows.Forms.GroupBox aliDatabaseGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox broadcastGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox recordTextBox;
        private System.Windows.Forms.TextBox broadcastTextBox;
        private System.Windows.Forms.Button allTransmitButton;
        private System.Windows.Forms.Button addRecordButton;
        private System.Windows.Forms.Button transmitRecordButton;
        private System.Windows.Forms.GroupBox npdGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label npdLabel3;
        private System.Windows.Forms.Label npdLabel2;
        private System.Windows.Forms.Label npdLabel1;
        private System.Windows.Forms.Label npdLabel0;
        private System.Windows.Forms.NumericUpDown npdSpinBox3;
        private System.Windows.Forms.NumericUpDown npdSpinBox2;
        private System.Windows.Forms.NumericUpDown npdSpinBox1;
        private System.Windows.Forms.NumericUpDown npdSpinBox0;
        private System.Windows.Forms.Label databaseFileLabel;
        private System.Windows.Forms.Button databaseFileButton;
        private System.Windows.Forms.ComboBox recordComboBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button deleteRecordButton;
        private System.Windows.Forms.Button saveDatabaseButton;
        private System.Windows.Forms.Button clearRecordButton;
        private System.Windows.Forms.Button clearBroadcastButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox aliOptionsGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox dosGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label dosFileLabel;
        private System.Windows.Forms.Button dosFileButton;
        private System.Windows.Forms.Label dosLabel;
        private System.Windows.Forms.NumericUpDown dosSpinBox;
        private System.Windows.Forms.Button dosLink1Button;
        private System.Windows.Forms.ComboBox sendModeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox nakHeartbeats1CheckBox;
        private System.Windows.Forms.CheckBox nakRequests1CheckBox;
        private System.Windows.Forms.NumericUpDown delayAckNak1SpinBox;
        private System.Windows.Forms.NumericUpDown delayRequest1SpinBox;
        private System.Windows.Forms.GroupBox link2GroupBox;
        private System.Windows.Forms.GroupBox link1GroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button link1TransmitButton;
        private System.Windows.Forms.Button link2TransmitButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.CheckBox nakRequests2CheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox nakHeartbeats2CheckBox;
        private System.Windows.Forms.NumericUpDown delayRequest2SpinBox;
        private System.Windows.Forms.NumericUpDown delayAckNak2SpinBox;
        private System.Windows.Forms.Button dosLink2Button;
        private System.Windows.Forms.Button dosBothButton;
        private System.Windows.Forms.CheckBox stxCheckBox;
        private System.Windows.Forms.CheckBox etxCheckBox;
        private System.Windows.Forms.CheckBox transmitPosAndTypeCheckBox;
        private System.Windows.Forms.TextBox transmitPosAndTypeTextBox;
    }
}

