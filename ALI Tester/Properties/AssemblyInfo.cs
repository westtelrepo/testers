﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WestTel ALI Simulator")]
[assembly: AssemblyDescription("WestTel ALI Simulator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WestTel International")]
[assembly: AssemblyProduct("WestTel ALI Simulator")]
[assembly: AssemblyCopyright("Copyright © 2004-2016 WestTel International")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("76adefbe-d79c-4f54-b935-b105604f7cc9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.1.1.0")]
[assembly: AssemblyFileVersion("2.1.1.0")]
