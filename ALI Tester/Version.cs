
/*****************************************************************************
* FILE: main.cs
*  
* DESCRIPTION: Version history
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 30 Aug 2007 Jesse Elliott
******************************************************************************/

using System;

namespace ALITester
{
    class Version
    {
        public static string getVersionStr() {return "2.1.1";}
    }
}

/*==================================================*
    version 2.1.1 delivered on 8/5/16

    - Fixed regression caused by making a Dictionary a SortedDictionary
 *==================================================*/

/*==================================================*
    version 2.01 delivered on 7/27/16

    - Added record queue so that each number can have multiple records
    - Changed exit confirmation from Yes/No/Cancel to Yes/No
 *==================================================*/

/*==================================================*
    version 2.0 Beta 3  delivered on 7/03/11

    - Fixed �Link Settings� being cut off on some computers (Bill's laptop)
    - Moved the NPD settings closer together
    - Swapped the Clear and Browse buttons so that Clear is first
    - Put only the path of the ALI file in the status bar
 *==================================================*/

/*==================================================*
    version 2.0 Beta 2  delivered on 6/03/11

    - In simulator mode, the window is no longer resizeable
    - NPD labels changed from '0' to 'NPD0' ...
    - Removed Verbose check box in simulator mode (verbose is always on)
    - Added a Browse button to choose the ALI Database
    - 'Clear Log' button is now 'Clear'
    - *.alidb is now *.ali
    - Added an 'Are you sure you want to quit?' check in simulator mode
 *==================================================*/

/*==================================================*
    version 2.0 Beta 1  delivered on 5/20/11

    - added simulator capability
    - added hidden command line option /guiTesterMode
    - allow same port for both links
    - fixed regression in 1.86 where the duplicate timeout
      was changed from 200 to 800 milliseconds
 *==================================================*/

/*==================================================*
    version 1.86  delivered on 3/30/08

    - fixed bug where duplicates weren't handled correctly with
      multiple messages received at once
    - fixed splitter settings regression
 *==================================================*/

/*==================================================*
    version 1.85  delivered on 3/11/08

    - fixed bug where multiple messages didn't look like duplicates
 *==================================================*/

/*==================================================*
    version 1.84  delivered on 3/2/08

    - Removed "Needs Hearbeat" logic
 *==================================================*/

/*==================================================*
    version 1.83  delivered on 2/28/08

    - Send any three characters for type and position
    - Added going-out-of-service button
 *==================================================*/

/*==================================================*
    version 1.82  delivered on 2/25/08

    - Changed Same_Link mode to Independant_Links mode
    - Manual transmit in Independant_Links mode sends on both links
    - Explicitly specifying types Broadcast_Stop and No_Record_Found now use 5 and 9
 *==================================================*/

/*==================================================*
    version 1.81  delivered on 2/25/08

    - Handles multiple messages on the socket
 *==================================================*/

/*==================================================*
    version 1.8  delivered on 2/12/08

    - Added capability to transmit an ALI Record without <stx> or <etx>
      and with manually set type and position number
 *==================================================*/

/*==================================================*
    version 1.76  delivered on 2/11/08

    - fixed ALI with LinkX_Down, the type field should be 1 not 2
 *==================================================*/

/*==================================================*
    version 1.75  delivered on 2/10/08

    - Fixed bug where a request was received on both links,
      but Link 1 was NAKing all requests -- so when Link 2
      received the request, it checked out with what Link 1
      had received, so it sent out the record on the alternating
      Link (which happened to be Link 1)
    - when in Same_Link mode, don't wait for duplicate requests
 *==================================================*/

/*==================================================*
    version 1.72  delivered on 1/26/08

    - Changed functionality so that anything received on a
      "reduced heartbeat link" makes it active again
 *==================================================*/

/*==================================================*
    version 1.711  delivered on 12/10/07

    - Set heartbeat timeout back to 2 minutes rather than 20 seconds
 *==================================================*/

/*==================================================*
    version 1.71   delivered on 12/7/07

    - Fixed bug where links sometimes ignored Send Mode when a heartbeat was needed
 *==================================================*/

/*==================================================*
    version 1.7   delivered on 11/15/07

    - Treat links as down when heartbeat > 2 minutes
 *==================================================*/

/*==================================================*
    version 1.6   delivered on 11/12/07

    - Added Same_Link functionality to SendMode
    - Same_Link mode enabled single requests to have the correct Link Up type
 *==================================================*/

/*==================================================*
    version 1.55   delivered on 10/30/07

    - Responses are on the received link if no duplicate comes in
 *==================================================*/

/*==================================================*
    version 1.5   delivered on 10/22/07

    - Links that are down no longer respond to anything
    - Enable Log is defaulted to off
    - Separated DOS buttons
 *==================================================*/

/*==================================================*
    version 1.4   delivered on 10/03/07

    - No longer overwrite alidb.dat file on install
    - Added phone number to the record-not-found message
    - No longer ignore every other request, rather ignore same request on other link
    - Made the NAK all and Delay options per link
    - Added Link1_Down and Link2_Down
 *==================================================*/

/*==================================================*
    version 1.3   delivered on 9/26/07

    - Added ability to paste from a menu
    - No longer ignore carriage returns
    - Ack/Nak only on link received on
    - Respond to double request (on both links) only once
    - Added delay for Ack/Nak and record response
 *==================================================*/

/*==================================================*
    version 1.25   delivered on 9/24/07

    - Changed NPDs from 1-4 to 0-3
    - Enabled drag and drop
    - Displaying related link in messages
    - Correlate position from request to record response
    - Message alternate links properly
 *==================================================*/

/*==================================================*
    version 1.2   delivered on 9/19/07

    - Added DOS functionality
 *==================================================*/

/*==================================================*
    version 1.1   delivered on 9/11/07

    - shrunk Broadcast window 
    - allow send mode to be modified
    - added option to NAK all requests or NAK all heartbeats
    - respond to requests
 *==================================================*/

/*==================================================*
    version 1.0   delivered on 9/10/07

    - created
 *==================================================*/
