
/*****************************************************************************
* FILE: main.cs
*  
* DESCRIPTION: Starting point for the program
* 
*              The ALI Tester (or simulator) loads and optionally edits an
*              ALI database, and responds to ALI requests and heartbeats.
*              It acurately models the dual link behavior and supports taking
*              one or both of links down as well as delaying the various
*              responses.
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 30 Aug 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ALITester
{
    static class ALITesterMain
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow(args));
        }
    }
}