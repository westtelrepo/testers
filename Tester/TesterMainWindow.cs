
/*****************************************************************************
* FILE: TesterMainWindow.cs
*  
* DESCRIPTION: Tester Main Window (base class)
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 27 Feb 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tester
{
    public class TesterMainWindow : Form
    {
        public enum SendMode
        {
            Alternate_Links,
            Independant_Links,
            Link1_Down,
            Link2_Down,
            Both_Down
        };

        /****************************************************************************************************
        * NAME: Constructor: TesterMainWindow()
        * 
        * DESCRIPTION: Constructs an instance of this class and initializes toolTip
        * 
        * VARIABLES:
        *   toolTip                  Local   - tool tip object
        *   InitialDelay                       <System.Windows.Forms.ToolTip>
        *   AutomaticDelay                     <System.Windows.Forms.ToolTip>
        *   AutoPopDelay                       <System.Windows.Forms.ToolTip>
        *   ShowAlways                         <System.Windows.Forms.ToolTip>
        * 
        * FUNCTIONS:
        *   ToolTip()                          <System.Windows.Forms>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 26 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public TesterMainWindow()
        {
            System.ComponentModel.Container components = new System.ComponentModel.Container();

            toolTip = new ToolTip(components);
            toolTip.InitialDelay    = 200;
            toolTip.AutomaticDelay  = 200;
            toolTip.AutoPopDelay    = 15000;
            toolTip.ShowAlways      = true;
            toolTip.StripAmpersands = true;
        }


        /****************************************************************************************************
        * NAME: Method: void init()
        * 
        * DESCRIPTION: Initialized all GUI elements
        * 
        * PARAMETERS:
        *   _appName     - name of the application (i.e. CAD, ANI...)
        *   versionStr   - version (i.e. "1.64")
        *   iconFilename - application .ico file
        *   mainLayout   - main window's table layout
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 22 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void init(
            string               _appName,
            string               versionStr,
            string               iconFilename,
            TableLayoutPanel     mainLayout,
            SplitContainer       _splitter,
            int                  numConnections
        )
        {
            appName = _appName;

            // connect events
            FormClosing += closeEvent;
            Load        += loadEvent;

            // create timers
            eventLoopTimer = new Timer();
            eventLoopTimer.Interval = 50;
            eventLoopTimer.Tick += execute;

            // set splitter
            splitter = _splitter;

            mainLayout.Padding = new Padding(4, 8, 4, 0);

            // create comm group box
            commGroupBox = new GroupBox();
            commGroupBox.AutoSize = true;
            commGroupBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            commGroupBox.Dock = DockStyle.Fill;
            commGroupBox.Text = "Link Settings";

            TableLayoutPanel commLayout = new TableLayoutPanel();
            commLayout.AutoSize    = true;
            commLayout.Dock        = DockStyle.Fill;
            commLayout.ColumnCount = 1;
            commLayout.ColumnStyles.Add(new ColumnStyle());
            commGroupBox.Controls.Add(commLayout);

            mainLayout.Controls.Add(commGroupBox, 0, 0);
            mainLayout.RowCount++;
            mainLayout.RowStyles.Insert(0, new RowStyle()); // auto-size

            // create comm widgets
            commWidget = new CommWidget[numConnections];

            for (int i = numConnections - 1; i >= 0; i--)
            {
                commWidget[i] = new CommWidget(commLayout, i + 1);
            }

            // create message window
            messageTextEdit = new RichTextBox();

            if (splitter != null)
            {
                splitter.Panel2.Controls.Add(messageTextEdit);
            }
            else
            {
                mainLayout.Controls.Add(messageTextEdit, 0, mainLayout.RowCount);
                mainLayout.RowCount++;
                mainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            }

            messageTextEdit.Dock          = DockStyle.Fill;
            messageTextEdit.BackColor     = SystemColors.Window;
            messageTextEdit.HideSelection = false;
            messageTextEdit.ReadOnly      = true;
            messageTextEdit.TabStop       = false;
            messageTextEdit.WordWrap      = false;

            // create general check boxes and buttons
            createButtons(mainLayout);

            // create status bar
            createStatusBar(mainLayout);

            // open log file
            if (pLogFile == null && isLoggingEnabled()) openLogFile();

            // add version to app name in the titlebar
            Text = "WestTel " + appName + " " + versionStr;

            // add icon to title bar
            try
            {
                Icon = new Icon(iconFilename);
            }
            catch (Exception) {}

            // restore settings
            try
            {
                pSettings = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(
                    "Software\\Experient\\" + appName);

                if (pSettings != null)
                {
                    try
                    {
                        restoreSettings(pSettings);
                    }
                    catch (Exception e)
                    {
                        warning("Failed to restore settings (" + e.Message + ")");
                    }
                }
                else
                {
                    warning("Failed to access settings");
                }
            }
            catch (Exception e)
            {
                warning("Failed to access settings (" + e.Message + ")");
                pSettings = null; 
            }

            // finish DOS initialization
            if (dosInitialized)
            {
                // read file
                readFile(dosFilename, out dosBuffer, out dosSize, dosFileLabel);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void initDOS()
        * 
        * DESCRIPTION: Initialized Denial of Service elements
        * 
        * PARAMETERS:
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 17 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void initDOS(
            ref Label         _dosFileLabel,
            ref Button        _dosFileButton,
            ref Button[]      dosButtonList,
            ref NumericUpDown _dosSpinBox
        )
        {
            dosInitialized = true;

            dosFileLabel  = _dosFileLabel;
            dosFileButton = _dosFileButton;
            dosSpinBox    = _dosSpinBox;

            foreach (Button dosButton in dosButtonList)
            {
                dosButton.Enabled = false;
                dosButton.Click += startDOS;

                // add tool tip
                toolTip.SetToolTip(dosButton, "Denial of Service (ESC to Stop)");
            }

            dosFileButton.Click += chooseDOSFile;

            // add tool tip
            toolTip.SetToolTip(dosFileButton, "Select Denial of Service File");

            // DOS timer
            dosTimer = new Timer();
            dosTimer.Interval = 1;
            dosTimer.Tick += sendSingleDOS;
        }


        /****************************************************************************************************
        * NAME: Method: void createButtons()
        * 
        * DESCRIPTION: Create generic main window check boxes and buttons
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 31 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void createButtons(TableLayoutPanel mainLayout)
        {
            TableLayoutPanel layout = new TableLayoutPanel();

            layout.SuspendLayout();

            mainLayout.Controls.Add(layout, 0, mainLayout.RowCount);
            mainLayout.RowCount++;
            mainLayout.RowStyles.Add(new RowStyle()); // auto-size

            layout.AutoSize    = true;
            layout.Dock        = DockStyle.Fill;
            layout.RowCount    = 1;
            layout.ColumnCount = 11;

            layout.RowStyles.   Add(new RowStyle());
            layout.ColumnStyles.Add(new ColumnStyle()); // unused column for another widget
            layout.ColumnStyles.Add(new ColumnStyle()); // unused column for another widget
            layout.ColumnStyles.Add(new ColumnStyle()); // unused column for another widget
            layout.ColumnStyles.Add(new ColumnStyle()); // clear button
            layout.ColumnStyles.Add(new ColumnStyle()); // verbose check box
            layout.ColumnStyles.Add(new ColumnStyle()); // log check box
            layout.ColumnStyles.Add(new ColumnStyle()); // unused column for another widget
            layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F)); // stretch space
            layout.ColumnStyles.Add(new ColumnStyle()); // unused column for another widget
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());

            logCheckBox     = new CheckBox();
            verboseCheckBox = new CheckBox();

            clearWindowButton = new Button();

            startButton = new Button();
            stopButton  = new Button();

            layout.Controls.Add(clearWindowButton, 3, 0);
            layout.Controls.Add(verboseCheckBox,   4, 0);
            layout.Controls.Add(logCheckBox,       5, 0);
            layout.Controls.Add(startButton,       9, 0);
            layout.Controls.Add(stopButton,       10, 0);

            // clearWindowButton
            clearWindowButton.AutoSize     = true;
            clearWindowButton.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            clearWindowButton.Text         = "Clear Message Window";
            clearWindowButton.Click       += clearMessageWindow;

            // verboseCheckBox
            verboseCheckBox.Text     = "&Verbose";
            verboseCheckBox.AutoSize = true;
            verboseCheckBox.Anchor   = AnchorStyles.None;

            // logCheckBox
            logCheckBox.Text     = "&Log Messages";
            logCheckBox.Checked  = false;
            logCheckBox.AutoSize = true;
            logCheckBox.Anchor   = AnchorStyles.None;

            // pStartButton
            startButton.AutoSize           = true;
            startButton.AutoSizeMode       = AutoSizeMode.GrowAndShrink;
            startButton.Text               = "&Start";
            startButton.Click             += start;

            // pStopButton
            stopButton.AutoSize           = true;
            stopButton.AutoSizeMode       = AutoSizeMode.GrowAndShrink;
            stopButton.Enabled            = false;
            stopButton.Text               = "S&top";
            stopButton.Click             += stop;

            // virtual method for adding other widgets
            addToBottomLayout(ref layout);

            layout.ResumeLayout();
        }

        protected virtual void addToBottomLayout(ref TableLayoutPanel layout) {}


        /****************************************************************************************************
        * NAME: Method: void createStatusBar()
        * 
        * DESCRIPTION: Create status bar
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected virtual StatusStrip createStatusBar(TableLayoutPanel mainLayout)
        {
            statusBar = new StatusStrip();

            stretchStatusLabel = new ToolStripStatusLabel();
            sentStatusLabel    = new ToolStripStatusLabel();
            recvStatusLabel    = new ToolStripStatusLabel();

            statusBar.SuspendLayout();

            // add status bar to main layout
            mainLayout.Controls.Add(statusBar, 0, mainLayout.RowCount);
            mainLayout.RowCount++;
            mainLayout.RowStyles.Add(new RowStyle()); // auto-size

            statusBar.AllowItemReorder = true;
            statusBar.Dock = DockStyle.Fill;
            statusBar.GripStyle = ToolStripGripStyle.Visible;
            statusBar.Items.AddRange(new ToolStripItem[] {stretchStatusLabel,sentStatusLabel,recvStatusLabel});
            statusBar.ShowItemToolTips = true;

            // stretch
            stretchStatusLabel.Spring = true;
            
            // sent
            sentStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;
            sentStatusLabel.Text        = "Sent 0 Bytes";
            sentStatusLabel.ToolTipText = "Total Sent: 0 Bytes";

            // recv
            recvStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;
            recvStatusLabel.Text        = "Recv 0 Bytes";
            recvStatusLabel.ToolTipText = "Total Received: 0 Bytes";

            statusBar.ResumeLayout();

            return statusBar;
        }


        /****************************************************************************************************
        * NAME: Method: void loadEvent()
        * 
        * DESCRIPTION: Handle load event
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2008 Experient Corporation
        *    AUTHOR: 4 Mar 2008 Jesse Elliott
        ****************************************************************************************************/ 

        virtual protected void loadEvent(object unusedObj, EventArgs unusedArgs)
        {
            restoreWindowState(pSettings);

            if (splitter != null)
            {
                try
                {
                    splitter.SplitterDistance =
                        (int)pSettings.GetValue("SplitDist", splitter.SplitterDistance);
                }
                catch (Exception e)
                {
                    warning("Failed to restore splitter settings (" + e.Message + ")");
                }
            }

            startButton.Focus();
        }

        virtual protected void restoreWindowState(Microsoft.Win32.RegistryKey _pSettings)
        {
            try
            {
                Top    = (int)_pSettings.GetValue("Top",    Top);
                Left   = (int)_pSettings.GetValue("Left",   Left);
                Width  = (int)_pSettings.GetValue("Width",  Width);
                Height = (int)_pSettings.GetValue("Height", Height);

                WindowState =
                    (FormWindowState)_pSettings.GetValue("WindowState", (int)WindowState);
            }
            catch (Exception e)
            {
                warning("Failed to restore window size and location (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void closeEvent()
        * 
        * DESCRIPTION: Handle close event
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 31 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        virtual protected void closeEvent(object sender, FormClosingEventArgs args)
        {
            stop(this, null);

            if (pSettings != null)
            {
                rememberWindowState(pSettings);
                rememberSettings(pSettings);
            }

            log("[End Log]");
        }

        virtual protected void rememberWindowState(Microsoft.Win32.RegistryKey _pSettings)
        {
            try
            {
                if (WindowState == FormWindowState.Normal)
                {
                    _pSettings.SetValue("Top",    Top);
                    _pSettings.SetValue("Left",   Left);
                    _pSettings.SetValue("Width",  Width);
                    _pSettings.SetValue("Height", Height);
                }

                if (WindowState != FormWindowState.Minimized)
                {
                    _pSettings.SetValue("WindowState", (int)WindowState);
                }
            }
            catch (Exception e)
            {
                warning("Failed to remember window settings: " + e.Message);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void restoreSettings()
        * 
        * DESCRIPTION: Restore settings from the registry
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 22 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected virtual void restoreSettings(Microsoft.Win32.RegistryKey _pSettings)
        {
            try
            {
                foreach (CommWidget c in commWidget) c.restoreSettings(_pSettings);
            }
            catch (Exception e)
            {
                warning("Failed to restore connection settings (" + e.Message + ")");
            }

            if (dosInitialized)
            {
                try
                {
                    dosFilename      = (string)_pSettings.GetValue("dosFile", dosFilename);
                    dosSpinBox.Value = Convert.ToDecimal(_pSettings.GetValue("dosDuration", dosSpinBox.Value));
                }
                catch (Exception e)
                {
                    warning("Failed to restore Denail of Service settings (" + e.Message + ")");
                }
            }

            try
            {
                logCheckBox.Checked = Convert.ToBoolean(_pSettings.GetValue("logEnabled", logCheckBox.Checked));
            }
            catch (Exception e)
            {
                warning("Failed to restore log settings (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void rememberSettings()
        * 
        * DESCRIPTION: Remember settings to the registry
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 22 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected virtual void rememberSettings(Microsoft.Win32.RegistryKey _pSettings)
        {
            try
            {
                if (splitter != null)
                {
                    _pSettings.SetValue("SplitDist", splitter.SplitterDistance);
                }
            }
            catch (Exception e)
            {
                warning("Failed to remember splitter settings: " + e.Message);
            }

            try
            {
                foreach (CommWidget c in commWidget) c.rememberSettings(_pSettings);
            }
            catch (Exception e)
            {
                warning("Failed to remember connection settings: " + e.Message);
            }

            if (dosInitialized)
            {
                try
                {
                    _pSettings.SetValue("dosFile",     dosFilename);
                    _pSettings.SetValue("dosDuration", dosSpinBox.Text);
                }
                catch (Exception e)
                {
                    warning("Failed to remember Denial of Service settings: " + e.Message);
                }
            }

            try
            {
                _pSettings.SetValue("logEnabled", logCheckBox.Checked);
            }
            catch (Exception e)
            {
                warning("Failed to remember log settings: " + e.Message);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void start()
        * 
        * DESCRIPTION: Start socket communication
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void start(object sender, EventArgs e)
        {
            if (running) return;

            bool connected = false;

            foreach (CommWidget c in commWidget) connected |= c.connect(this);

            if (connected)
            {
                // disable Links GUI while connected
                commGroupBox.Enabled = false;

                stopButton. Enabled = true;
                startButton.Enabled = false;

                notice("\n----------  Start  ----------");

                running = true;
                runningChanged(running);

                // start event loop
                eventLoopTimer.Start();
            }
        }


        /****************************************************************************************************
        * NAME: Method: void stop()
        * 
        * DESCRIPTION: Stop socket communication
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 26 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void stop(object sender, EventArgs e)
        {
            startButton.Enabled = true;
            stopButton. Enabled = false;

            // enable Links GUI when disconnected
            commGroupBox.Enabled = true;

            if ( ! running) return;

            if (curDOSButton != null) stopDOS(curDOSButton, null);

            running = false;
            runningChanged(running);

            lastUsedCommIndex = -1;

            // disconnect socket
            foreach (CommWidget c in commWidget) c.disconnect();

            eventLoopTimer.Stop();

            notice("----------  Stop  -----------\n");
        }

        protected virtual void runningChanged(bool r) {}


        /****************************************************************************************************
        * NAME: Method: void execute()
        * 
        * DESCRIPTION: Main event loop for socket communication (called from eventLoopTimer)
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 26 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void execute(object sender, EventArgs e)
        {
            // check sockets
            foreach (CommWidget c in commWidget)
            {
                if ( ! c.isConnected()) continue;

                // make sure that curCommWidget is initialized
                if (curCommWidget == null) curCommWidget = c;

                ulong  length    = 0;
                byte[] socketMsg = c.receiveBytes(out length);

                // if a message was received
                if (length > 0)
                {
                    curCommWidget = c;

                    byte[] discardedBytes = null;

                    // loop until all bytes are handled
                    while (length > 0)
                    {
                        uint bytesHandled = handleSocketMsg(socketMsg, length, c);

                        // if at least part of the message could be handled
                        if (bytesHandled != 0)
                        {
                            if (bytesHandled == length)
                            {
                                if (socketMsg.Length != (int)length) Array.Resize(ref socketMsg, (int)length);

                                c.rememberRecentSocketMsg(getCurTimeSec(), socketMsg);
                                break;
                            }
                            else
                            {
                                byte[] handledSocketMsg = new byte[bytesHandled];
                                Array.Copy(socketMsg, handledSocketMsg, bytesHandled);

                                c.rememberRecentSocketMsg(getCurTimeSec(), handledSocketMsg);
                            }
                        }
                        else
                        {
                            if (discardedBytes == null) discardedBytes = new byte[1];
                            else                        Array.Resize(ref discardedBytes, discardedBytes.Length + 1);

                            discardedBytes[discardedBytes.Length - 1] = socketMsg[0];

                            bytesHandled = 1;
                        }

                        length -= bytesHandled;

                        byte[] tempSocketMsg = new byte[length];
                        Array.Copy(socketMsg, (int)bytesHandled, tempSocketMsg, 0, (int)length);

                        socketMsg = tempSocketMsg;
                    }

                    if (discardedBytes != null)
                    {
                        if (verboseCheckBox.Checked)
                        {
                            warning("Link " + c.getId() + " message discarded: " + discardedBytes.ToString());
                        }
                    }
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: void clearMessageWindow()
        * 
        * DESCRIPTION: Clear the message window
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 26 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void clearMessageWindow(object sender, EventArgs e)
        {
            if (messageTextEdit != null) messageTextEdit.Clear();
        }


        /****************************************************************************************************
        * NAME: Method: double getCurTimeSec(), string getCurTimeStr
        * 
        * DESCRIPTION: Get the current time in seconds or as a string
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public double getCurTimeSec() {return DateTime.Now.Ticks * 0.0000001;}

        public string getCurTimeStr()
        {
            string str = "";

            try
            {
                DateTime now = DateTime.Now;

                // hour
                str = Convert.ToInt32(now.Hour) + ":";

                // min
                if (now.Minute < 10) str += "0";
                str += Convert.ToInt32(now.Minute) + ":";

                // sec
                if (now.Second < 10) str += "0";
                str += Convert.ToInt32(now.Second) + ".";

                // msec
                if (now.Millisecond < 100) str += "0";
                if (now.Millisecond < 10) str += "0";
                str += Convert.ToInt32(now.Millisecond);
            }
            catch (Exception e)
            {
                str = "Failed to get current time: " + e.Message;
            }

            return str;
        }


        /****************************************************************************************************
        * NAME: Method: bool chooseFile()
        * 
        * DESCRIPTION: Allow the user to choose a file via the File Dialog
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 16 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected bool chooseFile(
            ref string filename,
            ref byte[] buffer,
            ref ulong  size,
            Label      label
        )
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();

                fileDialog.FileName = filename;

                try
                {
                    fileDialog.ShowDialog(this);
                }
                catch
                {
                    fileDialog.FileName = "";
                    fileDialog.ShowDialog(this);
                }

                // if cancelled
                if (fileDialog.FileName.Length == 0) return false;

                filename = fileDialog.FileName;

                readFile(filename, out buffer, out size, label);
            }
            catch (Exception e)
            {
                warning("Failed to open file dialog: " + e.Message);
                return false;
            }

            return true;
        }


        /****************************************************************************************************
        * NAME: Method: bool readFile()
        * 
        * DESCRIPTION: Reads a file into the given buffer
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 14 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool readFile(
            string     fullFilename,
            out byte[] buffer,
            out ulong  length,
            Label      label
        )
        {
            string filename = fullFilename;

            string errStr = null;

            try
            {
                int lastSlash = filename.LastIndexOf('\\');

                if (lastSlash > 0) filename = filename.Substring(lastSlash + 1);

                if (System.IO.File.Exists(fullFilename))
                {
                    // try to open file
                    System.IO.FileStream pFile = System.IO.File.Open(fullFilename,
                                                                     System.IO.FileMode.Open,
                                                                     System.IO.FileAccess.Read,
                                                                     System.IO.FileShare.ReadWrite);
                    length = (ulong)pFile.Length;
                    buffer = new byte[length];

                    int bytesRead = pFile.Read(buffer, 0, (int)length);

                    if (bytesRead != (int)length)
                    {
                        warning("Failed to read entire file \"" + fullFilename + "\" (read " +
                                Convert.ToString(bytesRead) + " bytes of " + length.ToString());
                    }

                    // ignore trailing nulls
                    while (length > 0 && buffer[length - 1] == 0) length--;

                    // update label with filename
                    if (label != null)
                    {
                        // display filename and show fullFilename in a tooltip
                        label.Text      = filename + " (" + convertBytesToShortString(length) + ")";
                        label.ForeColor = Color.Black;

                        // fullFilename tool tip
                        toolTip.SetToolTip(label, fullFilename + " (" + convertBytesToString(length) + ")");
                    }

                    return true;
                }
                else
                {
                    errStr = "File not found";
                }
            }
            catch (Exception e)
            {
                errStr = e.Message;
            }

            length = (ulong)errStr.Length;
            buffer = new byte[length + 1];

            System.Text.ASCIIEncoding.ASCII.GetBytes(errStr, 0, (int)length, buffer, 0);

            // update label with error and turn it red
            if (label != null)
            {
                label.Text      = filename;
                label.ForeColor = Color.Red;

                toolTip.SetToolTip(label, "File Not Found");
            }

            return false;
        }


        /****************************************************************************************************
        * NAME: Method: bool getLine()
        * 
        * DESCRIPTION: Get a single line from the given buffer
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 16 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected bool getLine(ref byte[] buffer, ulong bufLength, ref ulong i, out string curLine)
        {
            curLine = "";

            while (i < bufLength)
            {
                char curChar = (char)buffer[i++];

                // return true on newline
                if (curChar == '\n') return true;

                // append everything but '\r'
                if (curChar != '\r') curLine += curChar;
            }

            return (curLine.Length > 0);
        }


        /****************************************************************************************************
        * NAME: Method: bool socketIsConnected()
        * 
        * DESCRIPTION: Returns whether there are any sockets connected
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool socketIsConnected()
        {
            // if there is at least one connection, return true
            foreach (CommWidget c in commWidget) if (c.isConnected()) return true;

            return false;
        }


        /****************************************************************************************************
        * NAME: Method: int getNumSocketsConnected()
        * 
        * DESCRIPTION: Returns the number of sockets connected
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 10 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public int getNumSocketsConnected()
        {
            int numConnections = 0;

            foreach (CommWidget c in commWidget) if (c.isConnected()) numConnections++;

            return numConnections;
        }


        /****************************************************************************************************
        * NAME: Method: uint handleSocketMsg()
        * 
        * DESCRIPTION: Virtual method to handle socket messages
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected virtual uint handleSocketMsg(byte[] socketMsg, ulong length, CommWidget c) {return 0;}


        /****************************************************************************************************
        * NAME: Method: void sendAck(), void sendNak(), void sendHeartbeat()
        * 
        * DESCRIPTION: Send various message over the socket
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void sendAck(int delayMSec)              {sendByte(SocketObj.ACK, "Ack", delayMSec);}
        protected void sendAck(object sender, EventArgs e) {sendByte(SocketObj.ACK, "Ack");}

        protected void sendNak(int delayMSec)              {sendByte(SocketObj.NAK, "Nak", delayMSec);}
        protected void sendNak(object sender, EventArgs e) {sendByte(SocketObj.NAK, "Nak");}

        protected void sendHeartbeat(object sender, EventArgs e) {sendHeartbeat();}

        static byte[] HEARTBEAT = {SocketObj.STX, (byte)'H', SocketObj.ETX, (byte)'K'};
        protected virtual void sendHeartbeat()             {sendBytes(HEARTBEAT, 4, "Heartbeat");}


        /****************************************************************************************************
        * NAME: Method: void startDOS()
        *
        * DESCRIPTION: Starts or stops the timer for Denial of Service (DOS)
        *    
        * VARIABLES:
        *   Text                               <System.Windows.Forms.NumericUpDown>
        *   dosFinishTime            Local   - DOS finish time
        *   dosStartTime             Local   - DOS start time
        *   Cursor                             <System.Windows.Forms>
        *   dosSpinBox               Local   - DOS duration in seconds
        *   dosTimer                 Local   - DOS timer
        *                                                                         
        * FUNCTIONS:
        *   getCurTimeSec()          Library - <TesterMainWindow.cs> gets current time in seconds
        *   notice()                 Library - <TesterMainWindow.cs> record notice message
        *   socketIsConnected()      Library - <TesterMainWindow.cs> tests for valid socket
        *   toDouble()                         <System.Convert>
        *   Stop()                             <System.Windows.Forms.Timer>
        *   Start()                            <System.Windows.Forms.Timer>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void startDOS(object obj, EventArgs unusedArgs)
        {
            // if the timer is running, disable more DOS'
            if (dosTimer.Enabled) return;

            Button dosButton = null;

            try {dosButton = (Button)obj;}
            catch (Exception) {return;}

            // if a socket is connected, start a DOS
            if (socketIsConnected())
            {
                dosStartTime  = getCurTimeSec();
                dosFinishTime = dosStartTime + Convert.ToDouble(dosSpinBox.Text);

                Cursor = Cursors.AppStarting;

                dosTimer.Stop();
                dosTimer.Start();

                curDOSButton     = dosButton;
                curDOSButtonText = dosButton.Text;
                dosButton.Text   = "&Stop";

                dosButton.Click -= startDOS;
                dosButton.Click += stopDOS;

                // handle ESC key pressed
                dosButton.KeyDown += keyPressedOnDOS;
            }
        }


        /****************************************************************************************************
        * NAME: Method: void sendSingleDOS()
        *
        * DESCRIPTION: Sends contents of DOS file
        *    
        * VARIABLES:
        *   dosFinishTime            Local   - DOS finish time
        *   dosBuffer                Local   - contents of DOS file
        *   dosSize                  Local   - size of DOS file
        *   dosFilename              Local   - DOS filename
        *   dosTimer                 Local   - DOS timer
        *   Cursor                             <System.Windows.Forms>
        *   curDOSButton             Local   - DOS button
        *   Text                               <System.Windows.Forms.Button>
        *                                                                         
        * FUNCTIONS:
        *   getCurTimeSec()          Library - <TesterMainWindow.cs> gets current time in seconds
        *   sendBytes()              Library - <TesterMainWindow.cs> sends bytes over socket
        *   Stop()                             <System.Windows.Forms.Timer>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected virtual void sendSingleDOS(object unusedObject, EventArgs unusedArgs)
        {
            if (getCurTimeSec() < dosFinishTime)
            {
                sendBytes(dosBuffer, dosSize, "DOS File \"" + dosFilename + "\"");
            }
            else
            {
                dosTimer.Stop();
                Cursor = Cursors.Default;

                stopDOS(curDOSButton, null);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void stopDOS()
        *
        * DESCRIPTION: Stops DOS if it is currently in process
        *    
        * VARIABLES:
        *   dosButton                Local   - DOS button
        *   Text                               <System.Windows.Forms.Button>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void stopDOS(object obj, EventArgs unusedArgs)
        {
            if (dosTimer.Enabled)
            {
                dosFinishTime = getCurTimeSec();
                notice("cancelled Denial of Service");
            }

            if (curDOSButton != null)
            {
                curDOSButton.Text = curDOSButtonText;
                curDOSButton = null;
            }

            try
            {
                Button dosButton = (Button)obj;

                dosButton.Click -= stopDOS;
                dosButton.Click += startDOS;

                // unhandle ESC key pressed
                dosButton.KeyDown -= keyPressedOnDOS;
            }
            catch (Exception) {}
        }


        /****************************************************************************************************
        * NAME: Method: void keyPressedOnDOS()
        *
        * DESCRIPTION: Handles escape key pressed by stopping DOS
        *    
        * PARAMETERS:
        *   e - Key event args
        *  
        * VARIABLES:
        *   KeyCode                            <System.Windows.Forms.KeyEventArgs>
        *   Escape                             <System.Windows.Forms.Keys>
        *                                                                         
        * FUNCTIONS:
        *   stopDOS()                Local   - stops DOS timer
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 10 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void keyPressedOnDOS(object obj, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                try {stopDOS((Button)obj, null);}
                catch (Exception) {}
            }
        }


        /****************************************************************************************************
        * NAME: Method: void textBoxKeyPress()
        *
        * DESCRIPTION: Handles text box key press events
        *    
        * PARAMETERS:
        *   senderTextBox - text box to act on
        *   e             - Key event args
        *  
        * VARIABLES:
        *   Control                            <System.Windows.Forms.KeyEventArgs>
        *   KeyCode                            <System.Windows.Forms.KeyEventArgs>
        *   A                                  <System.Windows.Forms.Keys>
        *                                                                         
        * FUNCTIONS:
        *   SelectAll()                        <System.Windows.Forms.TextBox>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 25 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected void textBoxKeyPress(object senderTextBox, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A) ((TextBox)senderTextBox).SelectAll();
        }


        /****************************************************************************************************
        * NAME: Method: void chooseDOSFile()
        *
        * DESCRIPTION: Allows the user to choose a DOS file
        *  
        * VARIABLES:
        *   dosFilename              Local   - DOS filename
        *   dosBuffer                Local   - DOS file contents
        *   dosSize                  Local   - DOS file size
        *   pDOSFileLabel            Local   - DOS filename label
        *                                                                         
        * FUNCTIONS:
        *   chooseFile()             Library - <TesterMainWindow.cs> Allows the user to choose a file
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 14 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void chooseDOSFile(object unusedObject, EventArgs unusedArgs)
        {
            chooseFile(ref dosFilename, ref dosBuffer, ref dosSize, dosFileLabel);
        }


        /****************************************************************************************************
        * NAME: Method: void sendByte(), void sendBytes
        * 
        * DESCRIPTION: Send bytes over the socket
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void sendByte(byte oneByte, string desc)
        {
            byte[] buffer = {oneByte};
            sendBytes(buffer, 1, desc);
        }

        public void sendByte(byte oneByte, string desc, int delayMSec)
        {
            byte[] buffer = {oneByte};
            sendBytes(buffer, 1, desc, delayMSec);
        }

        public void sendBytes(byte[] buffer, ulong length, string desc)
        {
            sendBytes(buffer, length, desc, 0);
        }

        public void sendBytes(byte[] buffer, ulong length, string desc, int delayMSec)
        {
            if ( ! socketIsConnected())
            {
                warning("Failed to send data (no socket connections)");
                return;
            }

            // if the send socket should alternate
            if (sendMode == SendMode.Alternate_Links)
            {
                // loop over all commWidgets, starting at the one after the last one used
                for (int i = (lastUsedCommIndex + 1) % commWidget.Length;
                     i < commWidget.Length; i = (i + 1) % commWidget.Length)
                {
                    // send on the first connected socket
                    if (commWidget[i].isConnected())
                    {
                        commWidget[i].sendBytes(buffer, length, desc, delayMSec);
                        lastUsedCommIndex = i;
                        break;
                    }
                }
            }
            else if (sendMode == SendMode.Independant_Links)
            {
                if (curCommWidget.isConnected())
                {
                    curCommWidget.sendBytes(buffer, length, desc, delayMSec);
                }
            }
            else if (sendMode == SendMode.Link1_Down)
            {
                if (commWidget.Length > 1 && commWidget[1].isConnected())
                {
                    commWidget[1].sendBytes(buffer, length, desc, delayMSec);
                }
            }
            else if (sendMode == SendMode.Link2_Down)
            {
                if (commWidget[0].isConnected())
                {
                    commWidget[0].sendBytes(buffer, length, desc, delayMSec);
                }
            }
            else if (sendMode == SendMode.Both_Down)
            {
                // do nothing
            }
            else
            {
                warning("Failed to send data (don't know which socket to use)");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void sendString()
        * 
        * DESCRIPTION: Send string over the socket
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void sendString(string msgStr, string desc)
        {
            sendString(msgStr, desc, 0);
        }

        public void sendString(string msgStr, string desc, int delayMSec)
        {
            byte[] msg = new byte[msgStr.Length];

            System.Text.ASCIIEncoding.ASCII.GetBytes(msgStr, 0, msgStr.Length, msg, 0);

            sendBytes(msg, (ulong)msgStr.Length, desc, delayMSec);
        }


        /****************************************************************************************************
        * NAME: Method: void handleSentBytes()
        * 
        * DESCRIPTION: Displays number of bytes sent
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public virtual void handleSentBytes(SocketObj socket, byte[] buffer, ulong nBytes, string desc)
        {
            totalBytesSent += nBytes;

            if (sentStatusLabel != null)
            {
                sentStatusLabel.Text        = "Sent " + convertBytesToString(totalBytesSent);
                sentStatusLabel.ToolTipText = "Total Sent: " + Convert.ToString(totalBytesSent) + " Bytes";
            }

            string noticeStr = socket.getMessageHeader() + "Sent";
            if (desc != null && desc.Length > 0) noticeStr += " " + desc;

            if (nBytes == 1) noticeStr += " (1 byte)";
            else             noticeStr += " (" + Convert.ToString(nBytes) + " bytes)";

            if (inVerboseMode()) noticeStr += ": " + formatForDisplay(buffer, nBytes);

            notice(noticeStr);

            // TODO: we should probably truncate the message window
        }


        /****************************************************************************************************
        * NAME: Method: void handleRecvBytes()
        * 
        * DESCRIPTION: Displays number of bytes received
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public virtual void handleRecvBytes(SocketObj socket, byte[] buffer, ulong nBytes)
        {
            totalBytesRecv += nBytes;

            if (recvStatusLabel != null)
            {
                recvStatusLabel.Text        = "Recv " + convertBytesToString(totalBytesRecv);
                recvStatusLabel.ToolTipText = "Total Received: " + Convert.ToString(totalBytesRecv) + " Bytes";
            }

            string noticeStr = socket.getMessageHeader() + "Recv ";

            if (nBytes == 1)
            {
                noticeStr += "1 byte: " + formatForDisplay(buffer, nBytes);
            }
            else
            {
                if (nBytes < 100 || inVerboseMode())
                {
                    noticeStr += Convert.ToString(nBytes) + " bytes: " +
                           formatForDisplay(buffer, nBytes);
                }
                else
                {
                    noticeStr += Convert.ToString(nBytes) + " bytes";
                }
            }

            notice(noticeStr);
        }


        /****************************************************************************************************
        * NAME: Method: string convertBytesToString()
        * 
        * DESCRIPTION: Convert bytes to a string (i.e. 1048576 -> 1 MBytes)
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 4 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public string convertBytesToShortString(ulong nBytes)
        {
            if      (nBytes < 1024)              return nBytes.                                  ToString() + " B";
            else if (nBytes < Math.Pow(1024, 2)) return Math.Ceiling(nBytes / 1024.0).           ToString() + " KiB";
            else                                 return Math.Ceiling(nBytes / Math.Pow(1024, 2)).ToString() + " MiB";
        }

        public string convertBytesToString(ulong nBytes)
        {
            if      (nBytes < 1024)              return nBytes.                      ToString()    + " Bytes";
            else if (nBytes < Math.Pow(1024, 2)) return (nBytes / 1024.0).           ToString("N") + " KiB";
            else                                 return (nBytes / Math.Pow(1024, 2)).ToString("N") + " MiB";
        }


        /****************************************************************************************************
        * NAME: Method: string formatForDisplay()
        * 
        * DESCRIPTION: Format text for display by replacing unreadable characters with their bracketed [NAME]
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 6 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private string STX_STR = new string((char)SocketObj.STX, 1);
        private string ETX_STR = new string((char)SocketObj.ETX, 1);
        private string ACK_STR = new string((char)SocketObj.ACK, 1);
        private string NAK_STR = new string((char)SocketObj.NAK, 1);
        private string LF_STR  = new string((char)SocketObj.LF,  1);
        private string CR_STR  = new string((char)SocketObj.CR,  1);

        protected string formatForDisplay(byte[] buffer, ulong length)
        {
            byte[] temp = new byte[buffer.Length];
            buffer.CopyTo(temp, 0);

            // replace NULL with newline
            for (ulong i = 0; i < length; i++)
            {
                if (temp[i] == 0) temp[i] = (byte)'\n';
            }

            string text = System.Text.ASCIIEncoding.ASCII.GetString(temp);

            text = text.Replace(STX_STR, "[STX]");
            text = text.Replace(ETX_STR, "[ETX]");
            text = text.Replace(ACK_STR, "[ACK]");
            text = text.Replace(CR_STR,  "[\\r]");
            text = text.Replace(LF_STR,  "[\\n]");
            text = text.Replace(NAK_STR, "[NAK]");

            return text;
        }


        /****************************************************************************************************
        * NAME: Method: void log(), void notice(), void warning(), void severeWarning(), void fatal(), void todo()
        * 
        * DESCRIPTION: Various messaging methods
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 30 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected enum MessageType {Log, Notice, Todo, Warning, SevereWarning, Fatal}

        public void log           (string message) { messageHandler(MessageType.Log,           message);}
        public void notice        (string message) { messageHandler(MessageType.Notice,        message);}
        public void warning       (string message) { messageHandler(MessageType.SevereWarning, message); }
        public void severeWarning (string message) { messageHandler(MessageType.Warning,       message); }
        public void fatal         (string message) { messageHandler(MessageType.Fatal,         message); }

        public void todo(string message)
        {
        #if DEBUG
            messageHandler(MessageType.Todo, message);
        #endif
        }


        /****************************************************************************************************
        * NAME: Method: bool handlingMsg()
        * 
        * DESCRIPTION: Message handler
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private static bool handlingMsg = false;

        private void messageHandler(MessageType type, string message)
        {
            if (handlingMsg)
            {
                MessageBox.Show(message, "Error occured while handling another message");
                return;
            }

            // open log file if not yet opened
            if (pLogFile == null && isLoggingEnabled()) openLogFile();

            handlingMsg = true;

            string msg = "";

            if (type != MessageType.Log && type != MessageType.Notice)
            {
                if      (type == MessageType.Warning)       msg = "WARNING: ";
                else if (type == MessageType.SevereWarning) msg = "WARNING: ";
                else if (type == MessageType.Todo)          msg = "TODO: ";
                else if (type == MessageType.Fatal)         msg = "FATAL: ";
            }

            msg += message;

            // time stamp
            string timestamp = "";

            if (running) timestamp = getCurTimeStr() + " -  ";

            if (pLogFile != null && isLoggingEnabled())
            {
                string logMsg = timestamp + msg.Replace("\n", "\r\n") + "\r\n";

                pLogFile.Write(System.Text.ASCIIEncoding.ASCII.GetBytes(logMsg), 0, logMsg.Length);
                pLogFile.Flush();
            }

            // FATAL
            if (type == MessageType.Fatal)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(message, "Error");

            #if DEBUG
                throw (new Exception(message));
            #else
                Application.Exit();
            #endif
            }

            if (type != MessageType.Log && messageTextEdit != null)
            {
                // remember cursor position
                int curPos = messageTextEdit.SelectionStart;
                messageTextEdit.SelectionStart = messageTextEdit.TextLength;

                bool atEnd = (curPos == messageTextEdit.TextLength);

                // send to message window
                if (timestamp.Length > 0)
                {
                    messageTextEdit.SelectionColor = Color.Gray;
                    messageTextEdit.SelectedText   = timestamp;
                }

                if      (type == MessageType.Notice)        messageTextEdit.SelectionColor = Color.Empty;
                else if (type == MessageType.Warning)       messageTextEdit.SelectionColor = Color.Red;
                else if (type == MessageType.SevereWarning) messageTextEdit.SelectionColor = Color.Red;
                else if (type == MessageType.Todo)          messageTextEdit.SelectionColor = Color.Green;
                else                                        messageTextEdit.SelectionColor = Color.Empty;

                messageTextEdit.SelectedText = msg;
                messageTextEdit.SelectedText = "\n";

                // restore current cursor position
                if ( ! atEnd) messageTextEdit.SelectionStart = curPos;
            }

            handlingMsg = false;
        }


        /****************************************************************************************************
        * NAME: Method: void openLogFile()
        * 
        * DESCRIPTION: Open the log file
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void openLogFile()
        {
            string appPath = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            appPath = appPath.Replace("file:\\", "");

            string logPath = appPath + "\\" + appName + ".log";

            // try to open the log file up to 10 times (this app might have several instances)
            const int N_LOG_FILE_ATTEMPTS = 10;

            for (int i = 0; i < N_LOG_FILE_ATTEMPTS; i++)
            {
                try
                {
                    string actualLogPath = logPath;

                    if (i > 0) actualLogPath = logPath.Replace(".log", "-" + i.ToString() + ".log");

                    pLogFile = System.IO.File.Open(actualLogPath, System.IO.FileMode.Create,
                                                   System.IO.FileAccess.Write, System.IO.FileShare.Read);

                    string appData = appPath;

                    try
                    {
                        System.IO.FileInfo appFileInfo = new System.IO.FileInfo(appData);

                        appData += " (created on ";
                        appData += appFileInfo.LastWriteTime.ToString();
                        appData += ")\r\n\r\n";

                        pLogFile.Write(System.Text.ASCIIEncoding.ASCII.GetBytes(appData), 0, appData.Length);
                        pLogFile.Flush();
                    }
                    catch (Exception) {}

                    notice("Logging to \"" + actualLogPath + "\"\n");

                    break;
                }
                catch (Exception e)
                {
                    if (i == N_LOG_FILE_ATTEMPTS - 1)
                    {
                        pLogFile = null;

                        if (logCheckBox != null) logCheckBox.Checked = false; ;

                        warning(e.Message + "\n");
                    }
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: bool isLoggingEnabled(), bool inVerboseMode()
        * 
        * DESCRIPTION: Check logging and verbose modes
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isLoggingEnabled() {return (logCheckBox     != null && logCheckBox.    Checked);}
        public bool inVerboseMode()    {return (verboseCheckBox != null && verboseCheckBox.Checked);}


        /****************************************************************************************************
        * NAME: Method: SendMode getSendMode(), void setSendMode()
        * 
        * DESCRIPTION: Get or Set sendMode
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 9 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public SendMode getSendMode()          {return sendMode;}
        public void setSendMode(SendMode mode) {sendMode = mode;}


        /****************************************************************************************************
        * NAME: Method: CommWidget getCurCommWidget(), void setCurCommWidget()
        * 
        * DESCRIPTION: Get or Set current comm widget
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 12 Nov 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public CommWidget getCurCommWidget() {return curCommWidget;}

        public void setCurCommWidget(int id) // one-based
        {
            if (id < 1)
            {
                warning("Invalid Link ID '" + id.ToString() + "' using '1' instead");
                id = 1;
            }
            else if (id > commWidget.Length)
            {
                warning("Invalid Link ID '" + id.ToString() + "' using '" +
                        (commWidget.Length).ToString() + "' instead");
                id = commWidget.Length;
            }

            curCommWidget = commWidget[id - 1];
        }


        /****************************************************************************************************
        * NAME: Method: void setupRecentSocketMsg()
        * 
        * DESCRIPTION: Set the recent socket message timeout and count
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void setupRecentSocketMsg(ushort msgCount, int msecInterval)
        {
            foreach (CommWidget c in commWidget) c.setupRecentSocketMsg(msgCount, msecInterval);
        }


        /****************************************************************************************************
        * NAME: Method: bool isDuplicateMsgFromOtherLinks()
        * 
        * DESCRIPTION: Check other links for duplicate message
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isDuplicateMsgFromOtherLinks(byte[] msg, uint length, CommWidget c)
        {
            foreach (CommWidget otherCommWidget in commWidget)
            {
                if (c == otherCommWidget) continue;

                if (otherCommWidget.isDuplicateMsg(msg, length, getCurTimeSec())) return true;
            }

            return false;
        }


        /****************************************************************************************************
        * NAME: Method: bool isRunning()
        * 
        * DESCRIPTION: Is the tester running
        * 
        *   TODO: finish documenting
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 20 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isRunning() {return running;}


        // private data members

        private string                      appName;
        private bool                        running            = false;

        private Timer                       eventLoopTimer     = null;

        private SendMode                    sendMode           = SendMode.Alternate_Links;
        private int                         lastUsedCommIndex  = -1;

        private SplitContainer              splitter           = null;
        protected GroupBox                  commGroupBox       = null;
        private CommWidget[]                commWidget         = null;
        private CommWidget                  curCommWidget      = null;
        private RichTextBox                 messageTextEdit    = null;
        protected CheckBox                  logCheckBox        = null;
        protected CheckBox                  verboseCheckBox    = null;
        protected Button                    clearWindowButton  = null;
        protected Button                    startButton        = null;
        protected Button                    stopButton         = null;

        protected StatusStrip               statusBar          = null;
        private ToolStripStatusLabel        sentStatusLabel    = null;
        private ToolStripStatusLabel        recvStatusLabel    = null;
        private ToolStripStatusLabel        stretchStatusLabel = null;

        protected ToolTip                   toolTip            = null;

        private Microsoft.Win32.RegistryKey pSettings          = null;
        private static System.IO.FileStream pLogFile           = null;

        private ulong                       totalBytesSent     = 0;
        private ulong                       totalBytesRecv     = 0;

        private bool                        dosInitialized     = false;
        private Button                      curDOSButton       = null;
        private String                      curDOSButtonText   = "";
        private Label                       dosFileLabel       = null;
        private Button                      dosFileButton      = null;
        private NumericUpDown               dosSpinBox         = null;
        private string                      dosFilename        = "dos.txt";
        private byte[]                      dosBuffer;
        private ulong                       dosSize            = 0;
        private Timer                       dosTimer           = null;
        private double                      dosStartTime       = 0.0;
        private double                      dosFinishTime      = 0.0;
    }
}