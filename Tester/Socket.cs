
/*****************************************************************************
* FILE: SocketObj.cs
*  
* DESCRIPTION: Generic Socket class for handling TCP-IP and UDP-IP sockets
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 27 Feb 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Tester
{
    public class SocketObj : IDisposable
    {
        public enum Type {DATAGRAM, STREAM, N_TYPES};
        public enum Role {SERVER, CLIENT, AUTO, N_ROLES};

        public const byte STX = 2;  // start of text
        public const byte ETX = 3;  // end of text
        public const byte ACK = 6;  // acknowledged
        public const byte NAK = 21; // not acknowledged
        public const byte LF  = 10; // line feed
        public const byte CR  = 13; // carriage return


        /****************************************************************************************************
        * NAME: Constructor: SocketObj()
        * 
        * DESCRIPTION: Constructs an instance of this class
        * 
        * PARAMETERS:
        *   _type       - socket type (DATAGRAM, STREAM)
        *   _mainWindow - the main window (for messaging and GUI stuff)
        *   _msgHeader  - string to prepend to messages
        * 
        * VARIABLES:
        *   sendSocket               Local   - send socket
        *   recvSocket               Local   - receive socket
        *   InterNetwork                       <System.Net.Sockets.AddressFamily>
        *   Dgram                              <System.Net.Sockets.SocketType>
        *   Stream                             <System.Net.Sockets.SocketType>
        *   Udp                                <System.Net.Sockets.ProtocolType>
        *   Tcp                                <System.Net.Sockets.ProtocolType>
        * 
        * FUNCTIONS:
        *   Socket()                           <System.Net.Sockets>
        *   warning()                Local   - displays a warning message
        *   ToString()                         <System.Convert>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public SocketObj(Type _type, TesterMainWindow _mainWindow, string _msgHeader)
        {
            type       = _type;
            mainWindow = _mainWindow;
            msgHeader  = _msgHeader;

            try
            {
                if (type == Type.DATAGRAM) // UDP
                {
                    sendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    recvSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                }
                else if (type == Type.STREAM) // TCP
                {
                    sendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    recvSocket = sendSocket;

                    sendSocket.NoDelay = true;
                }
                else
                {
                    warning("unrecognized socket type" + Convert.ToString(type));
                }
            }
            catch (Exception e)
            {
                warning(e.Source + ": failed to create socket (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Destructor: ~SocketObj(), void Dispose()
        * 
        * DESCRIPTION: Disposes of this class
        * 
        * VARIABLES:
        *   sendSocket               Local   - send socket
        *   recvSocket               Local   - receive socket
        *   GC                                 <System>
        * 
        * FUNCTIONS:
        *   Close()                            <System.Net.Sockets.Socket>
        *   SuppressFinalize()                 <System.GC>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        ~SocketObj() {Dispose();}

        public void Dispose()
        {
            if (sendSocket != null)                             sendSocket.Close();
            if (recvSocket != null && recvSocket != sendSocket) recvSocket.Close();

            GC.SuppressFinalize(this);
        }


        /****************************************************************************************************
        * NAME: Method: IPAddress getLocalIPAddress()
        * 
        * DESCRIPTION: Get local IP address
        * 
        * VARIABLES:
        *   AddressList                        <System.Net.IPHostEntry>
        * 
        * FUNCTIONS:
        *   GetHostEntry()                     <System.Net.Dns>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public IPAddress getLocalIPAddress()
        {
            return Dns.GetHostEntry("").AddressList[0];
        }


        /****************************************************************************************************
        * NAME: Method: bool connect()
        * 
        * DESCRIPTION: Connect socket to IP addresses and ports
        * 
        * PARAMETERS:
        *   localIPAddressStr  - local IP address
        *   remoteIPAddressStr - remote IP address
        *   _localPort         - local port
        *   _remotePort        - remote port
        *   role               - socket role (SERVER, CLIENT, AUTO)
        * 
        * VARIABLES:
        *   type                     Local   - DATAGRAM or STREAM
        *   localAddr                Local   - local address (IP and port)
        *   connectedAsServer        Local   - boolean whether the socket connected as a server
        *   recvSocket               Local   - receive socket
        *   localIPAddress           Local   - local IP address
        *   remoteIPAddress          Local   - remote IP address
        *   localPort                Local   - remote local
        *   remotePort               Local   - remote port
        *   mainWindow               Local   - Tester main window
        *   Cursor                             <System.Windows.Forms>
        *   WaitCursor                         <System.Windows.Forms.Cursors>
        *   sendSocket               Local   - send socket
        *   connected                Local   - boolean whether this socket is connected
        *   Default                            <System.Windows.Forms.Cursors>
        *   bound                    Local   - boolean whether socket has been bound
        *   note                     Local   - temp message string
        *   Socket                             <System.Net.Sockets.SocketOptionLevel>
        *   ReuseAddress                       <System.Net.Sockets.SocketOptionName>
        *  
        * FUNCTIONS:
        *   IPEndPoint()                       <System.Net>
        *   Parse()                            <System.Net.IPAddress>
        *   Bind()                             <System.Net.Sockets.Socket>
        *   notice()                 Local   - Display a notice message
        *   ToString()                         <System.Convert>
        *   Listen()                           <System.Net.Sockets.Socket>
        *   warning()                Local   - Display a warning message
        *   Accept()                           <System.Net.Sockets.Socket>
        *   Close()                            <System.Net.Sockets.Socket>
        *   Connect()                          <System.Net.Sockets.Socket>
        *   SetSocketOption()                  <System.Net.Sockets.Socket>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool connect(
            string localIPAddressStr,
            string remoteIPAddressStr,
            ushort _localPort,
            ushort _remotePort,
            Role   role
        )
        {
            if (type == Type.STREAM) _remotePort = _localPort;

            IPEndPoint localAddr = new IPEndPoint(IPAddress.Parse(localIPAddressStr), _localPort);

            if (type == Type.STREAM)
            {
                bool connectedAsServer = false;

                // try to be the server first
                if (role != Role.CLIENT)
                {
                    try
                    {
                        recvSocket.Bind(localAddr);
                        connectedAsServer = true;
                    }
                    catch (Exception)
                    {
                        connectedAsServer = false;
                    }
                }

                if (connectedAsServer)
                {
                    localIPAddress  = IPAddress.Parse(localIPAddressStr);
                    remoteIPAddress = IPAddress.Parse(remoteIPAddressStr);
                    localPort       = _localPort;
                    remotePort      = _remotePort;

                    notice("Acting as server on " + localIPAddressStr + ":" + Convert.ToString(localPort));

                    mainWindow.Cursor = Cursors.WaitCursor;

                    notice("(TCP mode is unresponsive until a client connects)");

                    try
                    {
                        recvSocket.Listen(65535);
                    }
                    catch (Exception e)
                    {
                        warning("Listen failed", e);
                        return false;
                    }

                    try
                    {
                        sendSocket = recvSocket.Accept();

                        // client accepted
                        recvSocket.Close();
                        recvSocket = sendSocket;

                        notice("Client accepted");
                        connected = true;
                    }
                    catch (Exception e)
                    {
                        warning("Client accept failed", e);
                    }

                    mainWindow.Cursor = Cursors.Default;
                }
                // else try to connect as a client
                else if (role != Role.SERVER)
                {
                    try
                    {
                        IPEndPoint remoteAddr = new IPEndPoint(IPAddress.Parse(remoteIPAddressStr), _remotePort);
                        sendSocket.Connect(remoteAddr);

                        localIPAddress  = IPAddress.Parse(localIPAddressStr);
                        remoteIPAddress = IPAddress.Parse(remoteIPAddressStr);
                        localPort       = _localPort;
                        remotePort      = _remotePort;

                        connected = true;

                        notice("Connected as client to " + remoteIPAddressStr + ":" + Convert.ToString(remotePort));
                    }
                    catch (Exception e)
                    {
                        warning("Failed to connect socket " + remoteIPAddressStr + ":" + Convert.ToString(_remotePort), e);
                    }
                }
                else
                {
                    warning("Failed to bind socket on " + localIPAddressStr + ":" + Convert.ToString(_localPort));
                }
            }
            else if (type == Type.DATAGRAM)
            {
                // start by listening
                bool bound = false;

                try
                {
                    recvSocket.SetSocketOption(SocketOptionLevel.Socket,
                                               SocketOptionName.ReuseAddress, true);
                    recvSocket.Bind(localAddr);
                    bound = true;
                }
                catch (Exception)
                {
                    bound = false;
                }

                string note = "";

                if ( ! bound)
                {
                    // try to reuse the port (already in use)
                    try
                    {
                        recvSocket.SetSocketOption(SocketOptionLevel.Socket,
                                                   SocketOptionName.ReuseAddress, true);
                        recvSocket.Bind(localAddr);

                        bound = true;
                        note = "    [Note: Port is also being used by another application]";
                    }
                    catch (Exception)
                    {
                        bound = false;
                    }
                }

                if (bound)
                {
                    localIPAddress  = IPAddress.Parse(localIPAddressStr);
                    remoteIPAddress = IPAddress.Parse(remoteIPAddressStr);
                    localPort       = _localPort;
                    remotePort      = _remotePort;

                    connected = true;

                    notice("Receiving on " + localIPAddressStr  + ":" + Convert.ToString(localPort) + note);
                    notice("Sending to "   + remoteIPAddressStr + ":" + Convert.ToString(remotePort));
                }
                else
                {
                    warning("Failed to bind socket on " + localIPAddressStr + ":" + Convert.ToString(_localPort));
                }
            }

            return connected;
        }

        public void connect(ushort localPort, ushort remotePort, Role role)
        {
            connect("127.0.0.1", "127.0.0.1", localPort, remotePort, role);
        }

        public void connect(ushort localPort, ushort remotePort)
        {
            connect("127.0.0.1", "127.0.0.1", localPort, remotePort, Role.AUTO);
        }

        public void connect(ushort localPort) {connect(localPort, 0);}


        /****************************************************************************************************
        * NAME: Method: bool isConnected()
        * 
        * DESCRIPTION: Return state of socket connection
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isConnected()    {return connected;}
        public bool isNotConnected() {return ! connected;}


        /****************************************************************************************************
        * NAME: Method: byte[] receiveBytes()
        * 
        * DESCRIPTION: Check for bytes on the socket
        * 
        * PARAMETERS:
        *   totalBytesReceived - total bytes taken off the socket
        * 
        * VARIABLES:
        *   bytesAvailable           Local   - bytes currently on recvSocket
        *   recvSocket               Local   - receive socket
        *   Available                          <System.Net.Sockets.Socket>
        *   buffer                   Local   - temporary holder for bytes from the socket
        *   bytesReceived            Local   - keeps track of bytes removed from socket
        *   mainWindow               Local   - Tester main window
        * 
        * FUNCTIONS:
        *   Receive()                          <System.Net.Sockets.Socket>
        *   notice()                 Local   - displays a notice message
        *   warning()                Local   - displays a warning message
        *   handleRecvBytes()        Library - <TesterMainWindow.cs>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public byte[] receiveBytes(out ulong totalBytesReceived)
        {
            totalBytesReceived = 0;

            int bytesAvailable = recvSocket.Available;

            if (bytesAvailable == 0) return new byte[0];

            byte[] buffer = new byte[bytesAvailable];

            while ((int)totalBytesReceived < bytesAvailable)
            {
                try
                {
                    int bytesReceived = recvSocket.Receive(
                        buffer, (int)totalBytesReceived, bytesAvailable - (int)totalBytesReceived, 0);

                    if (bytesReceived <= 0)
                    {
                        notice("Connection closed");
                        return buffer;
                    }

                    totalBytesReceived += (ulong)bytesReceived;
                }
                catch (Exception e)
                {
                    warning("Socket read failed", e);
                    return buffer;
                }
            }

            mainWindow.handleRecvBytes(this, buffer, totalBytesReceived);

            return buffer;
        }


        /****************************************************************************************************
        * NAME: Method: void sendByte()
        * 
        * DESCRIPTION: Send one byte over the socket
        * 
        * PARAMETERS:
        *   oneByte
        *   desc      - description of the message
        *   delayMSec - optional delay in milliseconds
        * 
        * VARIABLES:
        *   buffer                   Local   - bytes to send
        * 
        * FUNCTIONS:
        *   sendBytes                Local   - sends bytes over socket
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void sendByte(byte oneByte, string desc)
        {
            byte[] buffer = {oneByte};
            sendBytes(buffer, 1, desc);
        }

        public void sendByte(byte oneByte, string desc, int delayMSec)
        {
            byte[] buffer = { oneByte };
            sendBytes(buffer, 1, desc, delayMSec);
        }


        /****************************************************************************************************
        * NAME: Method: void sendBytes()
        * 
        * DESCRIPTION: Send bytes over the socket
        * 
        * PARAMETERS:
        *   buffer - bytes to send
        *   length - length of buffer
        *   desc   - description of the message
        *   delayMSec - optional delay in milliseconds
        * 
        * VARIABLES:
        *   sendSocket                Local   - send socket
        *   Length                              <string>
        *   remoteIPAddress           Local   - remote IP address
        *   Connected                           <System.Net.Sockets.Socket>
        *   remoteAddr                          <System.Net.IPEndPoint>
        *   remotePort                Local   - remote port
        *   remoteAddr                Local   - remote address (IP and port)
        *   LocalEndPoint                       <System.Net.Sockets.Socket>
        *   maxBuf                    Local   - maximum bytes allowed on socket
        *   Socket                              <System.Net.Sockets.SocketOptionLevel>
        *   SendBuffer                          <System.Net.Sockets.SocketOptionName>
        *   bytesSent                 Local   - actual number of bytes sent
        *   curLength                 Local   - current length left in buffer
        *   forStr                    Local   - temp string used if there is a description
        *   mainWindow                Local   - Tester main window
        * 
        * FUNCTIONS:
        *   notice()                  Local   - displays a notice message
        *   IPEndPoint()                        <System.Net>
        *   Connect()                           <System.Net.Sockets.Socket>
        *   ToString()                          <System.Net.EndPoint>
        *   warning()                 Local   - displays a warning message
        *   GetSocketOption()                   <System.Net.Sockets.Socket>
        *   Send()                              <System.Net.Sockets.Socket>
        *   ToString()                          <System.Convert>
        *   handleSentBytes()         Library - <TesterMainWindow.cs>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void sendBytes(byte[] buffer, ulong length, string desc)
        {
            if (sendSocket == null)
            {
                if (desc == null || desc.Length <= 0) desc = "data";
                notice("Failed to send " + desc + " over socket (invalid socket)");
                return;
            }

            if (remoteIPAddress == null)
            {
                if (desc == null || desc.Length <= 0) desc = "data";
                notice("Failed to send " + desc + " over socket (not connected)");
                return;
            }

            if ( ! sendSocket.Connected)
            {
                IPEndPoint remoteAddr = new IPEndPoint(remoteIPAddress, remotePort);

                try
                {
                    sendSocket.Connect(remoteAddr);

                    notice("Sending out local port " + sendSocket.LocalEndPoint.ToString());
                }
                catch (Exception e)
                {
                    if (desc == null || desc.Length <= 0) desc = "data";
                    warning("Failed to send " + desc + " over socket (could not connect)", e);
                    return;
                }
            }

            int maxBuf = (int)sendSocket.GetSocketOption(SocketOptionLevel.Socket,
                                                         SocketOptionName.SendBuffer);
            int bytesSent = 0;

            while (bytesSent < (int)length)
            {
                int curLength = (int)length - bytesSent;
                if (curLength > maxBuf) curLength = maxBuf;

                try
                {
                    bytesSent += sendSocket.Send(buffer, bytesSent, curLength, 0);
                }
                catch (Exception e)
                {
                    if (desc == null || desc.Length <= 0) desc = "data";
                    warning("Failed to send " + desc + " over socket", e);
                    break;
                }
            }

            if (bytesSent == 0)
            {
                if (desc == null || desc.Length <= 0) desc = "data";
                warning("Failed to send " + desc + " over socket");
            }
            else if (bytesSent != (int)length)
            {
                string forStr = "";
                if (desc != null && desc.Length > 0) forStr = " for " + desc;

                warning("Only sent " + Convert.ToString(bytesSent) +
                        " of " + Convert.ToString(length) + " bytes" + forStr);
            }
            else
            {
                mainWindow.handleSentBytes(this, buffer, length, desc);
            }
        }


        /****************************************************************************************************
        * NAME: Class: SocketMsg
        * 
        * DESCRIPTION: Defines a socket message
        * 
        * MEMBERS:
        *   buffer - message content
        *   length - content length
        *   desc   - human readable description
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 25 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private class SocketMsg
        {
            public SocketMsg(byte[] b, ulong l, string d) {buffer = b; length = l; desc = d;}
            public byte[] buffer;
            public ulong  length;
            public string desc;
        }


        /****************************************************************************************************
        * NAME: Method: void sendBytes()
        * 
        * DESCRIPTION: Send bytes over the socket (optionally delayed)
        * 
        * PARAMETERS:
        *   buffer    - bytes to send
        *   length    - length of buffer
        *   desc      - description of the message
        *   delayMSec - optional delay in milliseconds
        *   timer     - timer created on initial call to sendBytes() that was was delayed
        * 
        * VARIABLES:
        *   timer                     Local   - timer created on the fly to delay messages
        *   socketMsg                 Local   - original socket message that was to be delayed
        * 
        * FUNCTIONS:
        *   Add()                     Library - <System.Collections.Generic.Dictionary>
        *   Start()                   Library - <System.Windows.Forms.Timer>
        *   sendBytes()               Local   - sends bytes over the socket
        *   Stop()                    Library - <System.Windows.Forms.Timer>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 25 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        // TODO: make this a SortedDictionary? but need to implement IComparable or something on SocketMsg...
        private Dictionary<Timer,SocketMsg> delayedMsgMap = new Dictionary<Timer,SocketMsg>();

        public void sendBytes(byte[] buffer, ulong length, string desc, int delayMSec)
        {
            if (delayMSec > 0)
            {
                Timer timer    = new Timer();
                timer.Tag      = desc;
                timer.Interval = delayMSec;
                timer.Tick    += sendBytes;

                delayedMsgMap.Add(timer, new SocketMsg(buffer, length, desc));

                timer.Start();
            }
            else
            {
                sendBytes(buffer, length, desc);
            }
        }

        private void sendBytes(object timer, EventArgs unusedArgs)
        {
            ((Timer)timer).Stop();

            SocketMsg socketMsg;

            if (delayedMsgMap.TryGetValue((Timer)timer, out socketMsg))
            {
                sendBytes(socketMsg.buffer, socketMsg.length, socketMsg.desc);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void notice()
        * 
        * DESCRIPTION: Display a notice message
        * 
        * PARAMETERS:
        *   msg - message to display
        * 
        * VARIABLES:
        *   msgHeader                Local   - message header
        * 
        * FUNCTIONS:
        *   notice()                 Library - <TesterMainWindow.cs>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 28 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void notice(string msg)
        {
            if (mainWindow != null) mainWindow.notice(msgHeader + msg);
        }


        /****************************************************************************************************
        * NAME: Method: void warning()
        * 
        * DESCRIPTION: Display a warning message
        * 
        * PARAMETERS:
        *   msg - message to display
        *   e   - exception object
        * 
        * VARIABLES:
        *   msgHeader                Local   - message header
        * 
        * FUNCTIONS:
        *   warning()                Library - <TesterMainWindow.cs>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 28 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void warning(string msg, Exception e)
        {
            msg += " (" + e.Message + ")";
            warning(msg);
        }

        private void warning(string msg)
        {
            if (mainWindow != null) mainWindow.warning(msgHeader + msg);
        }


        /****************************************************************************************************
        * NAME: Method: void setMessageHeader() and void getMessageHeader()
        * 
        * DESCRIPTION: Set or get the optional header to appear at the front of messages
        * 
        * PARAMETERS:
        *   header - string to prepend to messages
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void   setMessageHeader(string header) {msgHeader = header;}
        public string getMessageHeader()              {return msgHeader;}


        // private data members

        private Socket    sendSocket      = null; // these two sockets
        private Socket    recvSocket      = null; // may be the same

        private Type      type            = Type.DATAGRAM;

        private IPAddress localIPAddress;
        private int       localPort       = 0;

        private IPAddress remoteIPAddress;        // for datagram sockets
        private int       remotePort      = 0;    // for datagram sockets

        private bool      connected       = false;

        private string    msgHeader       = "";

        // main window object - used for messages, wait cursors, and bytes sent/rcvd display
        private TesterMainWindow mainWindow = null;
    }
}
