
/*****************************************************************************
* FILE: CommWidget.cs
*  
* DESCRIPTION: GUI elements for one socket connection
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 5 Sep 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Tester
{
    public class CommWidget
    {
        /****************************************************************************************************
        * NAME: Constructor: CommWidget()
        * 
        * DESCRIPTION: Constructs an instance of this class and initializes GUI elements
        * 
        * PARAMETERS:
        *   mainLayout - the main layout for the GUI
        *   _id        - the ID of this comm widget, used if there are multiple comm widgets (links)
        * 
        * VARIABLES: basic GUI variables needed for links
        * FUNCTIONS: basic GUI functions to add the GUI elements
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public CommWidget(TableLayoutPanel mainLayout, int _id)
        {
            id   = _id;
            name = "Link" + id.ToString();

            layout = new TableLayoutPanel();

            layout.SuspendLayout();

            mainLayout.Controls.Add(layout, 0, 0);
            mainLayout.RowCount = mainLayout.RowCount + 1;
            mainLayout.RowStyles.Insert(0, new RowStyle()); // auto-size

            layout.AutoSize    = true;
            layout.Dock        = DockStyle.Fill;
            layout.RowCount    = 1;
            layout.ColumnCount = 10;

            layout.RowStyles.   Add(new RowStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle());
            layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            layout.ColumnStyles.Add(new ColumnStyle());

            pUDPRadioButton          = new RadioButton();
            pTCPRadioButton          = new RadioButton();
            pIPAddressLabel          = new Label();
            pLocalIPAddressLineEdit  = new TextBox();
            pRemoteIPAddressLineEdit = new TextBox();
            pPortLabel               = new Label();
            pPort1SpinBox            = new NumericUpDown();
            pPort2SpinBox            = new NumericUpDown();
            pServerRadioButton       = new RadioButton();
            pClientRadioButton       = new RadioButton();
            pAutoRadioButton         = new RadioButton();
            socketRoleButtonGroup    = new TableLayoutPanel();

            // default values:
            // - UDP
            // - IP 127.0.0.1
            // - Ports 50730 and 50731
            // - Auto Detect Server/Client

            // pUDPRadioButton
            pUDPRadioButton.Anchor = AnchorStyles.None;
            pUDPRadioButton.AutoSize = true;
            pUDPRadioButton.Checked = true;
            pUDPRadioButton.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pUDPRadioButton.Text = "&UDP";
            pUDPRadioButton.CheckedChanged += handleUDPToggled;

            // pTCPRadioButton
            pTCPRadioButton.Anchor = AnchorStyles.None;
            pTCPRadioButton.AutoSize = true;
            pTCPRadioButton.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pTCPRadioButton.Text = "&TCP";

            // pIPAddressLabel
            pIPAddressLabel.Anchor = AnchorStyles.None;
            pIPAddressLabel.AutoSize = true;
            pIPAddressLabel.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pIPAddressLabel.Text = "&IP Address (local/remote)";
            pIPAddressLabel.TextAlign = ContentAlignment.MiddleRight;

            // pLocalIPAddressLineEdit
            pLocalIPAddressLineEdit.Anchor = AnchorStyles.None;
            pLocalIPAddressLineEdit.MaximumSize = new Size(100, 20);
            pLocalIPAddressLineEdit.MaxLength = 15;
            pLocalIPAddressLineEdit.MinimumSize = new Size(100, 20);
            pLocalIPAddressLineEdit.Text = "127.0.0.1";
            pLocalIPAddressLineEdit.TextAlign = HorizontalAlignment.Right;

            // pRemoteIPAddressLineEdit
            pRemoteIPAddressLineEdit.Anchor = AnchorStyles.None;
            pRemoteIPAddressLineEdit.MaximumSize = new Size(100, 20);
            pRemoteIPAddressLineEdit.MaxLength = 15;
            pRemoteIPAddressLineEdit.MinimumSize = new Size(100, 20);
            pRemoteIPAddressLineEdit.Text = "127.0.0.1";
            pRemoteIPAddressLineEdit.TextAlign = HorizontalAlignment.Right;

            // pPortLabel
            pPortLabel.Anchor = AnchorStyles.None;
            pPortLabel.AutoSize = true;
            pPortLabel.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pPortLabel.Text = "&Ports (local/remote)";
            pPortLabel.TextAlign = ContentAlignment.MiddleRight;

            // pPort1SpinBox
            pPort1SpinBox.Anchor = AnchorStyles.None;
            pPort1SpinBox.AutoSize = true;
            pPort1SpinBox.Maximum = 65535;
            pPort1SpinBox.Value = 50730;

            // pPort2SpinBox
            pPort2SpinBox.Anchor = AnchorStyles.None;
            pPort2SpinBox.AutoSize = true;
            pPort2SpinBox.Maximum = 65535;
            pPort2SpinBox.Value = 50731;

            // pServerRadioButton
            pServerRadioButton.Anchor = AnchorStyles.None;
            pServerRadioButton.AutoSize = true;
            pServerRadioButton.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pServerRadioButton.Text = "&Server";

            // pClientRadioButton
            pClientRadioButton.Anchor = AnchorStyles.None;
            pClientRadioButton.AutoSize = true;
            pClientRadioButton.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pClientRadioButton.Text = "&Client";

            // pAutoRadioButton
            pAutoRadioButton.Anchor = AnchorStyles.None;
            pAutoRadioButton.AutoSize = true;
            pAutoRadioButton.Checked = true;
            pAutoRadioButton.Font = new Font(FontFamily.GenericSansSerif, 11, GraphicsUnit.Pixel);
            pAutoRadioButton.Text = "&Auto";

            // socket role button group
            socketRoleButtonGroup = new TableLayoutPanel();

            socketRoleButtonGroup.Visible     = false;
            socketRoleButtonGroup.AutoSize    = true;
            socketRoleButtonGroup.Dock        = DockStyle.Fill;
            socketRoleButtonGroup.Margin      = new Padding(0);
            socketRoleButtonGroup.RowCount    = 1;
            socketRoleButtonGroup.ColumnCount = 3;

            socketRoleButtonGroup.RowStyles.   Add(new RowStyle());
            socketRoleButtonGroup.ColumnStyles.Add(new ColumnStyle());
            socketRoleButtonGroup.ColumnStyles.Add(new ColumnStyle());
            socketRoleButtonGroup.ColumnStyles.Add(new ColumnStyle());

            // add widgets to layout
            layout.Controls.Add(pUDPRadioButton,          0, 0);
            layout.Controls.Add(pTCPRadioButton,          1, 0);
            layout.Controls.Add(pIPAddressLabel,          2, 0);
            layout.Controls.Add(pLocalIPAddressLineEdit,  3, 0);
            layout.Controls.Add(pRemoteIPAddressLineEdit, 4, 0);
            layout.Controls.Add(pPortLabel,               5, 0);
            layout.Controls.Add(pPort1SpinBox,            6, 0);
            layout.Controls.Add(pPort2SpinBox,            7, 0);

            socketRoleButtonGroup.Controls.Add(pAutoRadioButton);
            socketRoleButtonGroup.Controls.Add(pClientRadioButton);
            socketRoleButtonGroup.Controls.Add(pServerRadioButton);
            layout.Controls.Add(socketRoleButtonGroup,    9, 0);

            layout.ResumeLayout();
        }


        /****************************************************************************************************
        * NAME: Method: void restoreSettings()
        * 
        * DESCRIPTION: Restore settings from the registry
        * 
        * PARAMETERS:
        *   mainSettings - this application's registry settings
        * 
        * VARIABLES:
        *   pSettings                Local   - registry settings object
        *   pUDPRadioButton          Local   - UDP radio button
        *   pTCPRadioButton          Local   - TCP radio button
        *   pLocalIPAddressLineEdit  Local   - local IP Address line edit
        *   pRemoteIPAddressLineEdit Local   - remote IP Address line edit
        *   pPort1SpinBox            Local   - Port 1 spin box
        *   pPort2SpinBox            Local   - Port 2 spin box
        *   pServerRadioButton       Local   - server radio button
        *   pClientRadioButton       Local   - client radio button
        *   pAutoRadioButton         Local   - auto ratio button
        * 
        * FUNCTIONS:
        *   CreateSubKey()           Library - <Microsoft.Win32.RegistryKey>
        *   GetValue()               Library - <Microsoft.Win32.RegistryKey>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void restoreSettings(Microsoft.Win32.RegistryKey mainSettings)
        {
            Microsoft.Win32.RegistryKey pSettings = mainSettings.CreateSubKey(name);

            if (pSettings == null) return;

            bool useUDP = Convert.ToBoolean(pSettings.GetValue("useUDP", pUDPRadioButton.Checked));

            if (useUDP) pUDPRadioButton.Checked = true;
            else        pTCPRadioButton.Checked = true;

            pLocalIPAddressLineEdit. Text = (string)pSettings.GetValue("localIP",  pLocalIPAddressLineEdit. Text);
            pRemoteIPAddressLineEdit.Text = (string)pSettings.GetValue("remoteIP", pRemoteIPAddressLineEdit.Text);

            pPort1SpinBox.Value = Convert.ToDecimal(pSettings.GetValue("port1", pPort1SpinBox.Value));
            pPort2SpinBox.Value = Convert.ToDecimal(pSettings.GetValue("port2", pPort2SpinBox.Value));

            SocketObj.Role socketRole = (SocketObj.Role)pSettings.GetValue("socketRole", SocketObj.Role.AUTO);

            if      (socketRole == SocketObj.Role.SERVER) pServerRadioButton. Checked = true;
            else if (socketRole == SocketObj.Role.CLIENT) pClientRadioButton. Checked = true;
            else                                          pAutoRadioButton.   Checked = true;
        }


        /****************************************************************************************************
        * NAME: Method: void rememberSettings()
        * 
        * DESCRIPTION: Remember settings in the registry
        * 
        * PARAMETERS:
        *   mainSettings - this application's registry settings
        * 
        * VARIABLES:
        *   pSettings                Local   - registry settings object
        *   pUDPRadioButton          Local   - UDP radio button
        *   pLocalIPAddressLineEdit  Local   - local IP Address line edit
        *   pRemoteIPAddressLineEdit Local   - remote IP Address line edit
        *   pPort1SpinBox            Local   - Port 1 spin box
        *   pPort2SpinBox            Local   - Port 2 spin box
        *   pServerRadioButton       Local   - server radio button
        *   pClientRadioButton       Local   - client radio button
        * 
        * FUNCTIONS:
        *   CreateSubKey()           Library - <Microsoft.Win32.RegistryKey>
        *   SetValue()               Library - <Microsoft.Win32.RegistryKey>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void rememberSettings(Microsoft.Win32.RegistryKey mainSettings)
        {
            Microsoft.Win32.RegistryKey pSettings = mainSettings.CreateSubKey(name);

            if (pSettings == null) return;

            pSettings.SetValue("useUDP",   pUDPRadioButton.Checked);

            pSettings.SetValue("localIP",  pLocalIPAddressLineEdit.Text);
            pSettings.SetValue("remoteIP", pRemoteIPAddressLineEdit.Text);

            pSettings.SetValue("port1",    pPort1SpinBox.Value);
            pSettings.SetValue("port2",    pPort2SpinBox.Value);

            if      (pServerRadioButton.Checked) pSettings.SetValue("socketRole", (int)SocketObj.Role.SERVER);
            else if (pClientRadioButton.Checked) pSettings.SetValue("socketRole", (int)SocketObj.Role.CLIENT);
            else                                 pSettings.SetValue("socketRole", (int)SocketObj.Role.AUTO);
        }


        /****************************************************************************************************
        * NAME: Method: int getId()
        * 
        * DESCRIPTION: Get the link's ID
        * 
        * VARIABLES:
        *   id                       Local   - this link's ID
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public int getId() {return id;}


        /****************************************************************************************************
        * NAME: Method: bool connect()
        * 
        * DESCRIPTION: Connect the socket using the values from this object's GUIs
        * 
        * PARAMETERS:
        *   mainWindow - the main window
        * 
        * VARIABLES:
        *   socket                   Local   - SocketObj this link communicates over
        *   name                     Local   - name of link
        *   socketType               Local   - socket type: DATAGRAM or STREAM
        *   pUDPRadioButton          Local   - UDP radio button
        *   socketRole               Local   - socket role: SERVER, CLIENT, or AUTO
        *   pServerRadioButton       Local   - server radio button
        *   pClientRadioButton       Local   - client radio button
        *   pAutoRadioButton         Local   - auto radio button
        *   msgHeader                Local   - name of link plus ':'
        *   useMsgHeaders            Local   - whether to put the name of the link in front
        *   connected                Local   - whether the socket successfully connected
        *   pLocalIPAddressLineEdit  Local   - local IP address line edit
        *   pRemoteIPAddressLineEdit Local   - remote IP address line edit
        *   pPort1SpinBox            Local   - Port 1 spin box
        *   pPort2SpinBox            Local   - Port 2 spin box (UDP only)
        * 
        * FUNCTIONS:
        *   warning()                Library - <TesterMainWindow>
        *   connect()                Library - <SocketObj>
        *   Dispose()                Library - <SocketObj>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool connect(TesterMainWindow mainWindow)
        {
            if (socket != null)
            {
                mainWindow.warning(name + ": failed to create socket (already created)");
                return false;
            }

            SocketObj.Type socketType =
                pUDPRadioButton.Checked ? SocketObj.Type.DATAGRAM : SocketObj.Type.STREAM;

            // server, client, auto
            SocketObj.Role socketRole = SocketObj.Role.AUTO;

            if (socketType == SocketObj.Type.STREAM)
            {
                if      (pServerRadioButton. Checked) socketRole = SocketObj.Role.SERVER;
                else if (pClientRadioButton. Checked) socketRole = SocketObj.Role.CLIENT;
                else if (pAutoRadioButton.   Checked) socketRole = SocketObj.Role.AUTO;
            }

            string msgHeader = "";
            if (useMsgHeaders) msgHeader = name + ": ";

            // create socket
            socket = new SocketObj(socketType, mainWindow, msgHeader);

            // try to connect
            bool connected = socket.connect(pLocalIPAddressLineEdit.Text, pRemoteIPAddressLineEdit.Text,
                                            (ushort)pPort1SpinBox.Value, (ushort)pPort2SpinBox.Value, socketRole);

            if ( ! connected)
            {
                socket.Dispose();
                socket = null;
            }

            return connected;
        }


        /****************************************************************************************************
        * NAME: Method: void disconnect()
        * 
        * DESCRIPTION: Disconnect the socket
        * 
        * VARIABLES:
        *   socket                   Local   - SocketObj this link communicates over
        * 
        * FUNCTIONS:
        *   Dispose()                Library - <SocketObj>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void disconnect()
        {
            if (socket != null)
            {
                socket.Dispose();
                socket = null;
            }
        }


        /****************************************************************************************************
        * NAME: Method: bool isConnected()
        * 
        * DESCRIPTION: Returns whether the socket is connected
        * 
        * VARIABLES:
        *   socket                   Local   - SocketObj this link communicates over
        * 
        * FUNCTIONS:
        *   isConnected()            Library - <SocketObj>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isConnected() {return (socket != null && socket.isConnected());}


        /****************************************************************************************************
        * NAME: Method: byte[] receiveBytes()
        * 
        * DESCRIPTION: Check for bytes on the socket
        * 
        * PARAMETERS:
        *   totalBytesReceived - total bytes taken off the socket
        * 
        * VARIABLES:
        *   socket                   Local   - SocketObj this link communicates over
        *   msg                      Local   - message taken off the socket
        * 
        * FUNCTIONS:
        *   receiveBytes()           Library - <SocketObj>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public byte[] receiveBytes(out ulong totalBytesReceived)
        {
            if (socket != null)
            {
                return socket.receiveBytes(out totalBytesReceived);
            }

            totalBytesReceived = 0;
            return new byte[0];
        }


        /****************************************************************************************************
        * NAME: Method: void sendByte() and void sendBytes()
        * 
        * DESCRIPTION: Send bytes over the socket delayed
        * 
        * PARAMETERS:
        *   buffer    - bytes to send over the socket
        *   length    - length of bytes to send
        *   desc      - human readable description of message
        *   delayMSec - delay in milli-seconds to send the message
        * 
        * VARIABLES:
        *   socket                   Local   - SocketObj this link communicates over
        * 
        * FUNCTIONS:
        *   sendByte()               Library - <SocketObj>
        *   sendBytes()              Library - <SocketObj>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void sendByte(byte buffer, string desc, int delayMSec)
        {
            if (socket != null) socket.sendByte(buffer, desc, delayMSec);
        }

        public void sendBytes(byte[] buffer, ulong length, string desc, int delayMSec)
        {
            if (socket != null) socket.sendBytes(buffer, length, desc, delayMSec);
        }


        /****************************************************************************************************
        * NAME: Method: void useMessageHeaders()
        * 
        * DESCRIPTION: Tells the comm widgets to set the socket's message headers
        * 
        * VARIABLES:
        *   useMsgHeaders            Local   - use message headers or not
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public static void useMessageHeaders() {useMsgHeaders = true;}


        /****************************************************************************************************
        * NAME: Method: void handleUDPToggled()
        * 
        * DESCRIPTION: Handle UDP/TCP toggled
        * 
        * VARIABLES:
        *   layout                   Local   - main layout for this widget
        *   pUDPRadioButton          Local   - specifies whether using UDP or not
        *   pPortLabel               Local   - displays either "Port" or "Ports (local/remote)"
        *   pPort2SpinBox            Local   - the second port spin box (UDP only)
        *   socketRoleButtonGroup    Local   - radio buttons "Auto", "Client", or "Server" (TCP only)
        *   pTCPRadioButton          Local   - specifies whether using TCP or not
        * 
        * FUNCTIONS:
        *   SuspendLayout()          Library - <System.Windows.Forms.TableLayoutPanel>
        *   ResumeLayout()           Library - <System.Windows.Forms.TableLayoutPanel>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 27 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void handleUDPToggled(object unusedObj, EventArgs unusedArgs)
        {
            layout.SuspendLayout();

            if (pUDPRadioButton.Checked) pPortLabel.Text = "&Ports (local/remote)";
            else                         pPortLabel.Text = "&Port";

            pPort2SpinBox.Visible = pUDPRadioButton.Checked;

            socketRoleButtonGroup.Visible = pTCPRadioButton.Checked;

            layout.ResumeLayout();
        }


        /****************************************************************************************************
        * NAME: Method: void setupRecentSocketMsg()
        * 
        * DESCRIPTION: Set recent socket message timeout and count
        * 
        * PARAMETERS:
        *   msgCount     - number of recent messages to store
        *   msecInterval - timeout after which the most recent socket message is cleared
        * 
        * VARIABLES:
        *   recentSocketMsgCount     Local   - number of recent messages stored
        *   maxRecentSocketMsgCount  Local   - maximum number of recent messages to store
        *   recentSocketMsgInterval  Local   - length of time to keep recent socket messages
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void setupRecentSocketMsg(ushort msgCount, int msecInterval)
        {
            maxRecentSocketMsgCount = msgCount;
            recentSocketMsgInterval = msecInterval / 1000.0;
        }


        /****************************************************************************************************
        * NAME: Method: void rememberRecentSocketMsg()
        * 
        * DESCRIPTION: Remember recent socket message
        * 
        * PARAMETERS:
        *   msg - most recent socket message
        * 
        * VARIABLES:
        *   recentSocketMsgList      Local   - most recent socket messages
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 30 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public void rememberRecentSocketMsg(double time, byte[] msg)
        {
            if (recentSocketMsgCount == maxRecentSocketMsgCount)
            {
                foreach (KeyValuePair<double, List<byte[]> > recentSocketMsgs in recentSocketMsgList)
                {
                    if (recentSocketMsgs.Value.Count == 1)
                    {
                        recentSocketMsgList.Remove(recentSocketMsgs.Key);
                    }
                    else
                    {
                        foreach (byte[] recentSocketMsg in recentSocketMsgs.Value)
                        {
                            recentSocketMsgs.Value.Remove(recentSocketMsg);
                            break;
                        }
                    }

                    break;
                }

                recentSocketMsgCount--;
            }

            // add the new msg
            List<byte[]> _recentSocketMsgs;

            if ( ! recentSocketMsgList.TryGetValue(time, out _recentSocketMsgs))
            {
                _recentSocketMsgs = new List<byte[]>();
                recentSocketMsgList.Add(time, _recentSocketMsgs);
            }

            _recentSocketMsgs.Add(msg);
            recentSocketMsgCount++;
        }


        /****************************************************************************************************
        * NAME: Method: bool isDuplicateMsg()
        *
        * DESCRIPTION: Check for a duplicate message
        *    
        * PARAMETERS:
        *   msg - message to test
        * 
        * VARIABLES:
        *   recentSocketMsgList      Local   - the most recent messages to test against
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 2 Oct 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public bool isDuplicateMsg(byte[] msg, uint length, double time)
        {
            bool foundDuplicate = false;

            List<double> oldMsgList = null;

            foreach (KeyValuePair<double, List<byte[]> > recentSocketMsgs in recentSocketMsgList)
            {
                // if too old
                if (time - recentSocketMsgInterval > recentSocketMsgs.Key)
                {
                    if (oldMsgList == null) oldMsgList = new List<double>();

                    oldMsgList.Add(recentSocketMsgs.Key);
                    continue;
                }

                foreach (byte[] recentSocketMsg in recentSocketMsgs.Value)
                {
                    // if the messages are not the same length
                    if (recentSocketMsg.Length != length) continue;

                    // compare messages byte by byte
                    int i = 0;

                    // if one byte is different, this is not a duplicate
                    for (; i < length; i++)
                    {
                        if (msg[i] != recentSocketMsg[i]) break;
                    }

                    if (i == length)
                    {
                        foundDuplicate = true;
                        break;
                    }
                }
            }

            // remove old messages
            if (oldMsgList != null)
            {
                foreach (double oldMsgTime in oldMsgList)
                {
                    recentSocketMsgCount -= (ushort)recentSocketMsgList[oldMsgTime].Count;

                    recentSocketMsgList.Remove(oldMsgTime);
                }
            }

            return foundDuplicate;
        }


        // private data members

        private static bool      useMsgHeaders            = false;


        private int              id                       = 0;
        private string           name                     = "";
        private SocketObj        socket                   = null;

        private TableLayoutPanel layout                   = null;
        private RadioButton      pUDPRadioButton          = null;
        private RadioButton      pTCPRadioButton          = null;
        private Label            pIPAddressLabel          = null;
        private TextBox          pLocalIPAddressLineEdit  = null;
        private TextBox          pRemoteIPAddressLineEdit = null;
        private Label            pPortLabel               = null;
        private NumericUpDown    pPort1SpinBox            = null;
        private NumericUpDown    pPort2SpinBox            = null;
        private RadioButton      pServerRadioButton       = null;
        private RadioButton      pClientRadioButton       = null;
        private RadioButton      pAutoRadioButton         = null;
        private TableLayoutPanel socketRoleButtonGroup    = null;

        private Dictionary<double, List<byte[]> > recentSocketMsgList= new Dictionary<double, List<byte[]> >();
        private ushort           recentSocketMsgCount     = 0;
        private ushort           maxRecentSocketMsgCount  = 1;
        private double           recentSocketMsgInterval  = 10;
    }
}
