
/*****************************************************************************
* FILE: MainWindow.Designer.cs
*  
* DESCRIPTION: ANI Main Window (auto-generated)
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 26 Jun 2007 Jesse Elliott
******************************************************************************/

namespace CADTester
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pAutoResponseGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.delaySpinBox = new System.Windows.Forms.NumericUpDown();
            this.pResendResponseComboBox = new System.Windows.Forms.ComboBox();
            this.pResendResponseLabel = new System.Windows.Forms.Label();
            this.pInitialResponseComboBox = new System.Windows.Forms.ComboBox();
            this.pInitialResponseLabel = new System.Windows.Forms.Label();
            this.delayLabel = new System.Windows.Forms.Label();
            this.pManualResponseGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pHButton = new System.Windows.Forms.Button();
            this.pGarbageButton = new System.Windows.Forms.Button();
            this.pDOSButton = new System.Windows.Forms.Button();
            this.pNakButton = new System.Windows.Forms.Button();
            this.pAckButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.pGarbageFileButton = new System.Windows.Forms.Button();
            this.pGarbageFileLabel = new System.Windows.Forms.Label();
            this.pDOSFileButton = new System.Windows.Forms.Button();
            this.pDOSFileLabel = new System.Windows.Forms.Label();
            this.pDOSLabel = new System.Windows.Forms.Label();
            this.pDOSLineEdit = new System.Windows.Forms.NumericUpDown();
            this.pAutoResponseGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delaySpinBox)).BeginInit();
            this.pManualResponseGroupBox.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.mainLayout.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pDOSLineEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // pAutoResponseGroupBox
            // 
            this.pAutoResponseGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pAutoResponseGroupBox.AutoSize = true;
            this.pAutoResponseGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pAutoResponseGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.pAutoResponseGroupBox.Location = new System.Drawing.Point(3, 3);
            this.pAutoResponseGroupBox.Name = "pAutoResponseGroupBox";
            this.pAutoResponseGroupBox.Size = new System.Drawing.Size(328, 71);
            this.pAutoResponseGroupBox.TabIndex = 1;
            this.pAutoResponseGroupBox.TabStop = false;
            this.pAutoResponseGroupBox.Text = "Auto-Response";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.delaySpinBox, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.pResendResponseComboBox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.pResendResponseLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pInitialResponseComboBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pInitialResponseLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.delayLabel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(322, 52);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // delaySpinBox
            // 
            this.delaySpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delaySpinBox.AutoSize = true;
            this.delaySpinBox.DecimalPlaces = 1;
            this.delaySpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.delaySpinBox.Location = new System.Drawing.Point(257, 28);
            this.delaySpinBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.delaySpinBox.Name = "delaySpinBox";
            this.delaySpinBox.Size = new System.Drawing.Size(62, 20);
            this.delaySpinBox.TabIndex = 2;
            // 
            // pResendResponseComboBox
            // 
            this.pResendResponseComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pResendResponseComboBox.Location = new System.Drawing.Point(130, 28);
            this.pResendResponseComboBox.Name = "pResendResponseComboBox";
            this.pResendResponseComboBox.Size = new System.Drawing.Size(121, 21);
            this.pResendResponseComboBox.TabIndex = 1;
            // 
            // pResendResponseLabel
            // 
            this.pResendResponseLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pResendResponseLabel.AutoSize = true;
            this.pResendResponseLabel.Location = new System.Drawing.Point(133, 6);
            this.pResendResponseLabel.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.pResendResponseLabel.Name = "pResendResponseLabel";
            this.pResendResponseLabel.Size = new System.Drawing.Size(95, 13);
            this.pResendResponseLabel.TabIndex = 2;
            this.pResendResponseLabel.Text = "&Resend Response";
            // 
            // pInitialResponseComboBox
            // 
            this.pInitialResponseComboBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pInitialResponseComboBox.Location = new System.Drawing.Point(3, 28);
            this.pInitialResponseComboBox.Name = "pInitialResponseComboBox";
            this.pInitialResponseComboBox.Size = new System.Drawing.Size(121, 21);
            this.pInitialResponseComboBox.TabIndex = 0;
            // 
            // pInitialResponseLabel
            // 
            this.pInitialResponseLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pInitialResponseLabel.AutoSize = true;
            this.pInitialResponseLabel.Location = new System.Drawing.Point(6, 6);
            this.pInitialResponseLabel.Margin = new System.Windows.Forms.Padding(6, 6, 0, 6);
            this.pInitialResponseLabel.Name = "pInitialResponseLabel";
            this.pInitialResponseLabel.Size = new System.Drawing.Size(82, 13);
            this.pInitialResponseLabel.TabIndex = 0;
            this.pInitialResponseLabel.Text = "&Initial Response";
            // 
            // delayLabel
            // 
            this.delayLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.delayLabel.AutoSize = true;
            this.delayLabel.Location = new System.Drawing.Point(257, 6);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(60, 13);
            this.delayLabel.TabIndex = 3;
            this.delayLabel.Text = "&Delay (sec)";
            // 
            // pManualResponseGroupBox
            // 
            this.pManualResponseGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pManualResponseGroupBox.AutoSize = true;
            this.pManualResponseGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pManualResponseGroupBox.Controls.Add(this.tableLayoutPanel3);
            this.pManualResponseGroupBox.Location = new System.Drawing.Point(337, 3);
            this.pManualResponseGroupBox.Name = "pManualResponseGroupBox";
            this.pManualResponseGroupBox.Size = new System.Drawing.Size(475, 71);
            this.pManualResponseGroupBox.TabIndex = 2;
            this.pManualResponseGroupBox.TabStop = false;
            this.pManualResponseGroupBox.Text = "Manual-Response";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.pHButton, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.pGarbageButton, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.pDOSButton, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.pNakButton, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.pAckButton, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(469, 52);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // pHButton
            // 
            this.pHButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pHButton.AutoSize = true;
            this.pHButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pHButton.Enabled = false;
            this.pHButton.Location = new System.Drawing.Point(198, 14);
            this.pHButton.Name = "pHButton";
            this.pHButton.Size = new System.Drawing.Size(25, 23);
            this.pHButton.TabIndex = 4;
            this.pHButton.Text = "&H";
            // 
            // pGarbageButton
            // 
            this.pGarbageButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pGarbageButton.AutoSize = true;
            this.pGarbageButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pGarbageButton.Enabled = false;
            this.pGarbageButton.Location = new System.Drawing.Point(134, 14);
            this.pGarbageButton.Name = "pGarbageButton";
            this.pGarbageButton.Size = new System.Drawing.Size(58, 23);
            this.pGarbageButton.TabIndex = 3;
            this.pGarbageButton.Text = "&Garbage";
            this.pGarbageButton.Click += new System.EventHandler(this.sendGarbage);
            // 
            // pDOSButton
            // 
            this.pDOSButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pDOSButton.AutoSize = true;
            this.pDOSButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pDOSButton.Location = new System.Drawing.Point(88, 14);
            this.pDOSButton.Name = "pDOSButton";
            this.pDOSButton.Size = new System.Drawing.Size(40, 23);
            this.pDOSButton.TabIndex = 2;
            this.pDOSButton.Text = "&DOS";
            // 
            // pNakButton
            // 
            this.pNakButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pNakButton.AutoSize = true;
            this.pNakButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pNakButton.Enabled = false;
            this.pNakButton.Location = new System.Drawing.Point(45, 14);
            this.pNakButton.Name = "pNakButton";
            this.pNakButton.Size = new System.Drawing.Size(37, 23);
            this.pNakButton.TabIndex = 1;
            this.pNakButton.Text = "&Nak";
            // 
            // pAckButton
            // 
            this.pAckButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pAckButton.AutoSize = true;
            this.pAckButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pAckButton.Enabled = false;
            this.pAckButton.Location = new System.Drawing.Point(3, 14);
            this.pAckButton.Name = "pAckButton";
            this.pAckButton.Size = new System.Drawing.Size(36, 23);
            this.pAckButton.TabIndex = 0;
            this.pAckButton.Text = "&Ack";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pManualResponseGroupBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pAutoResponseGroupBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(815, 77);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // mainLayout
            // 
            this.mainLayout.ColumnCount = 1;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.mainLayout.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 0);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 2;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainLayout.Size = new System.Drawing.Size(821, 305);
            this.mainLayout.TabIndex = 13;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.pGarbageFileButton, 6, 0);
            this.tableLayoutPanel5.Controls.Add(this.pGarbageFileLabel, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.pDOSFileButton, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.pDOSFileLabel, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.pDOSLabel, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.pDOSLineEdit, 1, 0);
            this.tableLayoutPanel5.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 86);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(815, 216);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // pGarbageFileButton
            // 
            this.pGarbageFileButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pGarbageFileButton.AutoSize = true;
            this.pGarbageFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pGarbageFileButton.Location = new System.Drawing.Point(726, 96);
            this.pGarbageFileButton.Name = "pGarbageFileButton";
            this.pGarbageFileButton.Size = new System.Drawing.Size(86, 23);
            this.pGarbageFileButton.TabIndex = 2;
            this.pGarbageFileButton.Text = "Garbage File...";
            this.pGarbageFileButton.Click += new System.EventHandler(this.chooseGarbageFile);
            // 
            // pGarbageFileLabel
            // 
            this.pGarbageFileLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pGarbageFileLabel.AutoSize = true;
            this.pGarbageFileLabel.Location = new System.Drawing.Point(660, 101);
            this.pGarbageFileLabel.Name = "pGarbageFileLabel";
            this.pGarbageFileLabel.Size = new System.Drawing.Size(60, 13);
            this.pGarbageFileLabel.TabIndex = 4;
            this.pGarbageFileLabel.Text = "garbage.txt";
            // 
            // pDOSFileButton
            // 
            this.pDOSFileButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pDOSFileButton.AutoSize = true;
            this.pDOSFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pDOSFileButton.Location = new System.Drawing.Point(586, 96);
            this.pDOSFileButton.Name = "pDOSFileButton";
            this.pDOSFileButton.Size = new System.Drawing.Size(68, 23);
            this.pDOSFileButton.TabIndex = 1;
            this.pDOSFileButton.Text = "DOS File...";
            // 
            // pDOSFileLabel
            // 
            this.pDOSFileLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pDOSFileLabel.AutoSize = true;
            this.pDOSFileLabel.Location = new System.Drawing.Point(542, 101);
            this.pDOSFileLabel.Name = "pDOSFileLabel";
            this.pDOSFileLabel.Size = new System.Drawing.Size(38, 13);
            this.pDOSFileLabel.TabIndex = 2;
            this.pDOSFileLabel.Text = "dos.txt";
            // 
            // pDOSLabel
            // 
            this.pDOSLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pDOSLabel.AutoSize = true;
            this.pDOSLabel.Location = new System.Drawing.Point(3, 101);
            this.pDOSLabel.Name = "pDOSLabel";
            this.pDOSLabel.Size = new System.Drawing.Size(99, 13);
            this.pDOSLabel.TabIndex = 0;
            this.pDOSLabel.Text = "DOS Duration (sec)";
            // 
            // pDOSLineEdit
            // 
            this.pDOSLineEdit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pDOSLineEdit.AutoSize = true;
            this.pDOSLineEdit.DecimalPlaces = 1;
            this.pDOSLineEdit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.pDOSLineEdit.Location = new System.Drawing.Point(108, 98);
            this.pDOSLineEdit.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pDOSLineEdit.Name = "pDOSLineEdit";
            this.pDOSLineEdit.Size = new System.Drawing.Size(62, 20);
            this.pDOSLineEdit.TabIndex = 0;
            this.pDOSLineEdit.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(821, 305);
            this.Controls.Add(this.mainLayout);
            this.Name = "MainWindow";
            this.Text = "WestTel CAD Tester ";
            this.pAutoResponseGroupBox.ResumeLayout(false);
            this.pAutoResponseGroupBox.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.delaySpinBox)).EndInit();
            this.pManualResponseGroupBox.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.mainLayout.ResumeLayout(false);
            this.mainLayout.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pDOSLineEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox pAutoResponseGroupBox;
        private System.Windows.Forms.GroupBox pManualResponseGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label pInitialResponseLabel;
        private System.Windows.Forms.ComboBox pInitialResponseComboBox;
        private System.Windows.Forms.Label pResendResponseLabel;
        private System.Windows.Forms.ComboBox pResendResponseComboBox;
        private System.Windows.Forms.Button pAckButton;
        private System.Windows.Forms.Button pNakButton;
        private System.Windows.Forms.Button pDOSButton;
        private System.Windows.Forms.Button pGarbageButton;
        private System.Windows.Forms.Button pHButton;
        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button pGarbageFileButton;
        private System.Windows.Forms.Label pGarbageFileLabel;
        private System.Windows.Forms.Button pDOSFileButton;
        private System.Windows.Forms.Label pDOSFileLabel;
        private System.Windows.Forms.Label pDOSLabel;
        private System.Windows.Forms.NumericUpDown pDOSLineEdit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label delayLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.NumericUpDown delaySpinBox;
    }
}

