
/*****************************************************************************
* FILE: Version.cs
*  
* DESCRIPTION: Version history
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 26 Jun 2007 Jesse Elliott
******************************************************************************/

using System;

namespace CADTester
{
    class Version
    {
        public static string getVersionStr() {return "2.23";}
    }
}

/*==================================================*
    version 2.23   delivered on 3/3/08

    - fixed regression where valid messages were NAKed
 *==================================================*/

/*==================================================*
    version 2.22   delivered on 2/25/08

    - Handles multiple messages on the socket
 *==================================================*/

/*==================================================*
    version 2.21   delivered on 11/14/07

    - Fixed delay timer to allow sub-seconds
 *==================================================*/

/*==================================================*
    version 2.2   delivered on 10/30/07

    - Added a delay for ACK, NAK, Verified, and Garbage
    - Fixed regression which caused all messages to look like duplicates
 *==================================================*/

/*==================================================*
    version 2.12   delivered on 10/22/07

    - Enable Log is defaulted to off
 *==================================================*/

/*==================================================*
    version 2.11   delivered on 7/??/07

    - Added icon on the window title bar
    - Pulled generic TesterMainWindow out of MainWindow
    - Support canceling DOS
 *==================================================*/

/*==================================================*
    version 2.1   delivered on 7/11/07

    - Added icon on the exe file
    - Fixed memory problem with DOS by only displaying long messages in Verbose mode
    - Improved "Failed to restore...settings" warning to track down Bill's Vista problem
 *==================================================*/

/*==================================================*
    version 2.02   delivered on 7/10/07

    - Fixed tabstops
    - Fixed inability to open a file with forward slashes '/'
    - Support multiple instances by incrmenting the log file (i.e. CADTester-1.log)
    - No longer Publish using Visual Studio so that the file directory is better
 *==================================================*/

/*==================================================*
    version 2.01   delivered on 7/4/07

    - Switched local and remote IP address labels to the correct order
    - Re-added support for DOS and Garbage files
    - Re-added support for logging
    - Added status labels for total bytes sent and received
 *==================================================*/

/*==================================================*
    version 2.0   delivered on 7/2/07

    - Converted to C#
 *==================================================*/

/*==================================================*
    version 1.24   delivered on 4/20/07

    - Select file buttons have names
    - Fixed DOS lockup (maybe) by moving processEvents in handleMessage()
    - Added Verbose checkbox
 *==================================================*/

/*==================================================*
    version 1.23   delivered on 4/18/07

    - Display number of bytes sent and rcvd
    - Actually show the data sent
 *==================================================*/

/*==================================================*
    version 1.22   delivered on 4/16/07

    - Flush the log file regularly (every 0.5 seconds)
    - User specified DOS and Garbage files
    - Ignore NULLs at end of files
 *==================================================*/

/*==================================================*
    version 1.21   delivered on 4/10/07

    - Added a clear message window button
    - Changed dos.txt to CADTester_DOS.txt
    - Fixed bug that caused resend response to be random (changed strcmp to strncmp)
 *==================================================*/
