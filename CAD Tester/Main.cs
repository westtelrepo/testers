
/*****************************************************************************
* FILE: main.cs
*  
* DESCRIPTION: Starting point for the program
* 
*              The CAD Tester (or simulator) connects to a remote IP address
*              and port via sockets. It then responds to incoming messages
*              based on user settings and whether the message is new or a
*              resent message. It can also manually send any of the responses
*              Ack, Nak, Heartbeat, Garbage (from a file), or Denial of
*              Service (from a file).
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 26 Jun 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CADTester
{
    static class ANITesterMain
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}