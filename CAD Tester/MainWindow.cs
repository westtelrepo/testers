
/*****************************************************************************
* FILE: MainWindow.cs
*  
* DESCRIPTION: CAD Main Window
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 26 Jun 2007 Jesse Elliott
******************************************************************************/

using Tester;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CADTester
{
    public partial class MainWindow : TesterMainWindow
    {
        private enum Response
        {
            Random,           // any of the below
            Verified,         // ACK / NAK based on received message
            ACK,              // ACK
            NAK,              // NAK
            Timeout,          // Timeout
            Garbage,          // Garbage
            Denial_of_Service // Denial of Service
        }


        /****************************************************************************************************
        * NAME: Constructor: MainWindow()
        *
        * DESCRIPTION: Constructs an instance of this class and initializes the GUI elements
        *  
        * VARIABLES:
        *   pInitialResponseComboBox Local   - initial responses
        *   pResendResponseComboBox  Local   - resend responses
        *   DataSource                         <System.Windows.Forms.ComboBox>
        *   SelectedIndex                      <System.Windows.Forms.ComboBox>
        *   Interval                           <System.Windows.Forms.Timer>
        *   Tick                               <System.Windows.Forms.Timer>
        *   pDOSButton               Local   - DOS button
        *   KeyPress                           <System.Windows.Forms.Button>
        *   pAckButton               Local   - ACK button
        *   pNakButton               Local   - NAK button
        *   pHButton                 Local   - Heartbeat button
        *   Click                              <System.Windows.Forms.Button>
        *   toolTip                  Library - <TesterMainWindow.cs>
        *                                                                         
        * FUNCTIONS:
        *   InitializeComponent()    Local   - builds GUI
        *   GetNames()                         <System.Enum>
        *   GetHashCode()                      <System.Enum>
        *   init()                   Library - <TesterMainWindow.cs> initializes GUI elements
        *   Timer()                            <System.Windows.Forms> constructor
        *   SetToolTip()                       <System.Windows.Forms.ToolTip>
        *   readFile()               Library - <TesterMainWindow.cs>
        *   setupRecentSocketMsg()   Library - <TesterMainWindow.cs>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 26 Jun 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public MainWindow()
        {
            // build GUI
            InitializeComponent();

            // populate response combo boxes
            pInitialResponseComboBox. DataSource = Enum.GetNames(typeof(Response));
            pResendResponseComboBox.  DataSource = Enum.GetNames(typeof(Response));

            pInitialResponseComboBox. SelectedIndex = Response.Verified.GetHashCode();
            pResendResponseComboBox.  SelectedIndex = Response.Verified.GetHashCode();

            // initialize denial of service (must be before init() so settings are restored)
            Button[] dosButtonList = {pDOSButton};

            initDOS(ref pDOSFileLabel, ref pDOSFileButton, ref dosButtonList, ref pDOSLineEdit);

            // add generic tester GUI
            init("CAD Tester", Version.getVersionStr(), "CAD Tester.ico", mainLayout, null, 1);

            // set duplicate message timeout
            setupRecentSocketMsg(1, 1500);

            // connect ack, nak, and heartbeat buttons
            pAckButton.Click += sendAck;
            pNakButton.Click += sendNak;
            pHButton.Click   += sendHeartbeat;

            // add tool tips
            toolTip.SetToolTip(pHButton,           "Heartbeat (Debug)");
            toolTip.SetToolTip(pGarbageFileButton, "Select Garbage File");

            // read files in
            readFile(garbageFilename, out garbageBuffer, out garbageSize, pGarbageFileLabel);
        }


        /****************************************************************************************************
        * NAME: Method: void restoreSettings()
        *
        * DESCRIPTION: Restores settings from the Windows Registry
        *    
        * PARAMETERS:
        *   pSettings - registry key
        * 
        * VARIABLES:
        *   pInitialResponseComboBox Local   - initial responses
        *   pResendResponseComboBox  Local   - resend responses
        *   SelectedIndex                      <System.Windows.Forms.ComboBox>
        *   Value                              <System.Windows.Forms.NumericUpDown>
        *   garbageFilename          Local   - Garbage filename
        *                                                                         
        * FUNCTIONS:
        *   restoreSettings()        Library - <TesterMainWindow.cs> base call to restore general settings
        *   GetValue()                         <Microsoft.Win32.RegistryKey>
        *   warning()                Library - <TesterMainWindow.cs> record warning message
        *   ToDecimal()                        <System.Convert>
        *   Replace()                          <string>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 23 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void restoreSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            // general settings
            base.restoreSettings(pSettings);

            // response settings
            try
            {
                pInitialResponseComboBox.SelectedIndex =
                    (int)pSettings.GetValue("initialResponse", pInitialResponseComboBox.SelectedIndex);

                pResendResponseComboBox.SelectedIndex =
                    (int)pSettings.GetValue("resendResponse", pResendResponseComboBox.SelectedIndex);

                delaySpinBox.Value = Convert.ToDecimal(pSettings.GetValue("delay", delaySpinBox.Value));
            }
            catch (Exception e)
            {
                warning("Failed to restore response settings (" + e.Message + ")");
            }

            // Garbage file settings
            try
            {
                garbageFilename = (string)pSettings.GetValue("garbageFile", garbageFilename);
                garbageFilename = garbageFilename. Replace('/', '\\');
            }
            catch (Exception e)
            {
                warning("Failed to restore Garbage file settings (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void rememberSettings()
        *
        * DESCRIPTION: Remembers settings in the Windows Registry
        *    
        * PARAMETERS:
        *   pSettings - registry key
        *  
        * VARIABLES:
        *   pInitialResponseComboBox Local   - initial responses
        *   pResendResponseComboBox  Local   - resend responses
        *   SelectedIndex                      <System.Windows.Forms.ComboBox>
        *   Text                               <System.Windows.Forms.NumericUpDown>
        *   garbageFilename          Local   - Garbage filename
        *                                                                         
        * FUNCTIONS:
        *   rememberSettings()       Library - <TesterMainWindow.cs> base call to remember general settings
        *   SetValue()                         <Microsoft.Win32.RegistryKey>
        *   warning()                Library - <TesterMainWindow.cs> record warning message
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 23 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void rememberSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            // general settings
            base.rememberSettings(pSettings);

            // CAD settings
            try
            {
                pSettings.SetValue("initialResponse", pInitialResponseComboBox.SelectedIndex);
                pSettings.SetValue("resendResponse",  pResendResponseComboBox.SelectedIndex);
                pSettings.SetValue("delay",           delaySpinBox.Value);
                pSettings.SetValue("garbageFile",     garbageFilename);
            }
            catch (Exception e)
            {
                warning("Failed to remember CAD settings: " + e.Message);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void sendGarbage()
        *
        * DESCRIPTION: Sends contents of Garbage file
        *    
        * VARIABLES:
        *   garbageBuffer            Local   - contents of Garbage file
        *   garbageSize              Local   - size of Garbage file
        *   garbageFilename          Local   - Garbage filename
        *                                                                         
        * FUNCTIONS:
        *   sendBytes()              Library - <TesterMainWindow.cs> sends bytes over socket
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void sendGarbage(object unusedObject, EventArgs unusedArgs) {sendGarbage(0);}
        private void sendGarbage(int delayMSec)
        {
            sendBytes(garbageBuffer, garbageSize, "Garbage File \"" + garbageFilename + "\"", delayMSec);
        }


        /****************************************************************************************************
        * NAME: Method: void runningChanged()
        *
        * DESCRIPTION: Enables or disables buttons based on running state
        *    
        * PARAMETERS:
        *   running - boolean whether the app is running or not
        *  
        * VARIABLES:
        *   pAckButton               Local   - ACK button
        *   pNakButton               Local   - NAK button
        *   pGarbageButton           Local   - Garbage button
        *   pHButton                 Local   - Heartbeat button
        *   Enabled                            <System.Windows.Forms.Button>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Feb 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void runningChanged(bool running)
        {
            pAckButton.     Enabled = running;
            pNakButton.     Enabled = running;
            pGarbageButton. Enabled = running;
            pHButton.       Enabled = running;
            pDOSButton.     Enabled = running;
        }


        /****************************************************************************************************
        * NAME: Method: uint handleSocketMsg()
        *
        * DESCRIPTION: Handle socket messages by responding with the selected Initial response
        *              if this is not a duplicate message or with the selected Resend response otherwise
        *    
        * PARAMETERS:
        *   newSocketMsg - new message from the socket
        *   length       - length of socket message
        *   c            - commWidget message was received on
        *  
        * VARIABLES:
        *   pInitialResponseComboBox Local   - initial responses
        *   pResendResponseComboBox  Local   - resend responses
        *   SelectedIndex                      <System.Windows.Forms.ComboBox>
        *                                                                         
        * FUNCTIONS:
        *   inVerboseMode()          Library - <TesterMainWindow.cs> tests verbose mode
        *   notice()                 Library - <TesterMainWindow.cs> record notice message
        *   respondToMsg()           Local   - respond to the socket message with the selected response
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 5 Mar 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override uint handleSocketMsg(byte[] newSocketMsg, ulong length, CommWidget c)
        {
            ulong i = 0;

            // look for ETX
            while (i < length)
            {
                if (newSocketMsg[i++] == SocketObj.ETX)
                {
                    i++;
                    break;
                }
            }

            if (i < length) length = i;

            // respond with the selected initial response
            if ( ! c.isDuplicateMsg(newSocketMsg, (uint)length, getCurTimeSec()))
            {
                respondToMsg(newSocketMsg, length,
                    (Response)pInitialResponseComboBox.SelectedIndex);
            }
            // else respond with the selected resend response
            else
            {
                if (inVerboseMode()) notice("[Responding to resent message]");

                respondToMsg(newSocketMsg, length,
                    (Response)pResendResponseComboBox.SelectedIndex);
            }

            return (uint)length;
        }


        /****************************************************************************************************
        * NAME: Method: void respondToMsg()
        *
        * DESCRIPTION: Responds to the given message with the user selected response
        *    
        * PARAMETERS:
        *   socketMsg - socket message to respond to
        *   length    - socket message length
        *   response  - user selected response
        *  
        * VARIABLES:
        *   rand                     Local   - random number generator
        *   Millisecond                        <DateTime.Now>
        *   Second                             <DateTime.Now>
        *   Response                 Local   - enumeration
        *   Length                             <string[]>
        *   Timeout                  Local   - <Response> enumeration value
        *                                                                         
        * FUNCTIONS:
        *   socketIsConnected()      Library - <TesterMainWindow.cs> tests for valid socket
        *   Random()                           <System>
        *   Next()                             <System.Random>
        *   warning()                Library - <TesterMainWindow.cs> record warning message
        *   verifyCheckSum()         Local   - verifies message validity
        *   sendAck()                Local   - sends an ACK
        *   sendNak()                Local   - sends a NAK
        *   notice()                 Library - <TesterMainWindow.cs> record notice message
        *   sendGarbage()            Local   - sends contents of the Garbage file
        *   handleDOSButtonClicked() Local   - starts a Denial of Service
        *   GetNames()                         <System.Enum>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 6 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void respondToMsg(byte[] socketMsg, ulong length, Response response)
        {
            if ( ! socketIsConnected()) return;

            // generate a random response from the Response enumeration
            if (response == Response.Random)
            {
                try
                {
                    Random rand = new Random(DateTime.Now.Millisecond + DateTime.Now.Second);
                    response = (Response)(rand.Next(1, Enum.GetNames(typeof(Response)).Length));
                }
                catch (Exception e)
                {
                    warning("Failed to randomize response (using Timeout): " + e.Message);
                    response = Response.Timeout;
                }
            }

            int delayMSec = (int)(delaySpinBox.Value * 1000);

            if (response == Response.Verified)
            {
                // verify message
                if (verifyCheckSum(socketMsg, length)) sendAck(delayMSec);
                else                                   sendNak(delayMSec);
            }
            else if (response == Response.ACK)               sendAck(delayMSec);
            else if (response == Response.NAK)               sendNak(delayMSec);
            else if (response == Response.Timeout)           notice("[response timeout]");
            else if (response == Response.Garbage)           sendGarbage(delayMSec);
            else if (response == Response.Denial_of_Service) startDOS(pDOSButton, null);
            else
            {
                warning("unrecognized response type \"" +
                    Enum.GetName(typeof(Response), response) + "\"");
            }
        }


        /****************************************************************************************************
        * NAME: Method: bool verifyCheckSum()
        * 
        * DESCRIPTION: Verify the checksum at the end of a data string (usually ALI database or CAD data)
        * 
        * PARAMETERS:
        *   socketMsg - socket message to verify
        *   length    - socket message length
        *  
        * VARIABLES:
        *   curCharIndex             Local   - index of the current character as we loop through the msg
        *   endCharIndex             Local   - index of the last character in the msg
        *   sum                      Local   - progressive sum as we loop through the msg
        *                                                                         
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: Jul 2006 Bill O'Neil
        ****************************************************************************************************/ 

        private bool verifyCheckSum(byte[] socketMsg, ulong length)
        {
            int curCharIndex = 0;
            int endCharIndex = (int)length - 1;

            // ignore STX
            if (socketMsg[curCharIndex] == SocketObj.STX) curCharIndex++;

            // calculate checksum ASCII value up to ETX (including ETX)
            int sum = 0;

            for (; curCharIndex <= endCharIndex; curCharIndex++)
            {
                sum ^= socketMsg[curCharIndex];

                if (socketMsg[curCharIndex] == SocketObj.ETX) break;
            }

            // go to check sum char
            curCharIndex++;

            // compare checksum
            return (curCharIndex < (int)length && socketMsg[curCharIndex] == (byte)sum);
        }


        /****************************************************************************************************
        * NAME: Method: void chooseGarbageFile()
        *
        * DESCRIPTION: Allows the user to choose a Garbage file
        *  
        * VARIABLES:
        *   garbageFilename          Local   - Garbage filename
        *   garbageBuffer            Local   - Garbage file contents
        *   garbageSize              Local   - Garbage file size
        *   pGarbageFileLabel        Local   - Garbage filename label
        *                                                                         
        * FUNCTIONS:
        *   chooseFile()             Library - <TesterMainWindow.cs> Allows the user to choose a file
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 14 Apr 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void chooseGarbageFile(object unusedObject, EventArgs unusedArgs)
        {
            chooseFile(ref garbageFilename, ref garbageBuffer, ref garbageSize, pGarbageFileLabel);
        }


        // private data members

        private string garbageFilename = "CADTester_Garbage.txt";
        private byte[] garbageBuffer;
        private ulong  garbageSize     = 0;
    }
}