
/*****************************************************************************
* FILE: Version.cs
*  
* DESCRIPTION: Version history
* 
* COPYRIGHT: 2011 Experient Corporation
*    AUTHOR: 24 Jul 2007 Jesse Elliott
******************************************************************************/

using System;

namespace ANITester
{
    class Version
    {
        public static string getVersionStr() {return "1.32";}
    }
}

/*==================================================*
    version 1.32  delivered on 3/26/11

    - Added scripting capability to parse and send '\r' and '\n'
 *==================================================*/

/*==================================================*
    version 1.31  delivered on 2/14/08

    - Added time tag to the script file "@time"
 *==================================================*/

/*==================================================*
    version 1.3  delivered on 11/27/07

    - Added DOS feature
    - Added capability to loop the script file
 *==================================================*/

/*==================================================*
    version 1.2  delivered on 11/6/07

    - Added heartbeat file and rate
 *==================================================*/

/*==================================================*
    version 1.1  delivered on 10/22/07

    - Enable Log is defaulted to off
 *==================================================*/

/*==================================================*
    version 1.02  delivered on 9/19/07

    - Added toggle for heartbeats
 *==================================================*/

/*==================================================*
    version 1.012  delivered on 8/28/07

    - changed max script delay from 100 to 100,000 seconds
 *==================================================*/

/*==================================================*
    version 1.011  delivered on 8/24/07

    - fixed heartbeat (not using CAD heartbeat)
 *==================================================*/

/*==================================================*
    version 1.01   delivered on 8/22/07

    - changed script format to [TAG] with the message on the next line
 *==================================================*/

/*==================================================*
    version 1.0   delivered on 8/20/07

    - created
 *==================================================*/
