
/*****************************************************************************
* FILE: MainWindow.Designer.cs
*  
* DESCRIPTION: ANI Main Window (auto-generated)
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 24 Jul 2007 Jesse Elliott
******************************************************************************/

namespace ANITester
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.splitter = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.scriptListBox = new System.Windows.Forms.ListBox();
            this.loadScriptButton = new System.Windows.Forms.Button();
            this.clearScriptButton = new System.Windows.Forms.Button();
            this.scriptDelaySpinBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.scriptLoopCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.heartbeatFileLabel = new System.Windows.Forms.Label();
            this.heartbeatFileButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.heartbeatRateSpinBox = new System.Windows.Forms.NumericUpDown();
            this.dosGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.dosLineEdit = new System.Windows.Forms.NumericUpDown();
            this.dosFileButton = new System.Windows.Forms.Button();
            this.dosFileLabel = new System.Windows.Forms.Label();
            this.dosLabel = new System.Windows.Forms.Label();
            this.dosButton = new System.Windows.Forms.Button();
            this.mainLayout.SuspendLayout();
            this.splitter.Panel1.SuspendLayout();
            this.splitter.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptDelaySpinBox)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heartbeatRateSpinBox)).BeginInit();
            this.dosGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dosLineEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.ColumnCount = 1;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Controls.Add(this.splitter, 0, 0);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 0);
            this.mainLayout.Margin = new System.Windows.Forms.Padding(0);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 1;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Size = new System.Drawing.Size(511, 219);
            this.mainLayout.TabIndex = 0;
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitter.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitter.Location = new System.Drawing.Point(3, 3);
            this.splitter.Name = "splitter";
            this.splitter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitter.Panel1
            // 
            this.splitter.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.splitter.Size = new System.Drawing.Size(505, 213);
            this.splitter.SplitterDistance = 161;
            this.splitter.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.dosGroupBox, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(505, 161);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 101);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scripting";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.scriptListBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.loadScriptButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.clearScriptButton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.scriptDelaySpinBox, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.scriptLoopCheckBox, 2, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(499, 82);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // scriptListBox
            // 
            this.scriptListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptListBox.IntegralHeight = false;
            this.scriptListBox.Location = new System.Drawing.Point(3, 3);
            this.scriptListBox.Name = "scriptListBox";
            this.tableLayoutPanel1.SetRowSpan(this.scriptListBox, 4);
            this.scriptListBox.Size = new System.Drawing.Size(360, 76);
            this.scriptListBox.TabIndex = 0;
            // 
            // loadScriptButton
            // 
            this.loadScriptButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadScriptButton.AutoSize = true;
            this.loadScriptButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.loadScriptButton.Location = new System.Drawing.Point(369, 3);
            this.loadScriptButton.Name = "loadScriptButton";
            this.loadScriptButton.Size = new System.Drawing.Size(41, 23);
            this.loadScriptButton.TabIndex = 1;
            this.loadScriptButton.Text = "&Load";
            this.loadScriptButton.UseVisualStyleBackColor = true;
            // 
            // clearScriptButton
            // 
            this.clearScriptButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clearScriptButton.AutoSize = true;
            this.clearScriptButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.clearScriptButton.Location = new System.Drawing.Point(369, 32);
            this.clearScriptButton.Name = "clearScriptButton";
            this.clearScriptButton.Size = new System.Drawing.Size(41, 23);
            this.clearScriptButton.TabIndex = 2;
            this.clearScriptButton.Text = "&Clear";
            this.clearScriptButton.UseVisualStyleBackColor = true;
            // 
            // scriptDelaySpinBox
            // 
            this.scriptDelaySpinBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.scriptDelaySpinBox.AutoSize = true;
            this.scriptDelaySpinBox.DecimalPlaces = 3;
            this.scriptDelaySpinBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.scriptDelaySpinBox.Location = new System.Drawing.Point(416, 33);
            this.scriptDelaySpinBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.scriptDelaySpinBox.Name = "scriptDelaySpinBox";
            this.scriptDelaySpinBox.Size = new System.Drawing.Size(80, 20);
            this.scriptDelaySpinBox.TabIndex = 3;
            this.scriptDelaySpinBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(416, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Delay (Sec)";
            // 
            // scriptLoopCheckBox
            // 
            this.scriptLoopCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.scriptLoopCheckBox.AutoSize = true;
            this.scriptLoopCheckBox.Location = new System.Drawing.Point(445, 61);
            this.scriptLoopCheckBox.Name = "scriptLoopCheckBox";
            this.scriptLoopCheckBox.Size = new System.Drawing.Size(51, 17);
            this.scriptLoopCheckBox.TabIndex = 5;
            this.scriptLoopCheckBox.Text = "&Loop";
            this.scriptLoopCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.tableLayoutPanel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 110);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(225, 48);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Heartbeat";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.heartbeatFileLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.heartbeatFileButton, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.heartbeatRateSpinBox, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(219, 29);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // heartbeatFileLabel
            // 
            this.heartbeatFileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.heartbeatFileLabel.AutoSize = true;
            this.heartbeatFileLabel.Location = new System.Drawing.Point(3, 8);
            this.heartbeatFileLabel.Name = "heartbeatFileLabel";
            this.heartbeatFileLabel.Size = new System.Drawing.Size(45, 13);
            this.heartbeatFileLabel.TabIndex = 0;
            this.heartbeatFileLabel.Text = "[default]";
            // 
            // heartbeatFileButton
            // 
            this.heartbeatFileButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.heartbeatFileButton.AutoSize = true;
            this.heartbeatFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.heartbeatFileButton.Location = new System.Drawing.Point(54, 3);
            this.heartbeatFileButton.Name = "heartbeatFileButton";
            this.heartbeatFileButton.Size = new System.Drawing.Size(26, 23);
            this.heartbeatFileButton.TabIndex = 1;
            this.heartbeatFileButton.Text = "...";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rate (sec)";
            // 
            // heartbeatRateSpinBox
            // 
            this.heartbeatRateSpinBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.heartbeatRateSpinBox.AutoSize = true;
            this.heartbeatRateSpinBox.DecimalPlaces = 1;
            this.heartbeatRateSpinBox.Location = new System.Drawing.Point(148, 4);
            this.heartbeatRateSpinBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.heartbeatRateSpinBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.heartbeatRateSpinBox.Name = "heartbeatRateSpinBox";
            this.heartbeatRateSpinBox.Size = new System.Drawing.Size(68, 20);
            this.heartbeatRateSpinBox.TabIndex = 4;
            this.heartbeatRateSpinBox.Value = new decimal(new int[] {
            600,
            0,
            0,
            65536});
            // 
            // dosGroupBox
            // 
            this.dosGroupBox.AutoSize = true;
            this.dosGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosGroupBox.Controls.Add(this.tableLayoutPanel4);
            this.dosGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dosGroupBox.Location = new System.Drawing.Point(234, 110);
            this.dosGroupBox.Name = "dosGroupBox";
            this.dosGroupBox.Size = new System.Drawing.Size(274, 48);
            this.dosGroupBox.TabIndex = 3;
            this.dosGroupBox.TabStop = false;
            this.dosGroupBox.Text = "Denial of Service (DOS)";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.dosLineEdit, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.dosFileButton, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.dosFileLabel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.dosLabel, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.dosButton, 5, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(268, 29);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // dosLineEdit
            // 
            this.dosLineEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosLineEdit.AutoSize = true;
            this.dosLineEdit.DecimalPlaces = 1;
            this.dosLineEdit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.dosLineEdit.Location = new System.Drawing.Point(158, 4);
            this.dosLineEdit.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.dosLineEdit.Name = "dosLineEdit";
            this.dosLineEdit.Size = new System.Drawing.Size(62, 20);
            this.dosLineEdit.TabIndex = 2;
            this.dosLineEdit.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // dosFileButton
            // 
            this.dosFileButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosFileButton.AutoSize = true;
            this.dosFileButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosFileButton.Location = new System.Drawing.Point(47, 3);
            this.dosFileButton.Name = "dosFileButton";
            this.dosFileButton.Size = new System.Drawing.Size(26, 23);
            this.dosFileButton.TabIndex = 4;
            this.dosFileButton.Text = "...";
            // 
            // dosFileLabel
            // 
            this.dosFileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosFileLabel.AutoSize = true;
            this.dosFileLabel.Location = new System.Drawing.Point(3, 8);
            this.dosFileLabel.Name = "dosFileLabel";
            this.dosFileLabel.Size = new System.Drawing.Size(38, 13);
            this.dosFileLabel.TabIndex = 3;
            this.dosFileLabel.Text = "dos.txt";
            // 
            // dosLabel
            // 
            this.dosLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosLabel.AutoSize = true;
            this.dosLabel.Location = new System.Drawing.Point(79, 8);
            this.dosLabel.Name = "dosLabel";
            this.dosLabel.Size = new System.Drawing.Size(73, 13);
            this.dosLabel.TabIndex = 1;
            this.dosLabel.Text = "Duration (sec)";
            // 
            // dosButton
            // 
            this.dosButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dosButton.AutoSize = true;
            this.dosButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dosButton.Location = new System.Drawing.Point(226, 3);
            this.dosButton.Name = "dosButton";
            this.dosButton.Size = new System.Drawing.Size(39, 23);
            this.dosButton.TabIndex = 5;
            this.dosButton.Text = "&Start";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 219);
            this.Controls.Add(this.mainLayout);
            this.Name = "MainWindow";
            this.Text = "WestTel ANI Tester";
            this.mainLayout.ResumeLayout(false);
            this.splitter.Panel1.ResumeLayout(false);
            this.splitter.Panel1.PerformLayout();
            this.splitter.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptDelaySpinBox)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heartbeatRateSpinBox)).EndInit();
            this.dosGroupBox.ResumeLayout(false);
            this.dosGroupBox.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dosLineEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.SplitContainer splitter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox scriptListBox;
        private System.Windows.Forms.Button loadScriptButton;
        private System.Windows.Forms.Button clearScriptButton;
        private System.Windows.Forms.NumericUpDown scriptDelaySpinBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label heartbeatFileLabel;
        private System.Windows.Forms.Button heartbeatFileButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown heartbeatRateSpinBox;
        private System.Windows.Forms.CheckBox scriptLoopCheckBox;
        private System.Windows.Forms.GroupBox dosGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label dosLabel;
        private System.Windows.Forms.NumericUpDown dosLineEdit;
        private System.Windows.Forms.Label dosFileLabel;
        private System.Windows.Forms.Button dosFileButton;
        private System.Windows.Forms.Button dosButton;
    }
}

