
/*****************************************************************************
* FILE: MainWindow.cs
*  
* DESCRIPTION: ANI Main Window
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 24 Jul 2007 Jesse Elliott
******************************************************************************/

using Tester;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ANITester
{
    public partial class MainWindow : TesterMainWindow
    {
        /****************************************************************************************************
        * NAME: Constructor: MainWindow()
        *
        * DESCRIPTION: Constructs an instance of this class and initializes the GUI elements
        *  
        * VARIABLES:
        *   pInitialResponseComboBox Local   - initial responses
        *   pResendResponseComboBox  Local   - resend responses
        *   DataSource                         <System.Windows.Forms.ComboBox>
        *   SelectedIndex                      <System.Windows.Forms.ComboBox>
        *   Interval                           <System.Windows.Forms.Timer>
        *   Tick                               <System.Windows.Forms.Timer>
        *   pDOSButton               Local   - DOS button
        *   KeyPress                           <System.Windows.Forms.Button>
        *   pAckButton               Local   - ACK button
        *   pNakButton               Local   - NAK button
        *   pHButton                 Local   - Heartbeat button
        *   Click                              <System.Windows.Forms.Button>
        *   toolTip                  Library - <TesterMainWindow.cs>
        *                                                                         
        * FUNCTIONS:
        *   InitializeComponent()    Local   - builds GUI
        *   GetNames()                         <System.Enum>
        *   GetHashCode()                      <System.Enum>
        *   init()                   Library - <TesterMainWindow.cs> initializes GUI elements
        *   Timer()                            <System.Windows.Forms> constructor
        *   SetToolTip()                       <System.Windows.Forms.ToolTip>
        *   readFile()               Library - <TesterMainWindow.cs>
        *   setupRecentSocketMsg()   Library - <TesterMainWindow.cs>
        *
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Jul 2007 Jesse Elliott
        ****************************************************************************************************/ 

        public MainWindow()
        {
            // build GUI
            InitializeComponent();

            // initialize denial of service (must be before init() so settings are restored)
            Button[] dosButtonList = {dosButton};

            initDOS(ref dosFileLabel, ref dosFileButton, ref dosButtonList, ref dosLineEdit);

            // initialize main window
            init("ANI Tester", Version.getVersionStr(), "ANI Tester.ico", mainLayout, splitter, 1);

            // heartbeat timer
            heartbeatTimer = new Timer();
            heartbeatTimer.Interval = (int)(heartbeatRateSpinBox.Value * 1000);
            heartbeatTimer.Tick += sendHeartbeat;

            heartbeatRateSpinBox.ValueChanged += heartbeatRateChanged;

            // script timer
            scriptTimer = new Timer();
            scriptTimer.Interval = 200;
            scriptTimer.Tick += processScript;

            // connect buttons
            loadScriptButton.    Click += loadScript;
            clearScriptButton.   Click += clearScript;
            heartbeatFileButton. Click += loadHeartbeatFile;

            // tool tips
            toolTip.SetToolTip(heartbeatFileButton, "Select Heartbeat File");

            // load database file
            bool success = readFile(
                heartbeatFilename, out heartbeatBuffer, out heartbeatLength, heartbeatFileLabel);

            if ( ! success)
            {
                heartbeatBuffer = DEFAULT_ANI_HEARTBEAT;
                heartbeatLength = (ulong)DEFAULT_ANI_HEARTBEAT.Length;
            }
        }


        /****************************************************************************************************
        * NAME: Method: void addToBottomLayout()
        *
        * DESCRIPTION: Adds the heartbeat checkbox at the bottom of the GUI
        * 
        * PARAMETERS:
        *   layout - the bottom layout to add GUI items to
        * 
        * VARIABLES:
        *   heartbeatCheckBox        Local   - enables or disables heartbeats
        * 
        * FUNCTIONS:
        *   Add()                    Library - <System.Windows.Forms.TableLayoutPanel.TableLayoutControlCollection>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 19 Sep 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void addToBottomLayout(ref TableLayoutPanel layout)
        {
            layout.Controls.Add(heartbeatCheckBox, 8, 0);

            heartbeatCheckBox.Text     = "&Heartbeat";
            heartbeatCheckBox.Checked  = true;
            heartbeatCheckBox.Anchor   = AnchorStyles.None;
            heartbeatCheckBox.AutoSize = true;
        }


        /****************************************************************************************************
        * NAME: Method: void restoreSettings()
        *
        * DESCRIPTION: Restores ANI settings
        * 
        * PARAMETERS:
        *   pSettings - settings object giving access to the registry
        * 
        * VARIABLES:
        *   heartbeatFilename        Local   - the name of the heartbeat file
        *   scriptDelaySpinBox       Local   - script processing rate
        *   heartbeatRateSpinBox     Local   - heartbeat rate
        *   heartbeatCheckBox        Local   - enables or disables heartbeats
        *   scriptLoopCheckBox       Local   - specifies whether the script loops
        * 
        * FUNCTIONS:
        *   GetValue()               Library - <Microsoft.Win32.RegistryKey>
        *   ToDecimal()              Library - <Microsoft.Win32.RegistryKey>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 14 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void restoreSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            base.restoreSettings(pSettings);

            try
            {
                heartbeatFilename = (string)pSettings.GetValue("heartbeatFile", heartbeatFilename);

                scriptDelaySpinBox.   Value   = Convert.ToDecimal(pSettings.GetValue("scriptDelay",   scriptDelaySpinBox.   Value));
                heartbeatRateSpinBox. Value   = Convert.ToDecimal(pSettings.GetValue("heartbeatRate", heartbeatRateSpinBox. Value));
                heartbeatCheckBox.    Checked = Convert.ToBoolean(pSettings.GetValue("heartbeat",     heartbeatCheckBox.    Checked));
                scriptLoopCheckBox.   Checked = Convert.ToBoolean(pSettings.GetValue("scriptLoop",    scriptLoopCheckBox.   Checked));
            }
            catch (Exception e)
            {
                warning("Failed to restore ANI settings (" + e.Message + ")");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void rememberSettings()
        *
        * DESCRIPTION: Remembers ANI settings
        * 
        * PARAMETERS:
        *   pSettings - settings object giving access to the registry
        * 
        * VARIABLES:
        *   heartbeatFilename        Local   - the name of the heartbeat file
        *   scriptDelaySpinBox       Local   - script processing rate
        *   heartbeatRateSpinBox     Local   - heartbeat rate
        *   heartbeatCheckBox        Local   - enables or disables heartbeats
        *   scriptLoopCheckBox       Local   - specifies whether the script loops
        * 
        * 
        * FUNCTIONS:
        *   SetValue()               Library - <Microsoft.Win32.RegistryKey>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 14 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void rememberSettings(Microsoft.Win32.RegistryKey pSettings)
        {
            base.rememberSettings(pSettings);

            try
            {
                pSettings.SetValue("heartbeatFile", heartbeatFilename);

                pSettings.SetValue("scriptDelay",   scriptDelaySpinBox.   Value);
                pSettings.SetValue("heartbeatRate", heartbeatRateSpinBox. Value);
                pSettings.SetValue("heartbeat",     heartbeatCheckBox.    Checked);
                pSettings.SetValue("scriptLoop",    scriptLoopCheckBox.   Checked);
            }
            catch (Exception e)
            {
                warning("Failed to remember ANI settings: " + e.Message);
            }
        }


        /****************************************************************************************************
        * NAME: Method: void runningChanged()
        *
        * DESCRIPTION: Handles "Start" and "Stop"
        * 
        * PARAMETERS:
        *   running - whether the tester is running
        * 
        * VARIABLES:
        *   dosButton                Local   - enables or disables heartbeats
        *   heartbeatTimer           Local   - enables or disables heartbeats
        *   scriptTimer              Local   - enables or disables heartbeats
        *   lastScriptMsgTime        Local   - enables or disables heartbeats
        * 
        * FUNCTIONS:
        *   sendHeartbeat()          Local   - sends a heartbeat defined in the heartbeat file
        *   Start()                  Library - <System.Windows.Forms.Timer>
        *   getCurTimeSec()          Library - <TesterMainWindow>
        *   processScript()          Local   - processes the currently scripted items
        *   Stop()                   Library - <System.Windows.Forms.Timer>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 7 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void runningChanged(bool running)
        {
            dosButton.Enabled = running;

            if (running)
            {
                sendHeartbeat();
                heartbeatTimer.Start();

                // script timer
                scriptTimer.Start();

                lastScriptMsgTime = getCurTimeSec();
                processScript(this, null);
            }
            else
            {
                heartbeatTimer.Stop();
                scriptTimer.Stop();
            }
        }


        /****************************************************************************************************
        * NAME: Method: void loadHeartbeatFile()
        *
        * DESCRIPTION: Allows the user to choose a heartbeat file
        * 
        * VARIABLES:
        *   DEFAULT_ANI_HEARTBEAT    Local   - default ANI heartbeat
        *   heartbeatFilename        Local   - file that defines the heartbeat
        *   heartbeatBuffer          Local   - content of heartbeat
        *   heartbeatLength          Local   - length of heartbeat content
        *   heartbeatFileLabel       Local   - GUI label to display heartbeat filename
        * 
        * FUNCTIONS:
        *   chooseFile()             Library - <TesterMainWindow>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 6 Nov 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private static byte sp   = (byte)' ';
        private static byte nine = (byte)'9';

        private static byte[] DEFAULT_ANI_HEARTBEAT =
        {
            SocketObj.STX,(byte)'1',(byte)'6',nine,nine,nine,nine,nine,nine,nine,nine,nine,nine,nine,nine,
            sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,sp,
            SocketObj.ETX
        };

        private void loadHeartbeatFile(object unusedObj, EventArgs unusedArgs)
        {
            // choose file
            bool success = chooseFile(
                ref heartbeatFilename, ref heartbeatBuffer, ref heartbeatLength, heartbeatFileLabel);

            if ( ! success)
            {
                heartbeatBuffer = DEFAULT_ANI_HEARTBEAT;
                heartbeatLength = (ulong)DEFAULT_ANI_HEARTBEAT.Length;
            }
        }


        /****************************************************************************************************
        * NAME: Method: void heartbeatRateChanged()
        *
        * DESCRIPTION: Handles when the heartbeat rate changes
        * 
        * VARIABLES:
        *   heartbeatTimer           Local   - manages the timing for heartbeats
        *   heartbeatRateSpinBox     Local   - defines the rate for heartbeats
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 6 Nov 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void heartbeatRateChanged(object unusedObj, EventArgs unusedArgs)
        {
            heartbeatTimer.Interval = (int)(heartbeatRateSpinBox.Value * 1000);
        }


        /****************************************************************************************************
        * NAME: Method: void sendHeartbeat()
        *
        * DESCRIPTION: Send heartbeat if the check box is checked
        * 
        * VARIABLES:
        *   heartbeatCheckBox        Local   - enables or disables heartbeats
        *   heartbeatBuffer          Local   - content of heartbeat
        *   heartbeatLength          Local   - length of heartbeat content
        * 
        * FUNCTIONS:
        *   sendBytes()              Library - <TesterMainWindow>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 24 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        protected override void sendHeartbeat()
        {
            if (heartbeatCheckBox.Checked)
            {
                sendBytes(heartbeatBuffer, heartbeatLength, "Heartbeat");
            }
        }


        /****************************************************************************************************
        * NAME: Method: void loadScript()
        *
        * DESCRIPTION: Ask the user to select a script file and then load it
        * 
        * VARIABLES:
        *   filename                 Local   - script filename
        *   buffer                   Local   - script file contents
        *   length                   Local   - script file length
        *   success                  Local   - script file choosing success
        *   curLine                  Local   - script file current line contents
        *   curLineNum               Local   - script file current line number
        *   curTag                   Local   - script file current message name
        *   scriptListBox            Local   - script item display
        *   scriptList               Local   - list of script items
        *   scriptDelayList          Local   - list of script delays (-1.0 to use default)
        * 
        * FUNCTIONS:
        *   chooseFile()             Library - <TesterMainWindow>
        *   getLine()                Library - <TesterMainWindow>
        *   Substring()              Library - <string>
        *   Add()                    Library - <ObjectCollection>
        *   Add()                    Library - <List<string>>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 12 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void loadScript(object unusedObj, EventArgs unusedArgs)
        {
            // choose file
            string filename = "";
            byte[] buffer   = null;
            ulong  length   = 0;

            bool success = chooseFile(ref filename, ref buffer, ref length, null);

            if ( ! success) return;

            // parse file
            ulong  i = 0;
            string curLine;
            int    curLineNum = 0;

            string curTag   = "";
            double curDelay = -1.0;

            while (getLine(ref buffer, length, ref i, out curLine))
            {
                curLineNum++;

                // ignore comments and blank lines
                if (curLine.Length == 0 || curLine[0] == '#' || curLine[0] == ' ') continue;

                // if expecting tag
                if (curTag.Length == 0)
                {
                    int openBracket  = curLine.IndexOf('[');
                    int closeBracket = curLine.IndexOf(']');

                    if (openBracket >= 0 && closeBracket > openBracket)
                    {
                        curTag = curLine.Substring(openBracket + 1, (closeBracket - openBracket) - 1);

                        // look for a time tag
                        int atSign = curLine.IndexOf('@', closeBracket);

                        if (atSign > closeBracket)
                        {
                            curDelay = Convert.ToDouble(curLine.Substring(atSign + 1));
                        }
                    }
                    else
                    {
                        warning("expected a [tag] - ignoring line " + curLineNum.ToString() + " \"" + curLine + "\"");
                    }
                }
                else
                {
                    curLine = curLine.Replace("<CR>", "\r");

                    scriptListBox.Items. Add(curTag);
                    scriptList.          Add(curLine);
                    scriptDelayList.     Add(curDelay);

                    curTag   = "";
                    curDelay = -1.0;
                }
            }
        }


        /****************************************************************************************************
        * NAME: Method: void clearScript()
        *
        * DESCRIPTION: Clears all currently scripted messages
        * 
        * VARIABLES:
        *   scriptList               Local   - list of script items
        *   scriptListBox            Local   - script item display
        *   scriptDelayList          Local   - list of script delays (-1.0 to use default)
        * 
        * FUNCTIONS:
        *   Clear()                  Library - <ObjectCollection>
        *   Clear()                  Library - <List<string>>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 12 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void clearScript(object unusedObj, EventArgs unusedArgs)
        {
            scriptListBox.Items. Clear();
            scriptList.          Clear();
            scriptDelayList.     Clear();
        }


        /****************************************************************************************************
        * NAME: Method: void processScript()
        *
        * DESCRIPTION: Updates the script timer and sends the next scripted message if the delay has been satisfied
        * 
        * VARIABLES:
        *   scriptDelaySpinBox       Local   - script delay user input
        *   delay                    Local   - script processing interval in seconds
        *   scriptTimer              Local   - timer to manage the script delay
        *   scriptList               Local   - list of script items
        *   scriptDelayList          Local   - list of script delays (-1.0 to use default)
        *   now                      Local   - current time
        *   lastScriptMsgTime        Local   - last time a scripted message was sent
        *   scriptListBox            Local   - script item display
        *   scriptLoopCheckBox       Local   - check box for if the script should repeat
        * 
        * FUNCTIONS:
        *   getCurTimeSec()          Library - <TesterMainWindow>
        *   sendString()             Library - <TesterMainWindow>
        *   Add()                    Library - <ObjectCollection>
        *   Add()                    Library - <List<string>>
        *   RemoveAt()               Library - <ObjectCollection>
        *   RemoveAt()               Library - <List<string>>
        * 
        * COPYRIGHT: 2007 Experient Corporation
        *    AUTHOR: 12 Aug 2007 Jesse Elliott
        ****************************************************************************************************/ 

        private void processScript(object unusedObj, EventArgs unusedArgs)
        {
            if (scriptList.Count > 0)
            {
                double now = getCurTimeSec();

                double nextScriptTime = lastScriptMsgTime + (double)scriptDelaySpinBox.Value;

                if (scriptDelayList[0] != -1.0) nextScriptTime = lastScriptMsgTime + scriptDelayList[0];

                if (now >= nextScriptTime)
                {
                    lastScriptMsgTime = now;

                    string msgStr = scriptList[0];
                    msgStr = msgStr.Replace("\\r", "\r");
                    msgStr = msgStr.Replace("\\n", "\n");

                    string msgHdr = "\nContent-Length:" + msgStr.Length.ToString().PadLeft(11) +
                                    "\nContent-Type:"   + "text/xml".PadLeft(21) + "\n\n";

                    sendString(msgHdr + msgStr, "Script " + (string)scriptListBox.Items[0]);

                    if (scriptLoopCheckBox.Checked)
                    {
                        scriptListBox.Items. Add(scriptListBox.Items[0]);
                        scriptList.          Add(scriptList[0]);
                        scriptDelayList.     Add(scriptDelayList[0]);
                    }

                    scriptListBox.Items. RemoveAt(0);
                    scriptList.          RemoveAt(0);
                    scriptDelayList.     RemoveAt(0);
                }
            }

            int delay = (int)(scriptDelaySpinBox.Value * 1000);

            if (scriptList.Count > 0)
            {
                // adjust the delay if specified in the file
                if (scriptDelayList[0] != -1.0) delay = (int)(scriptDelayList[0] * 1000);
            }

            // make sure timer is fast enough and never zero
            if      (delay > 100) delay = 100;
            else if (delay <= 0)  delay = 1;

            scriptTimer.Interval = delay;
        }


        // private data members
        private string   heartbeatFilename = "[default]";
        private byte[]   heartbeatBuffer   = DEFAULT_ANI_HEARTBEAT;
        private ulong    heartbeatLength   = (ulong)DEFAULT_ANI_HEARTBEAT.Length;

        private CheckBox heartbeatCheckBox = new CheckBox();

        private Timer    heartbeatTimer    = null;
        private Timer    scriptTimer       = null;

        private double   lastScriptMsgTime = -1.0;

        private List<string> scriptList    = new List<string>();
        private List<double> scriptDelayList = new List<double>();
    }
}