
/*****************************************************************************
* FILE: main.cs
*  
* DESCRIPTION: Starting point for the program
* 
*              The ANI Tester (or simulator) connect to one socket and sends
*              file driven messages manually or at a specified rate. It also
*              can send file driven heartbeats at a specified rate.
* 
* COPYRIGHT: 2007 Experient Corporation
*    AUTHOR: 24 Jul 2007 Jesse Elliott
******************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ANITester
{
    static class ANITesterMain
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}